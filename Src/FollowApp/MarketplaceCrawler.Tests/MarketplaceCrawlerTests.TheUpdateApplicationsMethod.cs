﻿using System;
using System.Collections.Generic;
using System.Linq;

using FollowApp;
using FollowApp.DataAccess;
using FollowApp.DataAccess.Entities;
using FollowApp.Entities;
using FollowApp.MarketplaceCrawler;
using FollowApp.UnitTesting.Utils.DataAccess;

using Moq;

using NUnit.Framework;

public sealed partial class MarketplaceCrawlerTests
{
	[TestFixture]
	public sealed class TheUpdateApplicationsMethod
	{
		[Test(Description = "Throws ArgumentNullException when marketplace region is not passed")]
		public void throws_argumentnullexception_when_marketplace_region_is_not_passed()
		{
			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock().Object,
				ApplicationsEntitiesContextMock.Create().Object
				);

			Assert.That(
				() => crawler.UpdateApplications(NullRegion, "Zest", false, CommonCallback),
				Throws.Exception.InstanceOf<ArgumentNullException>().And.Property("ParamName").EqualTo("region")
				);
		}

		[Test(Description = "Throws ArgumentNullException when the store is not passed")]
		public void throws_argumentnullexception_when_the_store_is_not_passed()
		{
			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock().Object,
				ApplicationsEntitiesContextMock.Create().Object
				);

			Assert.That(
				() => crawler.UpdateApplications(SomeRegion, NullStore, false, CommonCallback),
				Throws.Exception.InstanceOf<ArgumentNullException>().And.Property("ParamName").EqualTo("store")
				);
		}

		[Test(Description = "Searches applications with the IMarketplaceReader")]
		public void searches_applications_with_the_imarketplacereader()
		{
			var marketplaceReaderMock = CreateMarketplaceReaderMock();

			IMarketplaceCrawler crawler = CreateCrawler(
				marketplaceReaderMock.Object,
				ApplicationsEntitiesContextMock.Create().Object
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			marketplaceReaderMock.Verify(
				SearchApplicationsWithAnyQuery()
				);
		}

		[Test(Description = "Passed proper query to the SearchApplications method of the IMarketplaceReader")]
		public void passed_proper_query_to_the_searchapplications_method_of_the_imarketplacereader()
		{
			var marketplaceReaderMock = CreateMarketplaceReaderMock();

			IMarketplaceCrawler crawler = CreateCrawler(
				marketplaceReaderMock.Object,
				ApplicationsEntitiesContextMock.Create().Object
				);

			bool[] withDownloadRanksFlags = new[] {true, false};
			foreach (var withDownloadRanks in withDownloadRanksFlags)
			{
				crawler.UpdateApplications(SomeRegion, "Zest", withDownloadRanks, CommonCallback);
				bool tempWithDownoadRanks = withDownloadRanks;
				marketplaceReaderMock.Verify(
					r => r.SearchApplications(
						It.Is<SearchApplicationsQuery>(q =>
							q.ClientType == MarketplaceClientType.WinMobile71 &&
							q.Region.Code == SomeRegion.Code &&
							q.ChunkSize == SearchApplicationsChunkSize.Hundred &&
							q.SortOrder == (tempWithDownoadRanks ? 
								SearchApplicationsSortOrder.TopDownloadRankedGoesFirst : 
								SearchApplicationsSortOrder.LastReleasedGoesFirst
								) &&
							q.Store == "Zest" &&
							q.SearchString == null
							)
						)
					);
			}
		}

		[Test(Description = "Doesn't call callback when there are no applications read from marketplace")]
		public void doesn_t_call_callback_when_there_are_no_applications_read_from_marketplace()
		{
			UpdateApplicationsCallback callback = (application, action, ex) =>
			{
				Assert.Fail("Callback shouldn't be called.");
				return true;
			};

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock().Object,
				ApplicationsEntitiesContextMock.Create().Object
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);
		}

		[Test(Description = "Saves each application into the storage separately")]
		public void saves_each_application_into_the_storage_separately()
		{
			var entitiesContext = ApplicationEntitiesContextToAddApplications(SomeRegion, DummyApplication).Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { DummyApplication, DummyApplication, DummyApplication }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a => a != null)),
				Times.Exactly(3)
				);
			Mock.Get(entitiesContext).Verify(c => c.SaveChanges(), Times.Exactly(3));
		}

		[Test(Description = "Adds application root when it doesn't exist in the marketplace")]
		public void adds_application_root_when_it_doesn_t_exist_in_the_marketplace()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00001");

			var entitiesContext = ApplicationsEntitiesContextMock.Create().
				WithApplications(
					new[]
					{
						new ApplicationEntity
						{
							MarketplaceApplicationId = application2.Id,
							MarketplaceRegion = new MarketplaceRegionEntity {Code = "ru-RU"}
						}
					}
					).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2 }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a => a.MarketplaceApplicationId == application1.Id)),
				Times.Once()
				);
			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a => a.MarketplaceApplicationId == application2.Id)),
				Times.Once()
				);
		}

		[Test(Description = "Adds to the storage the new region if it was not added yet")]
		public void adds_to_the_storage_the_new_region_if_it_was_not_added_yet()
		{
			var region1 = new MarketplaceRegionEntity { Id = 1, Code = "ru-RU" };
			IApplicationsEntitiesContext entitiesContext = ApplicationsEntitiesContextMock.Create().
				WithMarketplaceRegions(new[] { region1 }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { DummyApplication }, null).Object,
				entitiesContext
				);

			crawler.UpdateApplications(new MarketplaceRegion(region1.Code), "Zest", false, CommonCallback);
			Mock.Get(entitiesContext.MarketplaceRegions).
				Verify(s => s.Add(It.IsAny<MarketplaceRegionEntity>()), Times.Never());
			Mock.Get(entitiesContext.MarketplaceRegions).
				Verify(s => s.Add(It.Is<MarketplaceRegionEntity>(r => r.Code == region1.Code)), Times.Never());

			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);
			Mock.Get(entitiesContext.MarketplaceRegions).
				Verify(s => s.Add(It.IsAny<MarketplaceRegionEntity>()), Times.Once());
			Mock.Get(entitiesContext.MarketplaceRegions).
				Verify(s => s.Add(It.Is<MarketplaceRegionEntity>(r => r.Code == SomeRegion.Code)), Times.Once());
		}

		[Test(Description = "Adds to the storage the new publisher if it was not added yet")]
		public void adds_to_the_storage_the_new_publisher_if_it_was_not_added_yet()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7f00000");
			application1.Publisher = new PublisherReference { Id = "Publisher1", Name = "Publisher 1 Inc." };
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7f00001");
			application2.Publisher = new PublisherReference { Id = "Publisher2", Name = "Publisher 2 Inc." };

			var entitiesContext = ApplicationsEntitiesContextMock.Create().
				WithPublishers(new[] { new PublisherEntity { Id = 1, Code = "Publisher1", Name = "Publisher 1 Inc." } }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2 }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.Publishers).Verify(s => s.Add(It.IsAny<PublisherEntity>()), Times.Once());
			Mock.Get(entitiesContext.Publishers).Verify(
				s => s.Add(It.Is<PublisherEntity>(p => p.Code == "Publisher2" && p.Name == "Publisher 2 Inc.")),
				Times.Once()
				);
		}

		[Test(Description = "Allows adding application with empty publisher")]
		public void allows_adding_application_with_empty_publisher()
		{
			var application = DummyApplication;
			application.Publisher = null;

			var entitiesContext = ApplicationsEntitiesContextMock.Create().Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);

			Assert.That(() => crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback), Throws.Nothing);
			Mock.Get(entitiesContext.Publishers).Verify(s => s.Add(It.IsAny<PublisherEntity>()), Times.Never());
		}

		[Test(Description = "Doesn't add application when it exists in the storage")]
		public void doesn_t_add_application_when_it_exists_in_the_storage()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			var entitiesContext = ApplicationsEntitiesContextMock.Create().
				WithApplications(
					new[]
					{
						new ApplicationEntity
						{
							Id = 1,
							MarketplaceApplicationId = application.Id,
							MarketplaceRegion = new MarketplaceRegionEntity {Code = SomeRegion.Code},
							DownloadRanks = new List<ApplicationDownloadRankEntity>()
						}
					}
					).
				WithApplicationAttributes(new[] { new ApplicationAttributesEntity { ApplicationId = 1 } }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a => a.MarketplaceApplicationId == application.Id)),
				Times.Never()
				);
		}

		[Test(Description = "Adds application to the storage if it was not added yet")]
		public void adds_application_to_the_storage_if_it_was_not_added_yet()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00001");

			var entitiesContext = ApplicationsEntitiesContextMock.Create().
				WithApplications(
					new[]
					{
						new ApplicationEntity
						{
							MarketplaceApplicationId = application2.Id,
							MarketplaceRegion = new MarketplaceRegionEntity { Code = "ru-RU" }
						}
					}
					).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2 }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a => a.MarketplaceApplicationId == application1.Id)),
				Times.Once()
				);
			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a => a.MarketplaceApplicationId == application2.Id)),
				Times.Once()
				);
		}

		[Test(Description = "Fills correctly all attributes when added application root to the storage")]
		public void fills_correctly_all_attributes_when_added_application_root_to_the_storage()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a =>
					a.MarketplaceApplicationId == application.Id &&
					a.MarketplaceRegion.Code == SomeRegion.Code &&
					a.Publisher.Code == application.Publisher.Id
					))
				);
		}

		[Test(Description = "Adds new ApplicationAttributes entity to the storage")]
		public void adds_new_applicationattributes_entity_to_the_storage()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.Title = "Sample1";
			application.SortTitle = "Sample1Sort";
			application.Description = "Sample1 description";
			application.ReleaseDate = new DateTime(2011, 12, 12, 13, 40, 20);
			application.Image = new ImageReference(Guid.Parse("578e6666-c265-46b7-b6f4-63cbd7f00000"));

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).Object;

			var now = new DateTime(2011, 12, 31, 0, 0, 0);
			var dateTimeProvider = Mock.Of<IDateTimeProvider>(p => p.UtcNow == now);

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext,
				dateTimeProvider
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.IsAny<ApplicationAttributesEntity>()),
				Times.Once()
				);
			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.DateFrom == now &&
					a.DateTo == null &&
					a.Title == application.Title &&
					a.SortTitle == application.SortTitle &&
					a.Description == null &&
					a.ShortDescription == application.Description &&
					a.Version == application.Version &&
					a.ReleaseDate == application.ReleaseDate &&
					a.ImageId == null &&
					a.SearchImageId == application.Image.Id &&
					a.BackgroundImageId == null &&
					a.VisibilityStatus == null
					)),
				Times.Once()
				);
		}

		[Test(Description = "Closes the previous version of the application attributes")]
		public void closes_the_previous_version_of_the_application_attributes()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity>()
			};
			var existedApplicationAttributesVersion1Entity = new ApplicationAttributesEntity
			{
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = new DateTime(2011, 11, 1)
			};
			var existedApplicationAttributesVersion2Entity = new ApplicationAttributesEntity
			{
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null
			};

			var now = new DateTime(2011, 12, 31, 0, 0, 0);
			int utcNowCallCount = 0;
			var dateTimeProviderMock = new Mock<IDateTimeProvider>();
			dateTimeProviderMock.
				SetupGet(p => p.UtcNow).
				Returns(
					() => (utcNowCallCount++ == 0 ? now : now.AddSeconds(5))
					);

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(
					new[] { existedApplicationAttributesVersion1Entity, existedApplicationAttributesVersion2Entity }
					).
				Object;
			bool previousVersionOfApplicationAttributesIsClosed = false;
			Mock.Get(entitiesContext).
				Setup(c => c.SaveChanges()).
				Callback(() =>
				{
					if (existedApplicationAttributesVersion2Entity.DateTo == now)
					{
						previousVersionOfApplicationAttributesIsClosed = true;
					}
					if (existedApplicationAttributesVersion1Entity.DateTo != new DateTime(2011, 11, 1))
					{
						previousVersionOfApplicationAttributesIsClosed = false;
					}
				}
					);

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext,
				dateTimeProviderMock.Object
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.DateFrom == now &&
					a.DateTo == null
					)),
				Times.Once()
				);
			Assert.That(previousVersionOfApplicationAttributesIsClosed, Is.True);
		}

		[Test(Description = "Doesn't update application attributes in the storage if there are no changes read from the marketplace")]
		public void doesn_t_update_application_attributes_in_the_storage_if_there_are_no_changes_read_from_the_marketplace()
		{
			TestApplicationChanges((application, changesRecepients) => false, "Unknown change was detected.");

			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.Categories =
						changesRecepients.ApplicationAttributes.Categories.Reverse().ToList();
					return false;
				},
				"Incorrect change in categories was detected."
				);

			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.Offers =
						changesRecepients.ApplicationAttributes.Offers.Reverse().ToList();
					return false;
				},
				"Incorrect change in offers was detected."
				);
		}

		[Test(Description = "Updates application attributes in the storage if the are changes of base attributes read from the marketplace")]
		public void updates_application_attributes_in_the_storage_if_the_are_changes_of_base_attributes_read_from_the_marketplace()
		{
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.Title = application.Title + "- new";
					return true;
				},
				"Title has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.SortTitle = application.SortTitle + "- new";
					return true;
				},
				"SortTitle changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.ShortDescription = application.Description + "- new";
					return true;
				},
				"ShortDescription has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.Version = "100.30";
					return true;
				},
				"Version has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.ReleaseDate = application.ReleaseDate.AddDays(1);
					return true;
				},
				"ReleaseDate has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.SearchImageId = new Guid("578ef361-c265-46b7-0001-63cbd7e0000F");
					return true;
				},
				"SearchImage has been changed but it wasn't detected."
				);
		}

		[Test(Description = "Updates application attributes in the storage if the are changes in categories read from the marketplace")]
		public void updates_application_attributes_in_the_storage_if_the_are_changes_in_categories_read_from_the_marketplace()
		{
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					var category = changesRecepients.ApplicationAttributes.Categories.First();
					category.Code = category.Code + ".new";
					return true;
				},
				"The code of the one of the categories has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					var category = changesRecepients.ApplicationAttributes.Categories.First();
					category.Title = category.Title + " - new";
					return true;
				},
				"The title of the one of the categories has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.Categories.Add(
						new ApplicationCategoryEntity { Id = 100, Code = "undefined.new1", Title = "undefined new 1" }
						);
					return true;
				},
				"Categories set has been changed but it wasn't detected."
				);
		}

		[Test(Description = "Updates application attributes in the storage if the are changes in offers read from the marketplace")]
		public void updates_application_attributes_in_the_storage_if_the_are_changes_in_offers_read_from_the_marketplace()
		{
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					var offer = changesRecepients.ApplicationAttributes.Offers.First();
					offer.PaymentTypes = (int)PaymentTypes.MobileOperator;
					return true;
				},
				"The payment type of the one of the offers has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					var offer = changesRecepients.ApplicationAttributes.Offers.First();
					offer.Price = 9.99M;
					return true;
				},
				"The price of the one of the offers has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					var offer = changesRecepients.ApplicationAttributes.Offers.First();
					offer.PriceCurrencyCode = "GBP";
					return true;
				},
				"The price currency code of the one of the offers has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					var offer = changesRecepients.ApplicationAttributes.Offers.First();
					offer.DisplayPrice = "$9.99";
					return true;
				},
				"The display price of the one of the offers has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					var offer = changesRecepients.ApplicationAttributes.Offers.First();
					offer.LicenseRight = (LicenseRight)offer.LicenseRight == LicenseRight.Trial ? (int)LicenseRight.Purchase : (int)LicenseRight.Trial;
					return true;
				},
				"The display price of the one of the offers has been changed but it wasn't detected."
				);
			TestApplicationChanges(
				(application, changesRecepients) =>
				{
					changesRecepients.ApplicationAttributes.Offers.Add(
						new ApplicationOfferEntity { Id = 100, PaymentTypes = (int)PaymentTypes.MobileOperator, Price = 0.89M, DisplayPrice = "$0.89", PriceCurrencyCode = "USD", LicenseRight = (int)LicenseRight.Purchase }
						);
					return true;
				},
				"Offers set has been changed but it wasn't detected."
				);
		}

		[Test(Description = "Adds categories to the new version of the application in the storage")]
		public void adds_categories_to_the_new_version_of_the_application_in_the_storage()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.Categories = new[]
			{
				new ApplicationCategory { Id = "category1", Title = "Category 1"},
				new ApplicationCategory { Id = "category1.1", Title = "Category 1.1", ParentId = "category1"},
				new ApplicationCategory { Id = "category1.2", Title = "Category 1.2", ParentId = "category1"},
				new ApplicationCategory { Id = "category2", Title = "Category 2"},
				new ApplicationCategory { Id = "category2.1", Title = "Category 2.1", ParentId = "category2"},
				new ApplicationCategory { Id = "category2.2", Title = "Category 2.2", ParentId = "category2"},
				new ApplicationCategory { Id = "category2.2.1", Title = "Category 2.2.1", ParentId = "category2.2"},
				new ApplicationCategory { Id = "category3", Title = "Category 3"}
			};

			var someRegionEntity = new MarketplaceRegionEntity { Id = 1, Code = SomeRegion.Code, Name = SomeRegion.Code};
			var otherRegionEntity = new MarketplaceRegionEntity { Id = 7, Code = "ru-RU", Name = "Russia"};
			var existedCategoryEntities = new[]
			{
				new ApplicationCategoryEntity
				{
					Id = 1, Code = "category1", Title = "Category 1", ParentId = null, MarketplaceRegion = someRegionEntity
				},
				new ApplicationCategoryEntity
				{
					Id = 11, Code = "category1.1", Title = "Category 1.1", ParentId = 1,
					ParentCategory = new ApplicationCategoryEntity
					{
						Id = 1, Code = "category1", Title = "Category 1", MarketplaceRegion = someRegionEntity
					},
					MarketplaceRegion = someRegionEntity
				},
				new ApplicationCategoryEntity
				{
					Id = 101, Code = "category1", Title = "Category 1", ParentId = null, MarketplaceRegion = otherRegionEntity
				},
				new ApplicationCategoryEntity
				{
					Id = 1011, Code = "category1.1", Title = "Category 1.1", ParentId = 1,
					ParentCategory = new ApplicationCategoryEntity
					{
						Id = 1, Code = "category1", Title = "Category 1", MarketplaceRegion = otherRegionEntity
					},
					MarketplaceRegion = otherRegionEntity
				}
			};

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithCategories(existedCategoryEntities).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.Categories.Count == 8
					)),
				"Incorrect number of categories were linked to the application."
				);
			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.Categories.Any(c => c.Id == 1 && c.Code == "category1" && c.Title == "Category 1" && c.ParentCategory == null && c.MarketplaceRegion.Code == SomeRegion.Code) &&
					a.Categories.Any(c => c.Id == 11 && c.Code == "category1.1" && c.Title == "Category 1.1" && c.ParentCategory.Id == 1 && c.MarketplaceRegion.Code == SomeRegion.Code)
					)),
				"Categories that are already in the storage weren't linked to te application"
				);
			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.Categories.Any(c => Test(c))
					)),
				"New added categories weren't linked to the parents that are already in the storage"
				);
			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.Categories.Any(c =>
						c.Code == "category2.1" && c.Title == "Category 2.1" &&
						c.ParentCategory != null &&
						c.ParentCategory.Code == "category2" && c.ParentCategory.Title == "Category 2" && c.ParentCategory.ParentCategory == null &&
						c.MarketplaceRegion.Code == SomeRegion.Code
						) &&
					a.Categories.Any(c =>
						c.Code == "category2.2" && c.Title == "Category 2.2" &&
						c.ParentCategory != null &&
						c.ParentCategory.Code == "category2" && c.ParentCategory.Title == "Category 2" && c.ParentCategory.ParentCategory == null &&
						c.MarketplaceRegion.Code == SomeRegion.Code
						) &&
					a.Categories.Any(c =>
						c.Code == "category2.2.1" && c.Title == "Category 2.2.1" &&
						c.ParentCategory != null &&
						c.ParentCategory.Code == "category2.2" && c.ParentCategory.Title == "Category 2.2" && c.ParentCategory.ParentCategory != null &&
						c.MarketplaceRegion.Code == SomeRegion.Code
						)
					)),
				"New added categories weren't linked to the parents that are not in the storage yet"
				);
			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.Categories.Any(c => c.Code == "category3" && c.Title == "Category 3" && c.ParentCategory == null && c.MarketplaceRegion.Code == SomeRegion.Code)
					)),
				"Categories without parents weren't linked to the application"
				);
		}

		private static bool Test(ApplicationCategoryEntity c)
		{
			return c.Code == "category1.2" && c.Title == "Category 1.2" && c.ParentCategory.Id == 1 && c.MarketplaceRegion.Code == SomeRegion.Code;
		}

		[Test(Description = "Adds offes to the new version of the application in the storage")]
		public void adds_offes_to_the_new_version_of_the_application_in_the_storage()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.Offers = new[]
			{
				new ApplicationOffer
				{
					Id = new Guid("578ef361-c265-46b7-b6f4-63cbddd00101"),
					MediaInstanceId = new Guid("578ef361-c265-46b7-b6f4-63cbddf00101"),
					PaymentTypes = PaymentTypes.CreditCard,
					Price = 0.99M,
					DisplayPrice = "$0.99",
					PriceCurrencyCode = "USD",
					Store = "Zest",
					LicenseRight = LicenseRight.Purchase
				},
				new ApplicationOffer
				{
					Id = new Guid("578ef361-c265-46b7-b6f4-63cbddd00102"),
					MediaInstanceId = new Guid("578ef361-c265-46b7-b6f4-63cbddf00102"),
					PaymentTypes = PaymentTypes.CreditCard | PaymentTypes.MobileOperator,
					Price = 0,
					DisplayPrice = "$0.00",
					PriceCurrencyCode = "USD",
					Store = "Zest",
					LicenseRight = LicenseRight.Trial
				}
			};

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.Offers.Count == 2
					)),
				"Incorrect number of offers were linked to the application."
				);
			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.Offers.Any(o =>
						o.MarketplaceOfferId == new Guid("578ef361-c265-46b7-b6f4-63cbddd00101") &&
						o.MarketplaceMediaInstanceId == new Guid("578ef361-c265-46b7-b6f4-63cbddf00101") &&
						o.PaymentTypes == (int)PaymentTypes.CreditCard &&
						o.Price == 0.99M && o.PriceCurrencyCode == "USD" && o.DisplayPrice == "$0.99" &&
						o.LicenseRight == (int)LicenseRight.Purchase && o.Store == "Zest"
						) &&
					a.Offers.Any(o =>
						o.MarketplaceOfferId == new Guid("578ef361-c265-46b7-b6f4-63cbddd00102") &&
						o.MarketplaceMediaInstanceId == new Guid("578ef361-c265-46b7-b6f4-63cbddf00102") &&
						o.PaymentTypes == (int)(PaymentTypes.CreditCard | PaymentTypes.MobileOperator) &&
						o.Price == 0 && o.PriceCurrencyCode == "USD" && o.DisplayPrice == "$0.00" &&
						o.LicenseRight == (int)LicenseRight.Trial && o.Store == "Zest"
						)
					)),
				"The offers with incorrect attributes were linked to the application."
				);
		}

		[Test(Description = "Adds user rating statistics to the new added application")]
		public void adds_user_rating_statistics_to_the_new_added_application()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.UserRatingCount = 10;
			application.AverageUserRating = 4.8;
			application.LastInstanceUserRatingCount = 2;
			application.AverageLastInstanceUserRating = 3.6;

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.UserRatings.Count() == 1 &&
					a.UserRatings.Any(r => 
						r.DateTo == null &&
						r.Count == 10 &&
						Math.Abs(r.Average - 4.8) < 0.00001 &&
						r.LastInstanceCount == 2 &&
						Math.Abs(r.LastInstanceAverage - 3.6) < 0.00001
						)
					)),
				Times.Once()
				);
		}

		[Test(Description = "Closes previous user rating statistics record if the statistics is changed")]
		public void closes_previous_user_rating_statistics_record_if_the_statistics_is_changed()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.UserRatingCount = 10;
			application.AverageUserRating = 4.8;
			application.LastInstanceUserRatingCount = 2;
			application.AverageLastInstanceUserRating = 3.6;

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity>()
			};
			var userRatingStatisticsV1Entity = new ApplicationUserRatingStatisticsEntity
			{
				ApplicationAttributesId = 100123,
				DateTo = new DateTime(2011, 11, 10),
				Count = 5,
				Average = 8,
				LastInstanceCount = 3,
				LastInstanceAverage = 9
			};
			var userRatingStatisticsV2Entity = new ApplicationUserRatingStatisticsEntity
			{
				ApplicationAttributesId = 100123,
				DateTo = null,
				Count = 6,
				Average = 8.4,
				LastInstanceCount = 4,
				LastInstanceAverage = 9.4
			};
			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null,
				Title = application.Title,
				SortTitle = application.SortTitle,
				ShortDescription = application.Description,
				Version = application.Version,
				ReleaseDate = application.ReleaseDate,
				SearchImageId = application.Image.Id,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>
				{
					userRatingStatisticsV1Entity,
					userRatingStatisticsV2Entity
				}
			};

			var now = new DateTime(2011, 12, 31, 0, 0, 0);
			int utcNowCallCount = 0;
			var dateTimeProviderMock = new Mock<IDateTimeProvider>();
			dateTimeProviderMock.
				SetupGet(p => p.UtcNow).
				Returns(
					() => (utcNowCallCount++ == 0 ? now : now.AddSeconds(5))
					);

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { applicationAttributesEntity }).
				Object;
			
			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext,
				dateTimeProviderMock.Object
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Assert.That(userRatingStatisticsV1Entity.DateTo, Is.EqualTo(new DateTime(2011, 11, 10)));
			Assert.That(userRatingStatisticsV2Entity.DateTo, Is.EqualTo(now));
			Assert.That(
				applicationAttributesEntity.UserRatings.Any(r =>
						r.ApplicationAttributesId == 100123 &&
						r.DateTo == null &&
						r.DateFrom == now &&
						r.Count == 10 &&
						Math.Abs(r.Average - 4.8) < 0.00001 &&
						r.LastInstanceCount == 2 &&
						Math.Abs(r.LastInstanceAverage - 3.6) < 0.00001
					),
				Is.True
				);
		}

		[Test(Description = "Doesn't add new user rating statistics if it isn't changed")]
		public void doesn_t_add_new_user_rating_statistics_if_it_isn_t_changed()
		{
			TestUserRatingStatisticsChanges((application, changesRecepient) => false, "Unknown change was detected.");
		}

		[Test(Description = "Adds user rating statistics if it is changed")]
		public void adds_user_rating_statistics_if_it_is_changed()
		{
			TestUserRatingStatisticsChanges(
				(application, changesRecepient) =>
				{
					changesRecepient.UserRatingStatistics.Count = 1000;
					return true;
				},
				"User rating records count was changed but it wasn't detected."
				);
			TestUserRatingStatisticsChanges(
				(application, changesRecepient) =>
				{
					changesRecepient.UserRatingStatistics.Average = 1000;
					return true;
				},
				"Average user rating was changed but it wasn't detected."
				);
			TestUserRatingStatisticsChanges(
				(application, changesRecepient) =>
				{
					changesRecepient.UserRatingStatistics.LastInstanceCount = 1000;
					return true;
				},
				"User rating records count for the last release was changed but it wasn't detected."
				);
			TestUserRatingStatisticsChanges(
				(application, changesRecepient) =>
				{
					changesRecepient.UserRatingStatistics.LastInstanceAverage = 1000;
					return true;
				},
				"Average user rating for the last release was changed but it wasn't detected."
				);
		}

		[Test(Description = "Doesn't update download ranks when the flag withDownloadRanks is not passed")]
		public void doesn_t_update_download_ranks_when_the_flag_withdownloadranks_is_not_passed()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00001");

			var rankV1Entity = new ApplicationDownloadRankEntity
			{
				ApplicationId = 123,
				DateTo = null,
				Rank = 5
			};
			var application2Entity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application2.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity> { rankV1Entity }
			};
			var application2AttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = application2Entity,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>()
			};

			IApplicationsEntitiesContext entitiesContext = 
				ApplicationEntitiesContextToAddApplications(SomeRegion, application1).
				WithApplications(new[] {application2Entity}).
				WithApplicationAttributes(new[] {application2AttributesEntity}).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2 }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, CommonCallback);

			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a =>
					a.MarketplaceApplicationId == application1.Id &&
					!a.DownloadRanks.Any()
					)),
				Times.Once()
				);

			Assert.That(
				application2Entity.DownloadRanks.Count(),
				Is.EqualTo(1)
				);
			Assert.That(
				application2Entity.DownloadRanks.Any(r =>
					r.ApplicationId == 123 &&
					r.DateTo == null &&
					rankV1Entity.Rank == 5
					)
				);
		}

		[Test(Description = "Adds download rank to the new added application")]
		public void adds_download_rank_to_the_new_added_application()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00001");
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00002");
			var application3 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00003");
			var application4 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00004");
			var application5 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00005");

			IApplicationsEntitiesContext entitiesContext = ApplicationEntitiesContextToAddApplications(
				SomeRegion,
				application1, application2, application3, application4, application5
				).Object;

			Func<SearchApplicationsResult> nextChunkGetter2 =
				() => new SearchApplicationsResult(new[] { application5 }, null);
			Func<SearchApplicationsResult> nextChunkGetter1 =
				() => new SearchApplicationsResult(new[] { application3, application4 }, nextChunkGetter2);

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2 }, nextChunkGetter1).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", true, CommonCallback);

			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a =>
					a.MarketplaceApplicationId == application1.Id &&
					a.DownloadRanks.Count() == 1 &&
					a.DownloadRanks.Any(r => r.Rank == 1)
					)),
				Times.Once()
				);
			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a =>
					a.MarketplaceApplicationId == application2.Id &&
					a.DownloadRanks.Count() == 1 &&
					a.DownloadRanks.Any(r => r.Rank == 2)
					)),
				Times.Once()
				);
			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a =>
					a.MarketplaceApplicationId == application3.Id &&
					a.DownloadRanks.Count() == 1 &&
					a.DownloadRanks.Any(r => r.Rank == 3)
					)),
				Times.Once()
				);
			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a =>
					a.MarketplaceApplicationId == application4.Id &&
					a.DownloadRanks.Count() == 1 &&
					a.DownloadRanks.Any(r => r.Rank == 4)
					)),
				Times.Once()
				);
			Mock.Get(entitiesContext.Applications).Verify(
				s => s.Add(It.Is<ApplicationEntity>(a =>
					a.MarketplaceApplicationId == application5.Id &&
					a.DownloadRanks.Count() == 1 &&
					a.DownloadRanks.Any(r => r.Rank == 5)
					)),
				Times.Once()
				);
		}

		[Test(Description = "Closes previous download rank record if the rank is changed")]
		public void closes_previous_download_rank_record_if_the_rank_is_changed()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			var rankV1Entity = new ApplicationDownloadRankEntity
			{
				ApplicationId = 123,
				DateTo = new DateTime(2011, 11, 10),
				Rank = 10
			};
			var rankV2Entity = new ApplicationDownloadRankEntity
			{
				ApplicationId = 123,
				DateTo = null,
				Rank = 5
			};

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity> {rankV1Entity, rankV2Entity}
			};

			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>()
			};
			
			var now = new DateTime(2011, 12, 31, 0, 0, 0);
			var dateTimeProviderMock = new Mock<IDateTimeProvider>();
			dateTimeProviderMock.SetupGet(p => p.UtcNow).Returns(() => now);

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] {applicationAttributesEntity}).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext,
				dateTimeProviderMock.Object
				);
			crawler.UpdateApplications(SomeRegion, "Zest", true, CommonCallback);

			Assert.That(rankV1Entity.DateTo, Is.EqualTo(new DateTime(2011, 11, 10)));
			Assert.That(rankV2Entity.DateTo, Is.EqualTo(now));
			Assert.That(
				existedApplicationEntity.DownloadRanks.Any(r =>
					r.ApplicationId == existedApplicationEntity.Id &&
					r.DateTo == null &&
					r.DateFrom == now &&
					r.Rank == 1
					),
				Is.True
				);
		}

		[Test(Description = "Doesn't add new download rank if it isn't changed")]
		public void doesn_t_add_new_download_rank_if_it_isn_t_changed()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			
			var rankV1Entity = new ApplicationDownloadRankEntity
			{
				ApplicationId = 123,
				DateTo = null,
				Rank = 1
			};
			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity> {rankV1Entity}
			};
			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>()
			};

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { applicationAttributesEntity }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", true, null);

			Assert.That(
				existedApplicationEntity.DownloadRanks.Count(),
				Is.EqualTo(1),
				"Application download rank has been updated in the storage but it wasn't changed in the marketplace."
				);
		}


		[Test(Description = "Calls callbacks for all applications read from the marketplace")]
		public void calls_callbacks_for_all_applications_read_from_the_marketplace()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00001");
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00002");
			var application3 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00003");
			var application4 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00004");
			
			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2, application3, application4 }, null).Object,
				ApplicationEntitiesContextToAddApplications(SomeRegion, application1, application2, application3, application4).Object
				);

			int totalCallbacks = 0;
			int expectedApplicationsCallbacks = 0;
			var expectedApplicationIds = new[] { application1.Id, application2.Id, application3.Id, application4.Id };
			UpdateApplicationsCallback callback = (application, action, ex) =>
			{
				++totalCallbacks;

				if (expectedApplicationIds.Contains(application.Id))
				{
					++expectedApplicationsCallbacks;
				}
				
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(totalCallbacks, Is.EqualTo(4));
			Assert.That(expectedApplicationsCallbacks, Is.EqualTo(4));
		}

		[Test(Description = "Can be stopped by the callback")]
		public void can_be_stopped_by_the_callback()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00001");
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00002");

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2 }, null).Object,
				ApplicationEntitiesContextToAddApplications(SomeRegion, application1, application2).Object
				);

			int totalCallbacks = 0;
			UpdateApplicationsCallback callback = (application, action, ex) =>
			{
				++totalCallbacks;
				return false;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(totalCallbacks, Is.EqualTo(1));
		}

		[Test(Description = "Passes Nothing action to the callback if the application wasn't updated")]
		public void passes_nothing_action_to_the_callback_if_the_application_wasn_t_updated()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.UserRatingCount = 10;
			application.AverageUserRating = 4.8;
			application.LastInstanceUserRatingCount = 2;
			application.AverageLastInstanceUserRating = 3.6;

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity>
				{
					new ApplicationDownloadRankEntity
					{
						Rank = 1
					}
				}
			};
			var userRatingStatisticsEntity = new ApplicationUserRatingStatisticsEntity
			{
				ApplicationAttributesId = 100123,
				DateTo = null,
				Count = 10,
				Average = 4.8,
				LastInstanceCount = 2,
				LastInstanceAverage = 3.6
			};
			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null,
				Title = application.Title,
				SortTitle = application.SortTitle,
				ShortDescription = application.Description,
				Version = application.Version,
				ReleaseDate = application.ReleaseDate,
				SearchImageId = application.Image.Id,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>
				{
					userRatingStatisticsEntity
				}
			};

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { applicationAttributesEntity }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			
			UpdateApplicationAction? actualAction = null;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				actualAction = action;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(actualAction, Is.Not.Null.And.EqualTo(UpdateApplicationAction.Nothing));
		}

		[Test(Description = "Passes UpdatedDownloadRank action to the callback if the application rank was updated")]
		public void passes_updateddownloadrank_action_to_the_callback_if_the_application_rank_was_updated()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity>
				{
					new ApplicationDownloadRankEntity { Rank = 100 }
				}
			};
			var userRatingStatisticsEntity = new ApplicationUserRatingStatisticsEntity
			{
				ApplicationAttributesId = 100123,
				DateTo = null,
				Count = 10,
				Average = 4.8,
				LastInstanceCount = 2,
				LastInstanceAverage = 3.6
			};
			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null,
				Title = application.Title,
				SortTitle = application.SortTitle,
				ShortDescription = application.Description,
				Version = application.Version,
				ReleaseDate = application.ReleaseDate,
				SearchImageId = application.Image.Id,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>
				{
					userRatingStatisticsEntity
				}
			};

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { applicationAttributesEntity }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);

			UpdateApplicationAction? actualAction = null;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				actualAction = action;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", true, callback);

			Assert.That(actualAction, Is.Not.Null);
			Assert.That(actualAction & UpdateApplicationAction.UpdatedDownloadRank, Is.EqualTo(UpdateApplicationAction.UpdatedDownloadRank));
		}

		[Test(Description = "Passes AddedNew action to the callback if the application wasn't added to the storage before")]
		public void passes_addednew_action_to_the_callback_if_the_application_wasn_t_added_to_the_storage_before()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				ApplicationsEntitiesContextMock.Create().Object
				);

			UpdateApplicationAction? actualAction = null;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				actualAction = action;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(actualAction, Is.Not.Null.And.EqualTo(UpdateApplicationAction.AddedNew));
		}

		[Test(Description = "Passes Updated action to the callback if the base application attributes were updated")]
		public void passes_updated_action_to_the_callback_if_the_base_application_attributes_were_updated()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity>()
			};
			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null,
				Title = application.Title + " - old",
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
			};

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { applicationAttributesEntity }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);

			UpdateApplicationAction? actualAction = null;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				actualAction = action;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(actualAction, Is.Not.Null);
			Assert.That(actualAction & UpdateApplicationAction.Updated, Is.EqualTo(UpdateApplicationAction.Updated));
		}

		[Test(Description = "Passes UpdatedUserRatingStatistics action to the callback if only user rating statistics of the application was updated")]
		public void passes_updateduserratingstatistics_action_to_the_callback_if_only_user_rating_statistics_of_the_application_was_updated()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.UserRatingCount = 10;
			application.AverageUserRating = 4.8;
			application.LastInstanceUserRatingCount = 2;
			application.AverageLastInstanceUserRating = 3.6;

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity>()
			};
			var userRatingStatisticsEntity = new ApplicationUserRatingStatisticsEntity
			{
				ApplicationAttributesId = 100123,
				DateTo = null,
				Count = 11,
			};
			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null,
				Title = application.Title,
				SortTitle = application.SortTitle,
				ShortDescription = application.Description,
				Version = application.Version,
				ReleaseDate = application.ReleaseDate,
				SearchImageId = application.Image.Id,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>
				{
					userRatingStatisticsEntity
				}
			};

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { applicationAttributesEntity }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);

			UpdateApplicationAction? actualAction = null;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				actualAction = action;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(actualAction, Is.Not.Null);
			Assert.That(
				actualAction & UpdateApplicationAction.UpdatedUserRatingStatistics,
				Is.EqualTo(UpdateApplicationAction.UpdatedUserRatingStatistics)
				);
		}

		[Test(Description = "Passes application to the callback")]
		public void passes_application_to_the_callback()
		{
			var application = new Application
			{
				Id = Guid.NewGuid(),
				Publisher = new PublisherReference { Id = "MyCompany", Name = "My Company" },
				Title = "My App",
				SortTitle = "My App",
				Description = "The best app ever",
				ReleaseDate = new DateTime(2012, 04, 15),
				Image = new ImageReference(Guid.NewGuid()),
				Version = "1.5",
				Categories = new[] { new ApplicationCategory { Id = "business", Title = "Business", ParentId = null } },
				Offers = new[] { new ApplicationOffer { Id = Guid.NewGuid() } },
				Tags = new[] { new Tag("MyTag") },
				UserRatingCount = 10,
				AverageUserRating = 6,
				LastInstanceUserRatingCount = 3,
				AverageLastInstanceUserRating = 5
			};

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				ApplicationsEntitiesContextMock.Create().Object
				);

			Application callbackApp = null;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				callbackApp = app;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(callbackApp, Is.Not.Null);
			Assert.That(callbackApp.Id, Is.EqualTo(application.Id));
			Assert.That(callbackApp.Title, Is.EqualTo(application.Title));
			Assert.That(callbackApp.SortTitle, Is.EqualTo(application.SortTitle));
			Assert.That(callbackApp.Description, Is.EqualTo(application.Description));
			Assert.That(callbackApp.ReleaseDate, Is.EqualTo(application.ReleaseDate));
			Assert.That(callbackApp.Version, Is.EqualTo(application.Version));
			Assert.That(callbackApp.Image.Id, Is.EqualTo(application.Image.Id));
			Assert.That(callbackApp.UserRatingCount, Is.EqualTo(application.UserRatingCount));
			Assert.That(callbackApp.AverageUserRating, Is.EqualTo(application.AverageUserRating));
			Assert.That(callbackApp.LastInstanceUserRatingCount, Is.EqualTo(application.LastInstanceUserRatingCount));
			Assert.That(callbackApp.AverageLastInstanceUserRating, Is.EqualTo(application.AverageLastInstanceUserRating));
			Assert.That(callbackApp.Publisher.Id, Is.EqualTo(application.Publisher.Id));
			Assert.That(callbackApp.Publisher.Name, Is.EqualTo(application.Publisher.Name));
			Assert.That(callbackApp.Categories.Count(), Is.EqualTo(1));
			Assert.That(callbackApp.Categories.First().Id, Is.EqualTo(application.Categories.First().Id));
			Assert.That(callbackApp.Offers.Count(), Is.EqualTo(1));
			Assert.That(callbackApp.Offers.First().Id, Is.EqualTo(application.Offers.First().Id));
			Assert.That(callbackApp.Tags.Count(), Is.EqualTo(1));
			Assert.That(callbackApp.Tags.First().Id, Is.EqualTo(application.Tags.First().Id));
		}

		[Test(Description = "Passes exception object to the callback when it occured while processing application")]
		public void passes_exception_object_to_the_callback_when_it_occured_while_processing_application()
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");

			var applicationsEntitiesContext = ApplicationEntitiesContextToAddApplications(SomeRegion, application).Object;
			Mock.Get(applicationsEntitiesContext).
				Setup(c => c.SaveChanges()).
				Callback(
					delegate
					{
						throw new InvalidOperationException("Emulated exception.");
					}
					);

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				applicationsEntitiesContext
				);

			Exception exception = null;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				exception = ex;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(exception, Is.Not.Null);
			Assert.That(exception, Is.TypeOf<InvalidOperationException>());
			Assert.That(exception.Message, Is.EqualTo("Emulated exception."));
		}

		[Test(Description = "Loads all applications batches with marketplace reader")]
		public void loads_all_applications_batches_with_marketplace_reader()
		{
			var application1 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00001");
			var application2 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00002");
			var application3 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00003");
			var application4 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00004");
			var application5 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00005");
			var application6 = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00006");

			Func<SearchApplicationsResult> nextChunkGetter2 =
				() => new SearchApplicationsResult(new[] { application5, application6 }, null);
			Func<SearchApplicationsResult> nextChunkGetter1 =
				() => new SearchApplicationsResult(new[] { application3, application4 }, nextChunkGetter2);
			
			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application1, application2 }, nextChunkGetter1).Object,
				ApplicationsEntitiesContextMock.Create().Object
				);

			int callbacksCounter = 0;
			UpdateApplicationsCallback callback = (app, action, ex) =>
			{
				++callbacksCounter;
				return true;
			};

			crawler.UpdateApplications(SomeRegion, "Zest", false, callback);

			Assert.That(callbacksCounter, Is.EqualTo(6));
		}



		#region TheUpdateApplicationsMethod helpers

		private class ApplicationAttributesChangesRecepient
		{
			public ApplicationAttributesEntity ApplicationAttributes { get; private set; }

			public ApplicationAttributesChangesRecepient(ApplicationAttributesEntity applicationAttributes)
			{
				ApplicationAttributes = applicationAttributes;
			}
		}

		private class UserRatingStatisticsChangesRecipient
		{
			public ApplicationUserRatingStatisticsEntity UserRatingStatistics { get; private set; }

			public UserRatingStatisticsChangesRecipient(ApplicationUserRatingStatisticsEntity userRatingStatistics)
			{
				UserRatingStatistics = userRatingStatistics;
			}
		}

		private static bool CommonCallback(Application application, UpdateApplicationAction action, Exception exception)
		{
			Assert.DoesNotThrow(
				delegate { if (exception != null) throw exception; },
				exception == null ? string.Empty : exception.ToString()
				);
			return true;
		}

		private static void TestUserRatingStatisticsChanges(
			Func<Application, UserRatingStatisticsChangesRecipient, bool> changesInjector,
			string failMessage
			)
		{
			var application = GetDummyApplication("578ef361-c265-46b7-b6f4-63cbd7e00000");
			application.UserRatingCount = 10;
			application.AverageUserRating = 4.8;
			application.LastInstanceUserRatingCount = 2;
			application.AverageLastInstanceUserRating = 3.6;

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 123,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				DownloadRanks = new List<ApplicationDownloadRankEntity>()
			};
			var userRatingStatisticsV1Entity = new ApplicationUserRatingStatisticsEntity
			{
				ApplicationAttributesId = 100123,
				DateTo = null,
				Count = 10,
				Average = 4.8,
				LastInstanceCount = 2,
				LastInstanceAverage = 3.6
			};
			var applicationAttributesEntity = new ApplicationAttributesEntity
			{
				Id = 100123,
				ApplicationId = 123,
				Application = existedApplicationEntity,
				DateTo = null,
				Title = application.Title,
				SortTitle = application.SortTitle,
				ShortDescription = application.Description,
				Version = application.Version,
				ReleaseDate = application.ReleaseDate,
				SearchImageId = application.Image.Id,
				Categories = new List<ApplicationCategoryEntity>(),
				Offers = new List<ApplicationOfferEntity>(),
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>
				{
					userRatingStatisticsV1Entity
				}
			};

			var changesRecipient = new UserRatingStatisticsChangesRecipient(userRatingStatisticsV1Entity);
			bool hasChanges = changesInjector(application, changesRecipient);

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { applicationAttributesEntity }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, null);

			int expectedUserRatingStatisticsVersionsCount = hasChanges ? 2 : 1;
			Assert.That(
				applicationAttributesEntity.UserRatings.Count(),
				Is.EqualTo(expectedUserRatingStatisticsVersionsCount),
				failMessage
				);
		}

		private static void TestApplicationChanges(
			Func<Application, ApplicationAttributesChangesRecepient, bool> changesInjector,
			string failMessage
			)
		{
			var application = new Application
			{
				Id = Guid.Parse("578ef361-c265-46b7-b6f4-63cbd7e00000"),
				Publisher = new PublisherReference { Id = "Publisher1", Name = "Publisher 1 Inc." },
				Title = "App 1 (Lite)",
				SortTitle = "App 1",
				Description = "App 1 description",
				Version = "1.1",
				ReleaseDate = new DateTime(2011, 12, 01, 0, 0, 0, DateTimeKind.Utc),
				Image = new ImageReference(Guid.Parse("578ef361-c265-46b7-0001-63cbd7e00000")),
				Categories = new[]
				{
					new ApplicationCategory { Id = "windowsphone.navigation", Title = "navigation", ParentId = "windowsphone.travel"},
					new ApplicationCategory { Id = "windowsphone.travel", Title = "travel and navigation"}
				},
				Offers = new[]
				{
					new ApplicationOffer { PaymentTypes = PaymentTypes.CreditCard, Price = 0.99M, DisplayPrice = "$0.99", PriceCurrencyCode = "USD", LicenseRight = LicenseRight.Purchase },
					new ApplicationOffer { PaymentTypes = PaymentTypes.CreditCard, Price = 0, DisplayPrice = "$0.00", PriceCurrencyCode = "USD", LicenseRight = LicenseRight.Trial }
				}
			};

			var existedApplicationEntity = new ApplicationEntity
			{
				Id = 1,
				MarketplaceApplicationId = application.Id,
				MarketplaceRegion = new MarketplaceRegionEntity { Code = SomeRegion.Code },
				Publisher = new PublisherEntity { Code = application.Publisher.Id },
				DownloadRanks = new List<ApplicationDownloadRankEntity>()
			};
			var existedApplicationAttributesEntity = new ApplicationAttributesEntity
			{
				ApplicationId = 1,
				Title = application.Title,
				SortTitle = application.SortTitle,
				ShortDescription = application.Description,
				Version = application.Version,
				ReleaseDate = application.ReleaseDate,
				SearchImageId = application.Image.Id,
				Categories = new List<ApplicationCategoryEntity>
				{
					new ApplicationCategoryEntity {Id = 1, Code = "windowsphone.travel", Title = "travel and navigation", ParentId = null},
					new ApplicationCategoryEntity {Id = 2, Code = "windowsphone.navigation", Title = "navigation", ParentId = 1}
				},
				Offers = new List<ApplicationOfferEntity>
				{
					new ApplicationOfferEntity { Id = 1, PaymentTypes = (int)PaymentTypes.CreditCard, Price = 0.99M, DisplayPrice = "$0.99", PriceCurrencyCode = "USD", LicenseRight = (int)LicenseRight.Purchase },
					new ApplicationOfferEntity { Id = 2, PaymentTypes = (int)PaymentTypes.CreditCard, Price = 0, DisplayPrice = "$0.00", PriceCurrencyCode = "USD", LicenseRight = (int)LicenseRight.Trial }
				},
				UserRatings = new List<ApplicationUserRatingStatisticsEntity>
				{
					new ApplicationUserRatingStatisticsEntity { DateTo = null }
				}
			};

			var changesRecepients = new ApplicationAttributesChangesRecepient(existedApplicationAttributesEntity);
			bool haveChanges = changesInjector(application, changesRecepients);

			IApplicationsEntitiesContext entitiesContext =
				ApplicationEntitiesContextToAddApplications(SomeRegion, application).
				WithApplications(new[] { existedApplicationEntity }).
				WithApplicationAttributes(new[] { existedApplicationAttributesEntity }).
				Object;

			IMarketplaceCrawler crawler = CreateCrawler(
				CreateMarketplaceReaderMock(new[] { application }, null).Object,
				entitiesContext
				);
			crawler.UpdateApplications(SomeRegion, "Zest", false, null);

			Mock.Get(entitiesContext.ApplicationAttributes).Verify(
				s => s.Add(It.Is<ApplicationAttributesEntity>(a =>
					a.Application.MarketplaceApplicationId == application.Id &&
					a.DateTo == null
					)),
				haveChanges ? Times.Once() : Times.Never(),
				failMessage
				);
		}

		#endregion
	}
}
