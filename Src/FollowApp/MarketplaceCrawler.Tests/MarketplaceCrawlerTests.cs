using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using FollowApp;
using FollowApp.DataAccess;
using FollowApp.DataAccess.Entities;
using FollowApp.Entities;
using FollowApp.MarketplaceCrawler;
using FollowApp.UnitTesting.Utils.DataAccess;

using Moq;

public sealed partial class MarketplaceCrawlerTests
{
	// Here in this file the common helpers for the MarketplaceCrawlerTests are coming

	private static MarketplaceCrawler CreateCrawler(
		IMarketplaceReader marketplaceReader,
		IApplicationsEntitiesContext entitiesContext,
		IDateTimeProvider dateTimeProvider
		)
	{
		return new MarketplaceCrawler(marketplaceReader, () => entitiesContext, dateTimeProvider);
	}

	private static MarketplaceCrawler CreateCrawler(
	IMarketplaceReader marketplaceReader,
	IApplicationsEntitiesContext entitiesContext
	)
	{
		return CreateCrawler(marketplaceReader, entitiesContext, new SystemDateTimeProvider());
	}

	private static MarketplaceRegion NullRegion
	{
		get { return null; }
	}

	private static string NullStore
	{
		get { return null; }
	}

	private static MarketplaceRegion SomeRegion
	{
		get { return new MarketplaceRegion("en-US"); }
	}

	private static Expression<Func<IMarketplaceReader, SearchApplicationsResult>>
		SearchApplicationsWithAnyQuery()
	{
		return r => r.SearchApplications(It.IsAny<SearchApplicationsQuery>());
	}

	private static Mock<IMarketplaceReader> CreateMarketplaceReaderMock()
	{
		return CreateMarketplaceReaderMock(new Application[] { }, null);
	}

	private static Mock<IMarketplaceReader> CreateMarketplaceReaderMock(
		IEnumerable<Application> applications,
		Func<SearchApplicationsResult> nextChunkGetter
		)
	{
		var mock = new Mock<IMarketplaceReader>();
		mock.
			Setup(SearchApplicationsWithAnyQuery()).
			Returns(new SearchApplicationsResult(applications, nextChunkGetter));
		return mock;
	}

	private class PublisherEntityEqualityComparer : IEqualityComparer<PublisherEntity>
	{
		public bool Equals(PublisherEntity x, PublisherEntity y)
		{
			if (x == null && y == null) return true;
			if (x == null) return false;
			if (y == null) return false;

			return x.Code == y.Code;
		}

		public int GetHashCode(PublisherEntity obj)
		{
			if (obj == null) return 0;
			return obj.Code.GetHashCode();
		}
	}

	private static ApplicationsEntitiesContextMockBuilder ApplicationEntitiesContextToAddApplications(
		MarketplaceRegion region,
		params Application[] applications
		)
	{
		var publisherComparer = new PublisherEntityEqualityComparer();
		return ApplicationsEntitiesContextMock.Create().
			WithMarketplaceRegions(new[] { new MarketplaceRegionEntity { Code = region.Code } }).
			WithPublishers(
				applications.
					Where(a => a.Publisher != null).
					Select(a => new PublisherEntity { Code = a.Publisher.Id }).
					Distinct(publisherComparer)
				);
	}

	private static Application GetDummyApplication(string id)
	{
		return new Application
		{
			Id = Guid.Parse(id),
			Publisher = new PublisherReference { Id = "", Name = "" },
			Title = "",
			SortTitle = "",
			Description = "",
			ReleaseDate = DateTime.UtcNow,
			Image = new ImageReference(Guid.NewGuid()),
			Version = "",
			Categories = new ApplicationCategory[] { },
			Offers = new ApplicationOffer[] { },
			Tags = new Tag[] { }
		};
	}

	private static Application DummyApplication
	{
		get { return GetDummyApplication("00000000-0000-0000-0000-000000000000"); }
	}
}
