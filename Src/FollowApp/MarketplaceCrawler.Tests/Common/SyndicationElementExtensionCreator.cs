using System;
using System.IO;
using System.ServiceModel.Syndication;
using System.Xml;

namespace FollowApp.MarketplaceCrawler.Tests.Common
{
	public static class SyndicationElementExtensionCreator
	{
		public static SyndicationElementExtension FromXml([NotNull] string xml)
		{
			if (xml == null) throw new ArgumentNullException("xml");

			using (var stringReader = new StringReader(xml))
			using (var xmlReader = XmlReader.Create(stringReader))
			{
				return new SyndicationElementExtension(xmlReader);
			}
		}

		public static SyndicationElementExtension FromXmlElement(
			[NotNull] string elementNamespace,
			[NotNull] string elementName,
			[NotNull] string innerXml
			)
		{
			if (elementNamespace == null) throw new ArgumentNullException("elementNamespace");
			if (elementName == null) throw new ArgumentNullException("elementName");
			if (innerXml == null) throw new ArgumentNullException("innerXml");
			
			var xml = string.Format("<{1} xmlns=\"{0}\">{2}</{1}>", elementNamespace, elementName, innerXml);
			return FromXml(xml);
		}
	}
}
