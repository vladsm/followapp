using System;
using System.IO;
using System.Linq;
using System.Xml;

using FollowApp.Entities;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class MarketplaceReaderTests
	{
		[Test(Description = "SearchApplication returns data correctly")]
		public void searchapplication_returns_data_correctly()
		{
			#region Mock xml

			const string xml =
				@"<?xml version=""1.0"" encoding=""utf-8""?>
<a:feed xmlns:a=""http://www.w3.org/2005/Atom"" xmlns:os=""http://a9.com/-/spec/opensearch/1.1/"" xmlns=""http://schemas.zune.net/catalog/apps/2008/02"">
	<a:link rel=""prev"" type=""application/atom+xml"" href=""/v3.2/en-US/apps?beforeMarker=AAAAAAE%3d&amp;orderBy=releaseDate&amp;store=Zest&amp;clientType=WinMobile+7.1"" />
	<a:link rel=""next"" type=""application/atom+xml"" href=""/v3.2/en-US/apps?afterMarker=YwAAAAE%3d&amp;orderBy=releaseDate&amp;store=Zest&amp;clientType=WinMobile+7.1"" />
	<a:link rel=""self"" type=""application/atom+xml"" href=""/v3.2/en-US/apps?orderBy=releaseDate&amp;store=Zest&amp;clientType=WinMobile+7.1"" />
	<a:updated>2011-11-20T06:38:35.7777355Z</a:updated>
	<a:title type=""text"">List Of Items</a:title>
	<a:id>tag:catalog.zune.net,2011-11-20:/apps</a:id>
	<a:entry>
		<a:updated>2011-11-20T06:38:35.7777355Z</a:updated>
		<a:title type=""text"">FreeBox</a:title>
		<a:id>urn:uuid:a71b79cd-7a56-4d82-be82-02e85c674958</a:id>
		<sortTitle>FreeBox</sortTitle>
		<releaseDate>2011-11-20T01:45:49.997Z</releaseDate>
		<shortDescription>FreeBox</shortDescription>
		<version>1.0.0.0</version>
		<averageUserRating>0</averageUserRating>
		<userRatingCount>0</userRatingCount>
		<averageLastInstanceUserRating>0</averageLastInstanceUserRating>
		<lastInstanceUserRatingCount>0</lastInstanceUserRatingCount>
		<image>
			<id>urn:uuid:66ba5ca8-02f0-4485-950e-99fd5a956de5</id>
		</image>
		<categories>
			<category>
				<id>windowsphone.entertainment</id>
				<title>entertainment</title>
				<isRoot>True</isRoot>
			</category>
		</categories>
		<tags>
			<tag>apptag.independent</tag>
		</tags>
		<offers>
			<offer>
				<offerId>urn:uuid:75d2b476-c351-4855-bc04-55037cd5aa3b</offerId>
				<mediaInstanceId>urn:uuid:4b47a9ff-dc92-4b9d-ad7e-605d228d2ebe</mediaInstanceId>
				<clientTypes>
					<clientType>WinMobile 7.1</clientType>
				</clientTypes>
				<paymentTypes>
					<paymentType>Credit Card</paymentType>
					<paymentType>Mobile Operator</paymentType>
				</paymentTypes>
				<store>Zest</store>
				<price>0</price>
				<displayPrice>$0.00</displayPrice>
				<priceCurrencyCode>USD</priceCurrencyCode>
				<licenseRight>Trial</licenseRight>
			</offer>
			<offer>
				<offerId>urn:uuid:1d91f6bd-212b-487d-9cb4-97e64e430705</offerId>
				<mediaInstanceId>urn:uuid:4b47a9ff-dc92-4b9d-ad7e-605d228d2ebe</mediaInstanceId>
				<clientTypes>
					<clientType>WinMobile 7.1</clientType>
				</clientTypes>
				<paymentTypes>
					<paymentType>Credit Card</paymentType>
					<paymentType>Mobile Operator</paymentType>
				</paymentTypes>
				<store>Zest</store>
				<price>0.99</price>
				<displayPrice>$0.99</displayPrice>
				<priceCurrencyCode>USD</priceCurrencyCode>
				<licenseRight>Purchase</licenseRight>
			</offer>
		</offers>
		<publisher>
			<id>nicoseles</id>
			<name>nicoseles</name>
		</publisher>
	</a:entry>
	<a:entry>
		<a:updated>2011-11-20T06:38:35.7777355Z</a:updated>
		<a:title type=""text"">Visto Americano</a:title>
		<a:id>urn:uuid:17164de3-0ac0-49f1-9da7-02f7b3f58f0e</a:id>
		<sortTitle>Visto Americano</sortTitle>
		<releaseDate>2011-11-20T01:45:50.277Z</releaseDate>
		<shortDescription>Tudo para tirar o visto</shortDescription>
		<version>1.0.0.0</version>
		<averageUserRating>0</averageUserRating>
		<userRatingCount>0</userRatingCount>
		<averageLastInstanceUserRating>0</averageLastInstanceUserRating>
		<lastInstanceUserRatingCount>0</lastInstanceUserRatingCount>
		<image>
			<id>urn:uuid:990da79f-3e2d-4682-b4e4-35f78ffe1c1b</id>
		</image>
		<categories>
			<category>
				<id>windowsphone.travel</id>
				<title>travel + navigation</title>
				<isRoot>True</isRoot>
			</category>
			<category>
				<id>windowsphone.traveltools</id>
				<title>travel tools</title>
				<isRoot>False</isRoot>
				<parentId>windowsphone.travel</parentId>
			</category>
		</categories>
		<tags>
			<tag>apptag.independent</tag>
		</tags>
		<offers>
			<offer>
				<offerId>urn:uuid:da36e85b-e3b6-4ffb-bdfb-deabc909e519</offerId>
				<mediaInstanceId>urn:uuid:168f5c1f-046b-4995-99c6-ce2eb8510e5f</mediaInstanceId>
				<clientTypes>
					<clientType>WinMobile 7.1</clientType>
				</clientTypes>
				<paymentTypes>
					<paymentType>Credit Card</paymentType>
					<paymentType>Mobile Operator</paymentType>
				</paymentTypes>
				<store>Zest</store>
				<price>0</price>
				<displayPrice>$0.00</displayPrice>
				<priceCurrencyCode>USD</priceCurrencyCode>
				<licenseRight>Purchase</licenseRight>
			</offer>
		</offers>
		<publisher>
			<id>Samuel Liques</id>
			<name>Samuel Liques</name>
		</publisher>
	</a:entry>
	<a:author>
		<a:name>Microsoft Corporation</a:name>
	</a:author>
</a:feed>
";
			const string moreXml =
				@"<?xml version=""1.0"" encoding=""utf-8""?>
<a:feed xmlns:a=""http://www.w3.org/2005/Atom"" xmlns:os=""http://a9.com/-/spec/opensearch/1.1/"" xmlns=""http://schemas.zune.net/catalog/apps/2008/02"">
	<a:link rel=""prev"" type=""application/atom+xml"" href=""/v3.2/en-US/apps?beforeMarker=AAAAAAE%3d&amp;orderBy=releaseDate&amp;store=Zest&amp;clientType=WinMobile+7.1"" />
	<a:link rel=""self"" type=""application/atom+xml"" href=""/v3.2/en-US/apps?orderBy=releaseDate&amp;store=Zest&amp;clientType=WinMobile+7.1"" />
	<a:updated>2011-11-20T06:38:35.7777355Z</a:updated>
	<a:title type=""text"">List Of Items</a:title>
	<a:id>tag:catalog.zune.net,2011-11-20:/apps</a:id>
	<a:entry>
		<a:updated>2011-11-20T06:38:35.7777355Z</a:updated>
		<a:title type=""text"">FreeBox</a:title>
		<a:id>urn:uuid:a71b79cd-7a56-4d82-be82-02e85c674958</a:id>
		<sortTitle>FreeBox</sortTitle>
		<releaseDate>2011-11-20T01:45:49.997Z</releaseDate>
		<shortDescription>FreeBox</shortDescription>
		<version>1.0.0.0</version>
		<averageUserRating>0</averageUserRating>
		<userRatingCount>0</userRatingCount>
		<averageLastInstanceUserRating>0</averageLastInstanceUserRating>
		<lastInstanceUserRatingCount>0</lastInstanceUserRatingCount>
		<image>
			<id>urn:uuid:66ba5ca8-02f0-4485-950e-99fd5a956de5</id>
		</image>
		<categories>
			<category>
				<id>windowsphone.entertainment</id>
				<title>entertainment</title>
				<isRoot>True</isRoot>
			</category>
		</categories>
		<tags>
			<tag>apptag.independent</tag>
		</tags>
		<offers>
			<offer>
				<offerId>urn:uuid:75d2b476-c351-4855-bc04-55037cd5aa3b</offerId>
				<mediaInstanceId>urn:uuid:4b47a9ff-dc92-4b9d-ad7e-605d228d2ebe</mediaInstanceId>
				<clientTypes>
					<clientType>WinMobile 7.1</clientType>
				</clientTypes>
				<paymentTypes>
					<paymentType>Credit Card</paymentType>
					<paymentType>Mobile Operator</paymentType>
				</paymentTypes>
				<store>Zest</store>
				<price>0</price>
				<displayPrice>$0.00</displayPrice>
				<priceCurrencyCode>USD</priceCurrencyCode>
				<licenseRight>Trial</licenseRight>
			</offer>
			<offer>
				<offerId>urn:uuid:1d91f6bd-212b-487d-9cb4-97e64e430705</offerId>
				<mediaInstanceId>urn:uuid:4b47a9ff-dc92-4b9d-ad7e-605d228d2ebe</mediaInstanceId>
				<clientTypes>
					<clientType>WinMobile 7.1</clientType>
				</clientTypes>
				<paymentTypes>
					<paymentType>Credit Card</paymentType>
					<paymentType>Mobile Operator</paymentType>
				</paymentTypes>
				<store>Zest</store>
				<price>0.99</price>
				<displayPrice>$0.99</displayPrice>
				<priceCurrencyCode>USD</priceCurrencyCode>
				<licenseRight>Purchase</licenseRight>
			</offer>
		</offers>
		<publisher>
			<id>nicoseles</id>
			<name>nicoseles</name>
		</publisher>
	</a:entry>
	<a:entry>
		<a:updated>2011-11-20T06:38:35.7777355Z</a:updated>
		<a:title type=""text"">Visto Americano</a:title>
		<a:id>urn:uuid:17164de3-0ac0-49f1-9da7-02f7b3f58f0e</a:id>
		<sortTitle>Visto Americano</sortTitle>
		<releaseDate>2011-11-20T01:45:50.277Z</releaseDate>
		<shortDescription>Tudo para tirar o visto</shortDescription>
		<version>1.0.0.0</version>
		<averageUserRating>0</averageUserRating>
		<userRatingCount>0</userRatingCount>
		<averageLastInstanceUserRating>0</averageLastInstanceUserRating>
		<lastInstanceUserRatingCount>0</lastInstanceUserRatingCount>
		<image>
			<id>urn:uuid:990da79f-3e2d-4682-b4e4-35f78ffe1c1b</id>
		</image>
		<categories>
			<category>
				<id>windowsphone.travel</id>
				<title>travel + navigation</title>
				<isRoot>True</isRoot>
			</category>
			<category>
				<id>windowsphone.traveltools</id>
				<title>travel tools</title>
				<isRoot>False</isRoot>
				<parentId>windowsphone.travel</parentId>
			</category>
		</categories>
		<tags>
			<tag>apptag.independent</tag>
		</tags>
		<offers>
			<offer>
				<offerId>urn:uuid:da36e85b-e3b6-4ffb-bdfb-deabc909e519</offerId>
				<mediaInstanceId>urn:uuid:168f5c1f-046b-4995-99c6-ce2eb8510e5f</mediaInstanceId>
				<clientTypes>
					<clientType>WinMobile 7.1</clientType>
				</clientTypes>
				<paymentTypes>
					<paymentType>Credit Card</paymentType>
					<paymentType>Mobile Operator</paymentType>
				</paymentTypes>
				<store>Zest</store>
				<price>0</price>
				<displayPrice>$0.00</displayPrice>
				<priceCurrencyCode>USD</priceCurrencyCode>
				<licenseRight>Purchase</licenseRight>
			</offer>
		</offers>
		<publisher>
			<id>Samuel Liques</id>
			<name>Samuel Liques</name>
		</publisher>
	</a:entry>
	<a:author>
		<a:name>Microsoft Corporation</a:name>
	</a:author>
</a:feed>
";
			#endregion

			var reader = CreateMarketplaceReaderForXmlContent(xml, moreXml);
			SearchApplicationsResult result = reader.SearchApplications(
				new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile71)
				);

			Assert.That(result.Applications.Count(), Is.EqualTo(2));
			Assert.That(result.HasMore, Is.True);

			result = result.Next();
			Assert.That(result.Applications.Count(), Is.EqualTo(2));
			Assert.That(result.HasMore, Is.False);
		}


		private static MarketplaceReader CreateMarketplaceReaderForXmlContent(string xml, string moreXml)
		{
			return new MarketplaceReader(new MarketplaceQueryUriBuilder(), new ContentLoaderMock(xml, moreXml));
		}

		private static XmlReader GetXmlReader(string xml)
		{
			var stringReader = new StringReader(xml);
			return XmlReader.Create(stringReader);
		}

		private class ContentLoaderMock : ContentLoader
		{
			private readonly string _resultXml;
			private readonly string _moreResultXml;

			public override XmlReader LoadXmlContent(Uri uri)
			{
				if (uri.AbsoluteUri == "http://catalog.zune.net/v3.2/en-US/apps?afterMarker=YwAAAAE%3d&orderBy=releaseDate&store=Zest&clientType=WinMobile+7.1")
				{
					return GetXmlReader(_moreResultXml);
				}
				return GetXmlReader(_resultXml);
			}

			public ContentLoaderMock(string resultXml, string moreResultXml)
			{
				_resultXml = resultXml;
				_moreResultXml = moreResultXml;
			}
		}
	}
}
