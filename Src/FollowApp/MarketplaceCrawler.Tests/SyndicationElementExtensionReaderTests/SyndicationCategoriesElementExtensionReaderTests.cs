using System.Linq;

using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationCategoriesElementExtensionReaderTests
	{
		[Test(Description = "Readers reads empty categories collection")]
		public void readers_reads_empty_categories_collection()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "categories", "");
			var reader = new SyndicationCategoriesElementExtensionReader();

			var categories = reader.Read(see);
			Assert.That(categories, Is.Not.Null);
			Assert.That(categories, Is.Empty);
		}

		[Test(Description = "Readers reads non empty categories collection")]
		public void readers_reads_non_empty_categories_collection()
		{
			const string categoriesXml =
				"<category>" +
					"<id>windowsphone.travel</id>" +
					"<title>travel and navigation</title>" +
					"<isRoot>True</isRoot>" +
				"</category>"+
				"<category>" +
					"<id>windowsphone.navigation</id>" +
					"<title>navigation</title>" +
					"<isRoot>False</isRoot>" +
					"<parentId>windowsphone.travel</parentId>" +
				"</category>";
			var see = SyndicationElementExtensionCreator.FromXmlElement("tempNs", "categories", categoriesXml);
			var reader = new SyndicationCategoriesElementExtensionReader();

			var categories = reader.Read(see);
			
			Assert.That(categories, Is.Not.Null);
			Assert.That(categories, Has.Count.EqualTo(2));

			var category = categories.FirstOrDefault(c => c.Id == "windowsphone.travel");
			Assert.That(category, Is.Not.Null);
			Assert.That(category.Title, Is.EqualTo("travel and navigation"));
			Assert.That(category.ParentId, Is.Null);

			category = categories.FirstOrDefault(c => c.Id == "windowsphone.navigation");
			Assert.That(category, Is.Not.Null);
			Assert.That(category.Title, Is.EqualTo("navigation"));
			Assert.That(category.ParentId, Is.EqualTo("windowsphone.travel"));
		}

		[Test(Description = "Reader throws exception for the incorrect category element")]
		public void reader_throws_exception_for_the_incorrect_category_element()
		{
			const string categoriesXml =
				"<category>" +
					"<non_id>windowsphone.navigation</non_id>" +
					"<title>navigation</title>" +
					"<isRoot>False</isRoot>" +
					"<parentId>windowsphone.travel</parentId>" +
				"</category>";
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "categories", categoriesXml);
			var reader = new SyndicationCategoriesElementExtensionReader();

			Assert.That(
				() => reader.Read(see),
				Throws.Exception.InstanceOf<MarketplaceSyndicationParsingException>()
				);
		}
	}
}
