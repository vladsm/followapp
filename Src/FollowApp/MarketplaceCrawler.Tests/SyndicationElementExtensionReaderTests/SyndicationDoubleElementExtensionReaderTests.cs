using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationDoubleElementExtensionReaderTests
	{
		[Test(Description = "Reader reads double numbers")]
		public void reader_reads_double_numbers()
		{
			var see1 = SyndicationElementExtensionCreator.FromXmlElement("", "someDouble", "10");
			var see2 = SyndicationElementExtensionCreator.FromXmlElement("", "someDouble", "-10");
			var see3 = SyndicationElementExtensionCreator.FromXmlElement("", "someDouble", "3.14159265");
			var see4 = SyndicationElementExtensionCreator.FromXmlElement("", "someDouble", "-3.14159265");
			var reader = new SyndicationDecimalElementExtensionReader();

			Assert.That(reader.Read(see1), Is.EqualTo(10));
			Assert.That(reader.Read(see2), Is.EqualTo(-10));
			Assert.That(reader.Read(see3), Is.EqualTo(3.14159265));
			Assert.That(reader.Read(see4), Is.EqualTo(-3.14159265));
		}
	}
}
