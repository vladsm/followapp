using System;
using System.Linq;

using FollowApp.Entities;
using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationOffersElementExtensionReaderTests
	{
		[Test(Description = "Readers reads empty offers collection")]
		public void readers_reads_empty_offers_collection()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "offers", "");
			var reader = new SyndicationOffersElementExtensionReader();

			var offers = reader.Read(see);
			Assert.That(offers, Is.Not.Null);
			Assert.That(offers, Is.Empty);
		}

		[Test(Description = "Readers reads non empty offers collection")]
		public void readers_reads_non_empty_offers_collection()
		{
			const string offersXml =
				"<offer>" +
					"<offerId>urn:uuid:c64e0000-9917-4034-840d-e6c09bf00000</offerId>" +
					"<mediaInstanceId>urn:uuid:6c3f0000-490c-4ab5-989f-867966aa0000</mediaInstanceId>" +
					"<clientTypes>" +
						"<clientType>WinMobile 7.1</clientType>" +
					"</clientTypes>" +
					"<paymentTypes>" +
						"<paymentType>Credit Card</paymentType>" +
						"<paymentType>Mobile Operator</paymentType>" +
					"</paymentTypes>" +
					"<store>Zest</store>" +
					"<price>0.99</price>" +
					"<displayPrice>$0.99</displayPrice>" +
					"<priceCurrencyCode>USD</priceCurrencyCode>" +
					"<licenseRight>Purchase</licenseRight>" +
				"</offer>" +
				"<offer>" +
					"<offerId>urn:uuid:c64e0000-9917-4034-840d-e6c09bf00001</offerId>" +
					"<mediaInstanceId>urn:uuid:6c3f0000-490c-4ab5-989f-867966aa0001</mediaInstanceId>" +
					"<clientTypes>" +
						"<clientType>WinMobile 7.1</clientType>" +
					"</clientTypes>" +
					"<paymentTypes>" +
						"<paymentType>Credit Card</paymentType>" +
					"</paymentTypes>" +
					"<store>Zest</store>" +
					"<price>0.99</price>" +
					"<displayPrice>$0.99</displayPrice>" +
					"<priceCurrencyCode>USD</priceCurrencyCode>" +
					"<licenseRight>Trial</licenseRight>" +
				"</offer>";
			var see = SyndicationElementExtensionCreator.FromXmlElement("tempNs", "offers", offersXml);
			var reader = new SyndicationOffersElementExtensionReader();

			var offers = reader.Read(see).ToArray();
			
			Assert.That(offers, Is.Not.Null);
			Assert.That(offers, Has.Length.EqualTo(2));

			var offer = offers.FirstOrDefault(c => c.Id == Guid.Parse("c64e0000-9917-4034-840d-e6c09bf00000"));
			Assert.That(offer, Is.Not.Null);
			if (offer == null) return;
			Assert.That(offer.MediaInstanceId, Is.EqualTo(Guid.Parse("6c3f0000-490c-4ab5-989f-867966aa0000")));
			Assert.That(offer.ClientTypes, Is.EquivalentTo(new[] { MarketplaceClientType.WinMobile71 }));
			Assert.That(offer.PaymentTypes, Is.EqualTo(PaymentTypes.CreditCard | PaymentTypes.MobileOperator));
			Assert.That(offer.Store, Is.EqualTo("Zest"));
			Assert.That(offer.Price, Is.EqualTo(0.99));
			Assert.That(offer.DisplayPrice, Is.EqualTo("$0.99"));
			Assert.That(offer.PriceCurrencyCode, Is.EqualTo("USD"));
			Assert.That(offer.LicenseRight, Is.EqualTo(LicenseRight.Purchase));

			offer = offers.FirstOrDefault(c => c.Id == Guid.Parse("c64e0000-9917-4034-840d-e6c09bf00001"));
			Assert.That(offer, Is.Not.Null);
			if (offer == null) return;
			Assert.That(offer.MediaInstanceId, Is.EqualTo(Guid.Parse("6c3f0000-490c-4ab5-989f-867966aa0001")));
			Assert.That(offer.ClientTypes, Is.EquivalentTo(new[] { MarketplaceClientType.WinMobile71 }));
			Assert.That(offer.PaymentTypes, Is.EqualTo(PaymentTypes.CreditCard));
			Assert.That(offer.Store, Is.EqualTo("Zest"));
			Assert.That(offer.Price, Is.EqualTo(0.99));
			Assert.That(offer.DisplayPrice, Is.EqualTo("$0.99"));
			Assert.That(offer.PriceCurrencyCode, Is.EqualTo("USD"));
			Assert.That(offer.LicenseRight, Is.EqualTo(LicenseRight.Trial));
		}

		[Test(Description = "Reader throws exception for the incorrect paymentType in the offer element")]
		public void reader_throws_exception_for_the_incorrect_paymenttype_in_the_offer_element()
		{
			const string offersXml =
				"<offer>" +
					"<offerId>urn:uuid:c64e0000-9917-4034-840d-e6c09bf00000</offerId>" +
					"<mediaInstanceId>urn:uuid:6c3f0000-490c-4ab5-989f-867966aa0000</mediaInstanceId>" +
					"<clientTypes>" +
						"<clientType>WinMobile 7.1</clientType>" +
					"</clientTypes>" +
					"<paymentTypes>" +
						"<paymentType>Credit Card</paymentType>" +
						"<paymentType>Mobile Operator 1</paymentType>" +
					"</paymentTypes>" +
					"<store>Zest</store>" +
					"<price>0.99</price>" +
					"<displayPrice>$0.99</displayPrice>" +
					"<priceCurrencyCode>USD</priceCurrencyCode>" +
					"<licenseRight>Purchase</licenseRight>" +
				"</offer>";
			var see = SyndicationElementExtensionCreator.FromXmlElement("tempNs", "offers", offersXml);
			var reader = new SyndicationOffersElementExtensionReader();

			Assert.That(
				() => reader.Read(see),
				Throws.Exception.InstanceOf<MarketplaceSyndicationParsingException>().And.
					Message.EqualTo("Can not convert string \"Mobile Operator 1\" to PaymentTypes enum value.")
				);
		}

		[Test(Description = "Reader throws exception for the incorrect licenseRight in the offer element")]
		public void reader_throws_exception_for_the_incorrect_licenseright_in_the_offer_element()
		{
			const string offersXml =
				"<offer>" +
					"<offerId>urn:uuid:c64e0000-9917-4034-840d-e6c09bf00000</offerId>" +
					"<mediaInstanceId>urn:uuid:6c3f0000-490c-4ab5-989f-867966aa0000</mediaInstanceId>" +
					"<clientTypes>" +
						"<clientType>WinMobile 7.1</clientType>" +
					"</clientTypes>" +
					"<paymentTypes>" +
						"<paymentType>Credit Card</paymentType>" +
						"<paymentType>Mobile Operator</paymentType>" +
					"</paymentTypes>" +
					"<store>Zest</store>" +
					"<price>0.99</price>" +
					"<displayPrice>$0.99</displayPrice>" +
					"<priceCurrencyCode>USD</priceCurrencyCode>" +
					"<licenseRight>Purchase 1</licenseRight>" +
				"</offer>";
			var see = SyndicationElementExtensionCreator.FromXmlElement("tempNs", "offers", offersXml);
			var reader = new SyndicationOffersElementExtensionReader();

			Assert.That(
				() => reader.Read(see),
				Throws.Exception.InstanceOf<MarketplaceSyndicationParsingException>().And.
					Message.EqualTo("Can not convert string \"Purchase 1\" to LicenseRight enum value.")
				);
		}
	}
}
