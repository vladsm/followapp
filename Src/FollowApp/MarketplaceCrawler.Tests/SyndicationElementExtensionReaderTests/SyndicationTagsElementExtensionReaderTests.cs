using System.Linq;

using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationTagsElementExtensionReaderTests
	{
		[Test(Description = "Readers reads empty tags collection")]
		public void readers_reads_empty_tags_collection()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "tags", "");
			var reader = new SyndicationTagsElementExtensionReader();

			var tags = reader.Read(see);
			Assert.That(tags, Is.Not.Null);
			Assert.That(tags, Is.Empty);
		}

		[Test(Description = "Readers reads non empty tags collection")]
		public void readers_reads_non_empty_tags_collection()
		{
			const string tagsXml =
				"<tag>tag1</tag>" +
				"<tag>tag2</tag>" +
				"<tag>tag3</tag>";
			var see = SyndicationElementExtensionCreator.FromXmlElement("tempNs", "tags", tagsXml);
			var reader = new SyndicationTagsElementExtensionReader();

			var tags = reader.Read(see);
			
			Assert.That(tags, Is.Not.Null);
			Assert.That(tags, Has.Count.EqualTo(3));
			Assert.That(tags.Select(tag => tag.Id), Is.EquivalentTo(new[] {"tag1", "tag2", "tag3"}));
		}
	}
}
