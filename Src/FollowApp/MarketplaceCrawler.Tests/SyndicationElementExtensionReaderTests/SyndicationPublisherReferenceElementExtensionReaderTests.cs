using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationPublisherReferenceElementExtensionReaderTests
	{
		[Test(Description = "Readers reads publisher reference attributes")]
		public void readers_reads_publisher_reference_attributes()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("tempNs", "publisher", "<id>jsmith</id><name>John Smith</name>");
			var reader = new SyndicationPublisherReferenceElementExtensionReader();

			var publisherReference = reader.Read(see);

			Assert.That(publisherReference, Is.Not.Null);
			Assert.That(publisherReference.Id, Is.EqualTo("jsmith"));
			Assert.That(publisherReference.Name, Is.EqualTo("John Smith"));
		}

		[Test(Description = "Reader throws exception for the incorrect publisher reference element")]
		public void reader_throws_exception_for_the_incorrect_publisher_reference_element()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "publisher", "<nonid>jsmith</nonid><nonname>John Smith</nonname>");
			var reader = new SyndicationPublisherReferenceElementExtensionReader();

			Assert.That(
				() => reader.Read(see),
				Throws.Exception.InstanceOf<MarketplaceSyndicationParsingException>()
				);
		}
	}
}
