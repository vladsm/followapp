using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationDecimalElementExtensionReaderTests
	{
		[Test(Description = "Reader reads decimal numbers")]
		public void reader_reads_decimal_numbers()
		{
			var see1 = SyndicationElementExtensionCreator.FromXmlElement("", "someDecimal", "10");
			var see2 = SyndicationElementExtensionCreator.FromXmlElement("", "someDecimal", "-10");
			var see3 = SyndicationElementExtensionCreator.FromXmlElement("", "someDecimal", "3.14159265");
			var see4 = SyndicationElementExtensionCreator.FromXmlElement("", "someDecimal", "-3.14159265");
			var reader = new SyndicationDecimalElementExtensionReader();

			Assert.That(reader.Read(see1), Is.EqualTo(10));
			Assert.That(reader.Read(see2), Is.EqualTo(-10));
			Assert.That(reader.Read(see3), Is.EqualTo(3.14159265));
			Assert.That(reader.Read(see4), Is.EqualTo(-3.14159265));
		}
	}
}
