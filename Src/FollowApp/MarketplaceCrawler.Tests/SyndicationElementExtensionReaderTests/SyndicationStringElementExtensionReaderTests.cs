using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationStringElementExtensionReaderTests
	{
		[Test(Description = "Reader reads string content")]
		public void reader_reads_string_content()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "someString", "The string content");
			var reader = new SyndicationStringElementExtensionReader();

			Assert.That(reader.Read(see), Is.EqualTo("The string content"));
		}

		[Test(Description = "Reader reads empty string content")]
		public void reader_reads_empty_string_content()
		{
			var see = SyndicationElementExtensionCreator.FromXml("<someString></someString>");
			var reader = new SyndicationStringElementExtensionReader();

			Assert.That(reader.Read(see), Is.Empty);
		}
	}
}
