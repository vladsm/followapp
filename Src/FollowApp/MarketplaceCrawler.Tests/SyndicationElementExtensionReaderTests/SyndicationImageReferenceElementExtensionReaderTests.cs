using System;

using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationImageReferenceElementExtensionReaderTests
	{
		[Test(Description = "Readers reads image reference attributes")]
		public void readers_reads_image_reference_attributes()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("tempNs", "image", "<id>urn:uuid:71114000-62a0-4678-bb79-4c6071fc0000</id>");
			var reader = new SyndicationImageReferenceElementExtensionReader();

			var imageReference = reader.Read(see);

			Assert.That(imageReference, Is.Not.Null);
			Assert.That(imageReference.Id, Is.EqualTo(Guid.Parse("71114000-62a0-4678-bb79-4c6071fc0000")));
		}

		[Test(Description = "Reader throws exception for the incorrect image reference element")]
		public void reader_throws_exception_for_the_incorrect_image_reference_element()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "image", "<nonid>urn:uuid:71114000-62a0-4678-bb79-4c6071fc0000</nonid>");
			var reader = new SyndicationImageReferenceElementExtensionReader();

			Assert.That(
				() => reader.Read(see),
				Throws.Exception.InstanceOf<MarketplaceSyndicationParsingException>()
				);
		}
	}
}
