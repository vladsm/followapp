using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationInt32ElementExtensionReaderTests
	{
		[Test(Description = "Reader reads Int32 numbers")]
		public void reader_reads_int32_numbers()
		{
			var see1 = SyndicationElementExtensionCreator.FromXmlElement("", "someInt32", "10");
			var see2 = SyndicationElementExtensionCreator.FromXmlElement("", "someInt32", "-10");
			var reader = new SyndicationInt32ElementExtensionReader();

			Assert.That(reader.Read(see1), Is.EqualTo(10));
			Assert.That(reader.Read(see2), Is.EqualTo(-10));
		}
	}
}
