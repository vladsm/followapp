using System;

using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class SyndicationDateTimeElementExtensionReaderTests
	{
		[Test(Description = "Reader reads datetime content")]
		public void reader_reads_datetime_content()
		{
			var see = SyndicationElementExtensionCreator.FromXmlElement("", "someDatetime", "2011-11-20T01:45:49.997Z");
			var reader = new SyndicationDateTimeElementExtensionReader();

			Assert.That(reader.Read(see), Is.EqualTo(new DateTime(2011, 11, 20, 01, 45, 49, 997, DateTimeKind.Utc)));
		}
	}
}
