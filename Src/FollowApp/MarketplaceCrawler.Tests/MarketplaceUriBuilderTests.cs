using System;

using FollowApp.Entities;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class MarketplaceUriBuilderTests
	{
		[Test(Description = "Builder creates region specific search applications query URI")]
		public void builder_creates_region_specific_search_applications_query_uri()
		{
			var query = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70);
			var builder = new MarketplaceQueryUriBuilder();
			var uri = builder.GetUri(query);

			Assert.That(uri.GetLeftPart(UriPartial.Path), Is.EqualTo("http://catalog.zune.net/v3.2/en-US/apps"));
		}

		[Test(Description = "Builder creates search applications query URI with client type restriction")]
		public void builder_creates_search_applications_query_uri_with_client_type_restriction()
		{
			var builder = new MarketplaceQueryUriBuilder();
			var query1 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70);
			var query2 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile71);

			AssertThatQueryContainsParameter(builder.GetUri(query1), "clientType", "WinMobile 7.0");
			AssertThatQueryContainsParameter(builder.GetUri(query2), "clientType", "WinMobile 7.1");
		}

		[Test(Description = "Builder creates search applications query URI with store restriction")]
		public void builder_creates_search_applications_query_uri_with_store_restriction()
		{
			var builder = new MarketplaceQueryUriBuilder();
			var query1 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70);
			var query2 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				Store = "Zest"
			};

			AssertThatQueryDoesntContainParameter(builder.GetUri(query1), "store");
			AssertThatQueryContainsParameter(builder.GetUri(query2), "store", "Zest");
		}

		[Test(Description = "Builder creates search applications query URI with by ReleaseDate sort order")]
		public void builder_creates_search_applications_query_uri_with_by_releasedate_sort_order()
		{
			var builder = new MarketplaceQueryUriBuilder();
			var query1 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70);
			var query2 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				SortOrder = SearchApplicationsSortOrder.None
			};
			var query3 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				SortOrder = SearchApplicationsSortOrder.LastReleasedGoesFirst
			};

			AssertThatQueryDoesntContainParameter(builder.GetUri(query1), "orderby");
			AssertThatQueryDoesntContainParameter(builder.GetUri(query2), "orderby");
			AssertThatQueryContainsParameter(builder.GetUri(query3), "orderby", "releaseDate");
		}

		[Test(Description = "Builder creates search applications query URI with by DownloadRank sort order")]
		public void builder_creates_search_applications_query_uri_with_by_downloadrank_sort_order()
		{
			var builder = new MarketplaceQueryUriBuilder();
			var query1 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70);
			var query2 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				SortOrder = SearchApplicationsSortOrder.None
			};
			var query3 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				SortOrder = SearchApplicationsSortOrder.TopDownloadRankedGoesFirst
			};

			AssertThatQueryDoesntContainParameter(builder.GetUri(query1), "orderby");
			AssertThatQueryDoesntContainParameter(builder.GetUri(query2), "orderby");
			AssertThatQueryContainsParameter(builder.GetUri(query3), "orderby", "downloadRank");
		}


		[Test(Description = "Builder creates search applications query URI with application name filter")]
		public void builder_creates_search_applications_query_uri_with_application_name_filter()
		{
			var builder = new MarketplaceQueryUriBuilder();
			var query1 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70);
			var query2 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				SearchString = "app1"
			};

			AssertThatQueryDoesntContainParameter(builder.GetUri(query1), "q");
			AssertThatQueryContainsParameter(builder.GetUri(query2), "q", "app1");
		}

		[Test(Description = "Builder creates search applications query URI with chunk size parameter")]
		public void builder_creates_search_applications_query_uri_with_chunk_size_parameter()
		{
			var builder = new MarketplaceQueryUriBuilder();
			var query1 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70);
			var query2 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				ChunkSize = SearchApplicationsChunkSize.Default
			};
			var query3 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				ChunkSize = SearchApplicationsChunkSize.Ten
			};
			var query4 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				ChunkSize = SearchApplicationsChunkSize.Fifty
			};
			var query5 = new SearchApplicationsQuery(new MarketplaceRegion("en-US"), MarketplaceClientType.WinMobile70)
			{
				ChunkSize = SearchApplicationsChunkSize.Hundred
			};

			AssertThatQueryDoesntContainParameter(builder.GetUri(query1), "chunksize");
			AssertThatQueryDoesntContainParameter(builder.GetUri(query2), "chunksize");
			AssertThatQueryContainsParameter(builder.GetUri(query3), "chunksize", "10");
			AssertThatQueryContainsParameter(builder.GetUri(query4), "chunksize", "50");
			AssertThatQueryContainsParameter(builder.GetUri(query5), "chunksize", "100");
		}


		private static void AssertThatQueryContainsParameter(Uri uri, string parameterName, string parameterValue)
		{
			string[] parts = uri.GetComponents(UriComponents.Query, UriFormat.Unescaped).Split('&');
			Assert.That(parts, Contains.Item(parameterName + "=" + parameterValue));
		}

		private static void AssertThatQueryDoesntContainParameter(Uri uri, string parameterName)
		{
			string[] parts = uri.GetComponents(UriComponents.Query, UriFormat.Unescaped).Split('&');
			Assert.That(parts, Is.All.Not.StartsWith(parameterName + "=").IgnoreCase.And.Not.EqualTo(parameterName).IgnoreCase);
		}
	}
}
