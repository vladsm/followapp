﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FollowApp.MarketplaceCrawler.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vladimir Smirnov")]
[assembly: AssemblyProduct("FollowApp")]
[assembly: AssemblyCopyright("Copyright © Vladimir Smirnov (vladsm@gmail.com) 2011")]
[assembly: AssemblyTrademark("FollowApp")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("f9ec6a81-36b5-4f2a-80a4-61c408d1331a")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
