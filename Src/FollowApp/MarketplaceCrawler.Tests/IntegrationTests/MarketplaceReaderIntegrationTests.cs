using System.Collections.Generic;

using FollowApp.Entities;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests.IntegrationTests
{
	[Ignore]
	[TestFixture]
	public class MarketplaceReaderIntegrationTests
	{
		[Test(Description = "Integration test for the SearchApplications")]
		public void integration_test_for_the_searchapplications()
		{
			var applications = new List<Application>();
			var reader = new MarketplaceReader();
			var query = new SearchApplicationsQuery(new MarketplaceRegion("ru-RU"), MarketplaceClientType.WinMobile71)
			{
				Store = "Zest",
				ChunkSize = SearchApplicationsChunkSize.Ten,
				SortOrder = SearchApplicationsSortOrder.LastReleasedGoesFirst,
			};
			
			SearchApplicationsResult result = reader.SearchApplications(query);
			applications.AddRange(result.Applications);
			while (result.HasMore && applications.Count < 50)
			{
				result = result.Next();
				applications.AddRange(result.Applications);
			}

			Assert.That(result.Applications, Is.Not.Empty);
		}
	}
}
