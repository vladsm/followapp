using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;

using FollowApp.Entities;
using FollowApp.MarketplaceCrawler.Parsing;
using FollowApp.MarketplaceCrawler.Tests.Common;

using NUnit.Framework;

namespace FollowApp.MarketplaceCrawler.Tests
{
	[TestFixture]
	public class ApplicationBuilderTests
	{
		[Test(Description = "Builder resets state")]
		public void builder_resets_state()
		{
			var builder = new ApplicationBuilder();
			builder.SetId(Guid.NewGuid());
			builder.SetTitle("Title1");
			builder.Reset();

			var application = builder.Build();

			Assert.That(application.Id, Is.EqualTo(new Guid()));
			Assert.That(application.Title, Is.Null);
		}

		[Test(Description = "Builder sets id and title")]
		public void builder_sets_id_and_title()
		{
			Guid uuid = Guid.NewGuid();
			
			var builder = new ApplicationBuilder();
			builder.SetId(uuid);
			builder.SetTitle("Title1");
			
			var application = builder.Build();

			Assert.That(application.Id, Is.EqualTo(uuid));
			Assert.That(application.Title, Is.EqualTo("Title1"));
		}

		[Test(Description = "Builder sets attributes using extensions one by one")]
		public void builder_sets_attributes_using_extensions_one_by_one()
		{
			var builder = new ApplicationBuilder();
			foreach (var extension in CreateExtensions())
			{
				builder.AddExtension(extension);
			}

			var application = builder.Build();

			CheckBuilderResult(application);
		}

		[Test(Description = "Builder sets attributes using extensions in a batch")]
		public void builder_sets_attributes_using_extensions_in_a_batch()
		{
			var builder = new ApplicationBuilder();
			builder.AddExtensions(CreateExtensions());

			var application = builder.Build();

			CheckBuilderResult(application);
		}

		[Test(Description = "Builder sets empty collection attributes even if corresponding extensions do not exist")]
		public void builder_sets_empty_collection_attributes_even_if_corresponding_extensions_do_not_exist()
		{
			var builder = new ApplicationBuilder();
			builder.AddExtensions(
				CreateExtensions().
					Where(e => e.OuterName != "categories").
					Where(e => e.OuterName != "tags").
					Where(e => e.OuterName != "offers")
				);

			var application = builder.Build();

			Assert.That(application.Categories, Is.Not.Null);
			Assert.That(application.Tags, Is.Not.Null);
			Assert.That(application.Offers, Is.Not.Null);
		}


		private static void CheckBuilderResult(Application application)
		{
			Assert.That(application.SortTitle, Is.EqualTo("A sort title"));
			Assert.That(application.ReleaseDate, Is.EqualTo(new DateTime(2012, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)));
			Assert.That(application.Description, Is.EqualTo("A short description"));
			Assert.That(application.Version, Is.EqualTo("1.0.0.0"));
			Assert.That(application.AverageUserRating, Is.EqualTo(6.7));
			Assert.That(application.UserRatingCount, Is.EqualTo(100));
			Assert.That(application.AverageLastInstanceUserRating, Is.EqualTo(5.3));
			Assert.That(application.LastInstanceUserRatingCount, Is.EqualTo(10));
			Assert.That(application.Image, Is.Not.Null);
			Assert.That(application.Categories, Is.Not.Null);
			Assert.That(application.Tags, Is.Not.Null);
			Assert.That(application.Offers, Is.Not.Null);
			Assert.That(application.Publisher, Is.Not.Null);
		}

		private static IEnumerable<SyndicationElementExtension> CreateExtensions()
		{
			yield return CreateElementExtension("sortTitle", "A sort title");
			yield return CreateElementExtension("releaseDate", "2012-01-01T00:00:00.0Z");
			yield return CreateElementExtension("shortDescription", "A short description");
			yield return CreateElementExtension("version", "1.0.0.0");
			yield return CreateElementExtension("averageUserRating", "6.7");
			yield return CreateElementExtension("userRatingCount", "100");
			yield return CreateElementExtension("averageLastInstanceUserRating", "5.3");
			yield return CreateElementExtension("lastInstanceUserRatingCount", "10");
			yield return CreateElementExtension("image", "<id>urn:uuid:71114000-62a0-4678-bb79-4c6071fc0000</id>");
			yield return CreateElementExtension("categories", "");
			yield return CreateElementExtension("tags", "");
			yield return CreateElementExtension("offers", "");
			yield return CreateElementExtension("publisher", "<id>mobisoft</id><name>MobiSoft</name>");
		}

		private static SyndicationElementExtension CreateElementExtension(string extensionElementName, string xmlContent)
		{
			return SyndicationElementExtensionCreator.FromXmlElement(
				Constants.AppCatalogSchemaNamespace,
				extensionElementName,
				xmlContent
				);
		}
	}
}
