namespace FollowApp.DataAccess
{
	public sealed class EntitiesContextFactory
	{
		public IApplicationsEntitiesContext CreateApplicationsEntitiesContext()
		{
			return new ApplicationsEntitiesContext();
		}
		
		public IApplicationsEntitiesContext CreateApplicationsEntitiesContext(string nameOrConnectionString)
		{
			return new ApplicationsEntitiesContext(nameOrConnectionString);
		}
	}
}
