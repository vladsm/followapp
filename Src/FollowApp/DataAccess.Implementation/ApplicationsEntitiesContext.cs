﻿using System.Data.Entity;

using FollowApp.DataAccess.Entities;

namespace FollowApp.DataAccess
{
	public class ApplicationsEntitiesContext : DbContext, IApplicationsEntitiesContext
	{
		public ApplicationsEntitiesContext()
		{
		}

		public ApplicationsEntitiesContext(string nameOrConnectionString)
			: base(nameOrConnectionString)
		{
		}

		public DbSet<ApplicationEntity> Applications { get; set; }
		public DbSet<ApplicationAttributesEntity> ApplicationAttributes { get; set; }
		public DbSet<MarketplaceRegionEntity> MarketplaceRegions { get; set; }
		public DbSet<PublisherEntity> Publishers { get; set; }
		public DbSet<ApplicationCategoryEntity> Categories { get; set; }
		public DbSet<MarketplaceClientTypeDictionaryItem> MarketplaceClientTypes { get; set; }
		public DbSet<DeviceCapabilityEntity> Capabilities { get; set; }
		public DbSet<LanguageEntity> Languages { get; set; }

		IEntitySet<ApplicationEntity> IApplicationsEntitiesContext.Applications
		{
			get { return new EntitySet<ApplicationEntity>(Applications); }
		}

		IEntitySet<ApplicationAttributesEntity> IApplicationsEntitiesContext.ApplicationAttributes
		{
			get { return new EntitySet<ApplicationAttributesEntity>(ApplicationAttributes); }
		}

		IEntitySet<MarketplaceRegionEntity> IApplicationsEntitiesContext.MarketplaceRegions
		{
			get { return new EntitySet<MarketplaceRegionEntity>(MarketplaceRegions); }
		}

		IEntitySet<PublisherEntity> IApplicationsEntitiesContext.Publishers
		{
			get { return new EntitySet<PublisherEntity>(Publishers); }
		}

		IEntitySet<ApplicationCategoryEntity> IApplicationsEntitiesContext.Categories
		{
			get { return new EntitySet<ApplicationCategoryEntity>(Categories); }
		}

		IEntitySet<MarketplaceClientTypeDictionaryItem> IApplicationsEntitiesContext.MarketplaceClientTypes
		{
			get { return new EntitySet<MarketplaceClientTypeDictionaryItem>(MarketplaceClientTypes); }
		}

		IEntitySet<DeviceCapabilityEntity> IApplicationsEntitiesContext.Capabilities
		{
			get { return new EntitySet<DeviceCapabilityEntity>(Capabilities); }
		}

		IEntitySet<LanguageEntity> IApplicationsEntitiesContext.Languages
		{
			get { return new EntitySet<LanguageEntity>(Languages); }
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			new DataModelAdjustments(modelBuilder).Apply();
			base.OnModelCreating(modelBuilder);
		}
	}
}
