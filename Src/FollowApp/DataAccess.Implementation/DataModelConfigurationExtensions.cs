﻿using System.Data.Entity.ModelConfiguration.Configuration;

namespace FollowApp.DataAccess
{
	internal static class DataModelConfigurationExtensions
	{
		public static StringPropertyConfiguration IsUuid(
			this StringPropertyConfiguration stringPropertyConfiguration
			)
		{
			return stringPropertyConfiguration.IsUnicode(false).HasMaxLength(45).IsFixedLength();
		}

		public static PrimitivePropertyConfiguration IsUnique(
			this PrimitivePropertyConfiguration primitivePropertyConfiguration
			)
		{
			// Currently this property has no implementation and is used as a marker for the 
			// unique properties in the entity. When the Entity Framework will get support for the
			// unique indicies this property will get its implementation or will be replaced.

			return primitivePropertyConfiguration;
		}

		public static StringPropertyConfiguration IsUnique(
			this StringPropertyConfiguration primitivePropertyConfiguration
			)
		{
			// Currently this property has no implementation and is used as a marker for the 
			// unique properties in the entity. When the Entity Framework will get support for the
			// unique indicies this property will get its implementation or will be replaced.

			return primitivePropertyConfiguration;
		}
	}
}
