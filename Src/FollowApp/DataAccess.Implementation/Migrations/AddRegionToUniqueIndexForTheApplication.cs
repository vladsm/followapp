﻿namespace FollowApp.DataAccess.Migrations
{
	public partial class AddRegionToUniqueIndexForTheApplication
	{
		private void CreateUniqueIndexes()
		{
			DropIndex("Application", new[] { "MarketplaceApplicationId" });
			CreateIndex("Application", new[] { "MarketplaceApplicationId", "MarketplaceRegionId" }, unique: true);
		}

		private void DropUniqueIndexes()
		{
			DropIndex("Application", new[] { "MarketplaceApplicationId", "MarketplaceRegionId" });
			CreateIndex("Application", "MarketplaceApplicationId", unique: true);
		}
	}
}
