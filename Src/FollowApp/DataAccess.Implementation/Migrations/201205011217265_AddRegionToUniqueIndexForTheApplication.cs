namespace FollowApp.DataAccess.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class AddRegionToUniqueIndexForTheApplication : DbMigration
	{
		public override void Up()
		{
			CreateUniqueIndexes();
		}

		public override void Down()
		{
			DropUniqueIndexes();
		}
	}
}
