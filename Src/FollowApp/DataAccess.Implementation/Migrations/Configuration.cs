using System.Data.Entity.Migrations;
using System.Linq;

using FollowApp.DataAccess.Entities;
using FollowApp.Entities;

namespace FollowApp.DataAccess.Migrations
{
	internal sealed class Configuration : DbMigrationsConfiguration<ApplicationsEntitiesContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
		}

		protected override void Seed(ApplicationsEntitiesContext context)
		{
			if (context.MarketplaceRegions.All(r => r.Code != "en-US"))
			{
				context.MarketplaceRegions.Add(new MarketplaceRegionEntity { Code = "en-US", Name = "USA" });
			}
			if (context.MarketplaceRegions.All(r => r.Code != "ru-RU"))
			{
				context.MarketplaceRegions.Add(new MarketplaceRegionEntity { Code = "ru-RU", Name = "Russia" });
			}

			context.MarketplaceClientTypes.AddOrUpdate(
				new MarketplaceClientTypeDictionaryItem { Id = (int)MarketplaceClientType.WinMobile70, Name = "WinMobile 7.0" },
				new MarketplaceClientTypeDictionaryItem { Id = (int)MarketplaceClientType.WinMobile71, Name = "WinMobile 7.1" }
				);
		}
	}
}
