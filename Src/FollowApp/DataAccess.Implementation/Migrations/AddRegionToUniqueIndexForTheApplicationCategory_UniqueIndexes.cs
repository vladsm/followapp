﻿namespace FollowApp.DataAccess.Migrations
{
	public partial class AddRegionToUniqueIndexForTheApplicationCategory
	{
		private void CreateUniqueIndexes()
		{
			DropIndex("ApplicationCategory", new[] { "Code" });
			CreateIndex("ApplicationCategory", new[] {"Code", "MarketplaceRegionId"}, unique: true);
		}

		private void DropUniqueIndexes()
		{
			DropIndex("ApplicationCategory", new[] { "Code", "MarketplaceRegionId" });
			CreateIndex("ApplicationCategory", "Code", unique: true);
		}
	}
}
