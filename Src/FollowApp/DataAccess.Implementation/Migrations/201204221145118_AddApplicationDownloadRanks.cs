namespace FollowApp.DataAccess.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class AddApplicationDownloadRanks : DbMigration
	{
		public override void Up()
		{
			CreateTable(
					"ApplicationDownloadRanks",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								ApplicationId = c.Int(nullable: false),
								DateFrom = c.DateTime(nullable: false),
								DateTo = c.DateTime(),
								Rank = c.Int(nullable: false),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("Application", t => t.ApplicationId, cascadeDelete: true)
					.Index(t => t.ApplicationId);
		}

		public override void Down()
		{
			DropIndex("ApplicationDownloadRanks", new[] { "ApplicationId" });
			DropForeignKey("ApplicationDownloadRanks", "ApplicationId", "Application");
			DropTable("ApplicationDownloadRanks");
		}
	}
}
