namespace FollowApp.DataAccess.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class InitialApplicationsModel : DbMigration
	{
		public override void Up()
		{
			CreateTable(
					"Application",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								MarketplaceApplicationId = c.Guid(nullable: false),
								MarketplaceRegionId = c.Int(nullable: false),
								PublisherId = c.Int(),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("MarketplaceRegion", t => t.MarketplaceRegionId, cascadeDelete: true)
					.ForeignKey("Publisher", t => t.PublisherId)
					.Index(t => t.MarketplaceRegionId)
					.Index(t => t.PublisherId);

			CreateTable(
					"MarketplaceRegion",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								Code = c.String(nullable: false, maxLength: 8, unicode: false),
								Name = c.String(maxLength: 64, unicode: false),
							})
					.PrimaryKey(t => t.Id);

			CreateTable(
					"Publisher",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								Code = c.String(nullable: false, maxLength: 1024),
								Name = c.String(),
							})
					.PrimaryKey(t => t.Id);

			CreateTable(
					"ApplicationAttributes",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								ApplicationId = c.Int(nullable: false),
								DateFrom = c.DateTime(nullable: false),
								DateTo = c.DateTime(),
								Title = c.String(nullable: false),
								SortTitle = c.String(),
								Description = c.String(),
								ShortDescription = c.String(),
								ReleaseDate = c.DateTime(nullable: false),
								ImageId = c.Guid(),
								SearchImageId = c.Guid(),
								BackgroundImageId = c.Guid(),
								VisibilityStatus = c.String(maxLength: 128),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("Application", t => t.ApplicationId)
					.Index(t => t.ApplicationId);

			CreateTable(
					"ApplicationsUserRatingStatistics",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								ApplicationAttributesId = c.Int(nullable: false),
								DateFrom = c.DateTime(nullable: false),
								DateTo = c.DateTime(),
								Average = c.Double(nullable: false),
								Count = c.Int(nullable: false),
								LastInstanceAverage = c.Double(nullable: false),
								LastInstanceCount = c.Int(nullable: false),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("ApplicationAttributes", t => t.ApplicationAttributesId, cascadeDelete: true)
					.Index(t => t.ApplicationAttributesId);

			CreateTable(
					"ApplicationCategory",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								Code = c.String(nullable: false, maxLength: 512, unicode: false),
								Title = c.String(),
								ParentId = c.Int(),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("ApplicationCategory", t => t.ParentId)
					.Index(t => t.ParentId);

			CreateTable(
					"Tag",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								Code = c.String(nullable: false, maxLength: 128, unicode: false),
							})
					.PrimaryKey(t => t.Id);

			CreateTable(
					"ApplicationOffer",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								MarketplaceOfferId = c.Guid(nullable: false),
								MarketplaceMediaInstanceId = c.Guid(nullable: false),
								PaymentTypes = c.Int(nullable: false),
								Store = c.String(nullable: false),
								Price = c.Decimal(nullable: false, precision: 18, scale: 2),
								DisplayPrice = c.String(nullable: false, maxLength: 64),
								PriceCurrencyCode = c.String(nullable: false, maxLength: 3, unicode: false),
								LicenseRight = c.Int(nullable: false),
								ApplicationAttributesId = c.Int(nullable: false),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("ApplicationAttributes", t => t.ApplicationAttributesId, cascadeDelete: true)
					.Index(t => t.ApplicationAttributesId);

			CreateTable(
					"MarketplaceClientType",
					c => new
							{
								Id = c.Int(nullable: false),
								Name = c.String(nullable: false, maxLength: 64, unicode: false),
							})
					.PrimaryKey(t => t.Id);

			CreateTable(
					"ApplicationMediaInstance",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								MarketplaceMediaInstanceId = c.Guid(nullable: false),
								Title = c.String(),
								Version = c.String(nullable: false, maxLength: 20, unicode: false),
								Url = c.String(nullable: false, maxLength: 4096, unicode: false),
								PackageSize = c.Int(nullable: false),
								InstallSize = c.Int(nullable: false),
							})
					.PrimaryKey(t => t.Id);

			CreateTable(
					"Language",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								Name = c.String(nullable: false, maxLength: 64),
							})
					.PrimaryKey(t => t.Id);

			CreateTable(
					"DeviceCapabilityDisclosure",
					c => new
							{
								DeviceCapabilityId = c.Int(nullable: false),
								ApplicationMediaInstanceId = c.Int(nullable: false),
								DisclosureType = c.Byte(nullable: false),
							})
					.PrimaryKey(t => new { t.DeviceCapabilityId, t.ApplicationMediaInstanceId })
					.ForeignKey("DeviceCapability", t => t.DeviceCapabilityId, cascadeDelete: true)
					.ForeignKey("ApplicationMediaInstance", t => t.ApplicationMediaInstanceId, cascadeDelete: true)
					.Index(t => t.DeviceCapabilityId)
					.Index(t => t.ApplicationMediaInstanceId);

			CreateTable(
					"DeviceCapability",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								Code = c.String(nullable: false, maxLength: 64, unicode: false),
							})
					.PrimaryKey(t => t.Id);

			CreateTable(
					"ApplicationMediaInstanceUserRatingStatistics",
					c => new
							{
								Id = c.Int(nullable: false, identity: true),
								MediaInstanceId = c.Int(nullable: false),
								DateFrom = c.DateTime(nullable: false),
								DateTo = c.DateTime(),
								Average = c.Double(nullable: false),
								Count = c.Int(nullable: false),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("ApplicationMediaInstance", t => t.MediaInstanceId, cascadeDelete: true)
					.Index(t => t.MediaInstanceId);

			CreateTable(
					"ApplicationScreenshot",
					c => new
							{
								Id = c.Guid(nullable: false),
								ApplicationAttributesId = c.Int(nullable: false),
							})
					.PrimaryKey(t => t.Id)
					.ForeignKey("ApplicationAttributes", t => t.ApplicationAttributesId, cascadeDelete: true)
					.Index(t => t.ApplicationAttributesId);

			CreateTable(
					"ApplicationAttributesToCategory",
					c => new
							{
								ApplicationAttributesId = c.Int(nullable: false),
								ApplicationCategoryId = c.Int(nullable: false),
							})
					.PrimaryKey(t => new { t.ApplicationAttributesId, t.ApplicationCategoryId })
					.ForeignKey("ApplicationAttributes", t => t.ApplicationAttributesId, cascadeDelete: true)
					.ForeignKey("ApplicationCategory", t => t.ApplicationCategoryId, cascadeDelete: true)
					.Index(t => t.ApplicationAttributesId)
					.Index(t => t.ApplicationCategoryId);

			CreateTable(
					"ApplicationAttributesToTag",
					c => new
							{
								ApplicationAttributesId = c.Int(nullable: false),
								TagId = c.Int(nullable: false),
							})
					.PrimaryKey(t => new { t.ApplicationAttributesId, t.TagId })
					.ForeignKey("ApplicationAttributes", t => t.ApplicationAttributesId, cascadeDelete: true)
					.ForeignKey("Tag", t => t.TagId, cascadeDelete: true)
					.Index(t => t.ApplicationAttributesId)
					.Index(t => t.TagId);

			CreateTable(
					"ApplicationOfferToMarketplaceClientType",
					c => new
							{
								ApplicationOfferId = c.Int(nullable: false),
								MarketplaceClientTypeId = c.Int(nullable: false),
							})
					.PrimaryKey(t => new { t.ApplicationOfferId, t.MarketplaceClientTypeId })
					.ForeignKey("ApplicationOffer", t => t.ApplicationOfferId, cascadeDelete: true)
					.ForeignKey("MarketplaceClientType", t => t.MarketplaceClientTypeId, cascadeDelete: true)
					.Index(t => t.ApplicationOfferId)
					.Index(t => t.MarketplaceClientTypeId);

			CreateTable(
					"ApplicationMediaInstanceToMarketplaceClientType",
					c => new
							{
								ApplicationMediaInstanceId = c.Int(nullable: false),
								MarketplaceClientTypeId = c.Int(nullable: false),
							})
					.PrimaryKey(t => new { t.ApplicationMediaInstanceId, t.MarketplaceClientTypeId })
					.ForeignKey("ApplicationMediaInstance", t => t.ApplicationMediaInstanceId, cascadeDelete: true)
					.ForeignKey("MarketplaceClientType", t => t.MarketplaceClientTypeId, cascadeDelete: true)
					.Index(t => t.ApplicationMediaInstanceId)
					.Index(t => t.MarketplaceClientTypeId);

			CreateTable(
					"ApplicationMediaInstanceToSupportedLanguages",
					c => new
							{
								ApplicationMediaInstanceId = c.Int(nullable: false),
								LanguageId = c.Int(nullable: false),
							})
					.PrimaryKey(t => new { t.ApplicationMediaInstanceId, t.LanguageId })
					.ForeignKey("ApplicationMediaInstance", t => t.ApplicationMediaInstanceId, cascadeDelete: true)
					.ForeignKey("Language", t => t.LanguageId, cascadeDelete: true)
					.Index(t => t.ApplicationMediaInstanceId)
					.Index(t => t.LanguageId);

			CreateTable(
					"ApplicationAttributesToMediaInstances",
					c => new
							{
								ApplicationAttributeId = c.Int(nullable: false),
								MediaInstanceId = c.Int(nullable: false),
							})
					.PrimaryKey(t => new { t.ApplicationAttributeId, t.MediaInstanceId })
					.ForeignKey("ApplicationAttributes", t => t.ApplicationAttributeId, cascadeDelete: true)
					.ForeignKey("ApplicationMediaInstance", t => t.MediaInstanceId, cascadeDelete: true)
					.Index(t => t.ApplicationAttributeId)
					.Index(t => t.MediaInstanceId);

			CreateUniqueIndexes();
		}

		public override void Down()
		{
			DropUniqueIndexes();

			DropIndex("ApplicationAttributesToMediaInstances", new[] { "MediaInstanceId" });
			DropIndex("ApplicationAttributesToMediaInstances", new[] { "ApplicationAttributeId" });
			DropIndex("ApplicationMediaInstanceToSupportedLanguages", new[] { "LanguageId" });
			DropIndex("ApplicationMediaInstanceToSupportedLanguages", new[] { "ApplicationMediaInstanceId" });
			DropIndex("ApplicationMediaInstanceToMarketplaceClientType", new[] { "MarketplaceClientTypeId" });
			DropIndex("ApplicationMediaInstanceToMarketplaceClientType", new[] { "ApplicationMediaInstanceId" });
			DropIndex("ApplicationOfferToMarketplaceClientType", new[] { "MarketplaceClientTypeId" });
			DropIndex("ApplicationOfferToMarketplaceClientType", new[] { "ApplicationOfferId" });
			DropIndex("ApplicationAttributesToTag", new[] { "TagId" });
			DropIndex("ApplicationAttributesToTag", new[] { "ApplicationAttributesId" });
			DropIndex("ApplicationAttributesToCategory", new[] { "ApplicationCategoryId" });
			DropIndex("ApplicationAttributesToCategory", new[] { "ApplicationAttributesId" });
			DropIndex("ApplicationScreenshot", new[] { "ApplicationAttributesId" });
			DropIndex("ApplicationMediaInstanceUserRatingStatistics", new[] { "MediaInstanceId" });
			DropIndex("DeviceCapabilityDisclosure", new[] { "ApplicationMediaInstanceId" });
			DropIndex("DeviceCapabilityDisclosure", new[] { "DeviceCapabilityId" });
			DropIndex("ApplicationOffer", new[] { "ApplicationAttributesId" });
			DropIndex("ApplicationCategory", new[] { "ParentId" });
			DropIndex("ApplicationsUserRatingStatistics", new[] { "ApplicationAttributesId" });
			DropIndex("ApplicationAttributes", new[] { "ApplicationId" });
			DropIndex("Application", new[] { "PublisherId" });
			DropIndex("Application", new[] { "MarketplaceRegionId" });
			DropForeignKey("ApplicationAttributesToMediaInstances", "MediaInstanceId", "ApplicationMediaInstance");
			DropForeignKey("ApplicationAttributesToMediaInstances", "ApplicationAttributeId", "ApplicationAttributes");
			DropForeignKey("ApplicationMediaInstanceToSupportedLanguages", "LanguageId", "Language");
			DropForeignKey("ApplicationMediaInstanceToSupportedLanguages", "ApplicationMediaInstanceId", "ApplicationMediaInstance");
			DropForeignKey("ApplicationMediaInstanceToMarketplaceClientType", "MarketplaceClientTypeId", "MarketplaceClientType");
			DropForeignKey("ApplicationMediaInstanceToMarketplaceClientType", "ApplicationMediaInstanceId", "ApplicationMediaInstance");
			DropForeignKey("ApplicationOfferToMarketplaceClientType", "MarketplaceClientTypeId", "MarketplaceClientType");
			DropForeignKey("ApplicationOfferToMarketplaceClientType", "ApplicationOfferId", "ApplicationOffer");
			DropForeignKey("ApplicationAttributesToTag", "TagId", "Tag");
			DropForeignKey("ApplicationAttributesToTag", "ApplicationAttributesId", "ApplicationAttributes");
			DropForeignKey("ApplicationAttributesToCategory", "ApplicationCategoryId", "ApplicationCategory");
			DropForeignKey("ApplicationAttributesToCategory", "ApplicationAttributesId", "ApplicationAttributes");
			DropForeignKey("ApplicationScreenshot", "ApplicationAttributesId", "ApplicationAttributes");
			DropForeignKey("ApplicationMediaInstanceUserRatingStatistics", "MediaInstanceId", "ApplicationMediaInstance");
			DropForeignKey("DeviceCapabilityDisclosure", "ApplicationMediaInstanceId", "ApplicationMediaInstance");
			DropForeignKey("DeviceCapabilityDisclosure", "DeviceCapabilityId", "DeviceCapability");
			DropForeignKey("ApplicationOffer", "ApplicationAttributesId", "ApplicationAttributes");
			DropForeignKey("ApplicationCategory", "ParentId", "ApplicationCategory");
			DropForeignKey("ApplicationsUserRatingStatistics", "ApplicationAttributesId", "ApplicationAttributes");
			DropForeignKey("ApplicationAttributes", "ApplicationId", "Application");
			DropForeignKey("Application", "PublisherId", "Publisher");
			DropForeignKey("Application", "MarketplaceRegionId", "MarketplaceRegion");
			DropTable("ApplicationAttributesToMediaInstances");
			DropTable("ApplicationMediaInstanceToSupportedLanguages");
			DropTable("ApplicationMediaInstanceToMarketplaceClientType");
			DropTable("ApplicationOfferToMarketplaceClientType");
			DropTable("ApplicationAttributesToTag");
			DropTable("ApplicationAttributesToCategory");
			DropTable("ApplicationScreenshot");
			DropTable("ApplicationMediaInstanceUserRatingStatistics");
			DropTable("DeviceCapability");
			DropTable("DeviceCapabilityDisclosure");
			DropTable("Language");
			DropTable("ApplicationMediaInstance");
			DropTable("MarketplaceClientType");
			DropTable("ApplicationOffer");
			DropTable("Tag");
			DropTable("ApplicationCategory");
			DropTable("ApplicationsUserRatingStatistics");
			DropTable("ApplicationAttributes");
			DropTable("Publisher");
			DropTable("MarketplaceRegion");
			DropTable("Application");
		}
	}
}
