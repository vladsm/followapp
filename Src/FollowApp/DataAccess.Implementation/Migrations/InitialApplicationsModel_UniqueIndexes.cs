﻿namespace FollowApp.DataAccess.Migrations
{
	public partial class InitialApplicationsModel
	{
		private void CreateUniqueIndexes()
		{
			CreateIndex("Application", "MarketplaceApplicationId", unique: true);
			CreateIndex("MarketplaceRegion", "Code", unique: true);
			CreateIndex("Publisher", "Code", unique: true);
			CreateIndex("ApplicationCategory", "Code", unique: true);
			CreateIndex("Tag", "Code", unique: true);
			CreateIndex("MarketplaceClientType", "Name", unique: true);
			CreateIndex("Language", "Name", unique: true);
			CreateIndex("DeviceCapability", "Code", unique: true);
		}

		private void DropUniqueIndexes()
		{
			DropIndex("Application", new[] { "MarketplaceApplicationId" });
			DropIndex("MarketplaceRegion", new[] { "Code" });
			DropIndex("Publisher", new[] { "Code" });
			DropIndex("ApplicationCategory", new[] { "Code" });
			DropIndex("Tag", new[] { "Code" });
			DropIndex("MarketplaceClientType", new[] { "Name" });
			DropIndex("Language", new[] { "Name" });
			DropIndex("DeviceCapability", new[] { "Code" });
		}
	}
}
