namespace FollowApp.DataAccess.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class AddRegionToUniqueIndexForTheApplicationCategory : DbMigration
	{
		public override void Up()
		{
			CreateUniqueIndexes();
		}

		public override void Down()
		{
			DropUniqueIndexes();
		}
	}
}
