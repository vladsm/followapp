namespace FollowApp.DataAccess.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class VersionInTheApplicationAttributesAndLinkToRegionFromTheCategory : DbMigration
	{
		public override void Up()
		{
			AddColumn("ApplicationAttributes", "Version", c => c.String(nullable: false, maxLength: 64, unicode: false));
			AddColumn("ApplicationCategory", "MarketplaceRegionId", c => c.Int(nullable: true));
			Sql("update ApplicationCategory set MarketplaceRegionId = (select min(Id) from MarketplaceRegion)");
			AlterColumn("ApplicationCategory", "MarketplaceRegionId", c => c.Int(nullable: false));
			AddForeignKey("ApplicationCategory", "MarketplaceRegionId", "MarketplaceRegion", "Id", cascadeDelete: true);
			CreateIndex("ApplicationCategory", "MarketplaceRegionId");
		}

		public override void Down()
		{
			DropIndex("ApplicationCategory", new[] { "MarketplaceRegionId" });
			DropForeignKey("ApplicationCategory", "MarketplaceRegionId", "MarketplaceRegion");
			DropColumn("ApplicationCategory", "MarketplaceRegionId");
			DropColumn("ApplicationAttributes", "Version");
		}
	}
}
