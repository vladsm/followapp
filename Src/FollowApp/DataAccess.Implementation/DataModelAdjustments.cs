﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

using FollowApp.DataAccess.Entities;

namespace FollowApp.DataAccess
{
	internal sealed class DataModelAdjustments
	{
		private readonly DbModelBuilder _modelBuilder;

		public DataModelAdjustments([NotNull] DbModelBuilder modelBuilder)
		{
			if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");
			_modelBuilder = modelBuilder;
		}

		public void Apply()
		{
			_modelBuilder.Configurations.Add(new ApplicationConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationAttributesConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationUserRatingStatisticsConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationDownloadRankConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationCategoryConfiguration());
			_modelBuilder.Configurations.Add(new PublisherReferenceConfiguration());
			_modelBuilder.Configurations.Add(new TagConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationOfferConfiguration());
			_modelBuilder.Configurations.Add(new MarketplaceClientTypeConfiguration());
			_modelBuilder.Configurations.Add(new MarketplaceRegionConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationMediaInstanceConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationMediaInstanceUserRatingStatisticsConfiguration());
			_modelBuilder.Configurations.Add(new LanguageConfiguration());
			_modelBuilder.Configurations.Add(new DeviceCapabilityDisclosureConfiguration());
			_modelBuilder.Configurations.Add(new DeviceCapabilityConfiguration());
			_modelBuilder.Configurations.Add(new ApplicationScreenshotConfiguration());
		}

		private class ApplicationConfiguration : EntityTypeConfiguration<ApplicationEntity>
		{
			public ApplicationConfiguration()
			{
				ToTable("Application");
				Property(application => application.MarketplaceApplicationId).IsRequired().IsUnique();

				// Association with marketplace region
				HasRequired(application => application.MarketplaceRegion).
					WithMany().
					Map(mc => mc.MapKey("MarketplaceRegionId"));
				
				// Association with publisher
				HasOptional(application => application.Publisher).
					WithMany().
					Map(mc => mc.MapKey("PublisherId"));

				// Associations with download ranks
				HasMany(application => application.DownloadRanks).
					WithRequired().
					HasForeignKey(rank => rank.ApplicationId);
			}
		}

		private class ApplicationAttributesConfiguration : EntityTypeConfiguration<ApplicationAttributesEntity>
		{
			public ApplicationAttributesConfiguration()
			{
				ToTable("ApplicationAttributes");
				HasKey(application => application.Id);

				Property(appAttributes => appAttributes.Title).IsRequired();
				Property(appAttributes => appAttributes.ImageId).HasColumnName("ImageId").IsOptional();
				Property(appAttributes => appAttributes.SearchImageId).HasColumnName("SearchImageId").IsOptional();
				Property(appAttributes => appAttributes.BackgroundImageId).HasColumnName("BackgroundImageId").IsOptional();
				Property(appAttributes => appAttributes.VisibilityStatus).IsUnicode(true).HasMaxLength(128);
				Property(appAttributes => appAttributes.Version).IsRequired().IsUnicode(false).HasMaxLength(64);

				// Association with applications
				HasRequired(appAttributes => appAttributes.Application).
					WithMany().
					HasForeignKey(appAttributes => appAttributes.ApplicationId).
					WillCascadeOnDelete(false);

				// Associations with user rating statistics
				HasMany(appAttributes => appAttributes.UserRatings).
					WithRequired().
					HasForeignKey(userRatingStats => userRatingStats.ApplicationAttributesId);

				// Associations with application categories
				HasMany(appAttributes => appAttributes.Categories).
					WithMany().
					Map(mc => mc.
						ToTable("ApplicationAttributesToCategory").
						MapLeftKey("ApplicationAttributesId").
						MapRightKey("ApplicationCategoryId")
						);

				// Associations with tags
				HasMany(appAttributes => appAttributes.Tags).
					WithMany().
					Map(mc => mc.
						MapLeftKey("ApplicationAttributesId").
						MapRightKey("TagId").
						ToTable("ApplicationAttributesToTag")
						);

				// Associations with application offers
				HasMany(appAttributes => appAttributes.Offers).
					WithRequired().
					Map(mc => mc.MapKey("ApplicationAttributesId"));

				// Associations with media instances
				HasMany(appAttribute => appAttribute.MediaInstances).
					WithMany().
					Map(mc => mc.
						ToTable("ApplicationAttributesToMediaInstances").
						MapLeftKey("ApplicationAttributeId").
						MapRightKey("MediaInstanceId")
						);

				// Associations with screenshots
				HasMany(appAttributes => appAttributes.Screenshots).
					WithRequired().
					Map(mc => mc.MapKey("ApplicationAttributesId"));
			}
		}

		private class ApplicationUserRatingStatisticsConfiguration : EntityTypeConfiguration<ApplicationUserRatingStatisticsEntity>
		{
			public ApplicationUserRatingStatisticsConfiguration()
			{
				ToTable("ApplicationsUserRatingStatistics");
				HasKey(userRatingStats => userRatingStats.Id);
			}
		}

		internal class ApplicationDownloadRankConfiguration : EntityTypeConfiguration<ApplicationDownloadRankEntity>
		{
			public ApplicationDownloadRankConfiguration()
			{
				ToTable("ApplicationDownloadRanks");
				HasKey(rank => rank.Id);
			}
		}

		private class ApplicationCategoryConfiguration : EntityTypeConfiguration<ApplicationCategoryEntity>
		{
			public ApplicationCategoryConfiguration()
			{
				ToTable("ApplicationCategory");
				HasKey(category => category.Id);
				Property(category => category.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				Property(category => category.Code).IsUnicode(false).IsRequired().HasMaxLength(512).IsUnique();

				// Association with marketplace region
				HasRequired(category => category.MarketplaceRegion).
					WithMany().
					Map(mc => mc.MapKey("MarketplaceRegionId"));

				// Hierarchical association with itself
				HasOptional(category => category.ParentCategory).
					WithMany(category => category.ChildCategories).
					HasForeignKey(category => category.ParentId);
			}
		}

		private class PublisherReferenceConfiguration : EntityTypeConfiguration<PublisherEntity>
		{
			public PublisherReferenceConfiguration()
			{
				ToTable("Publisher");
				HasKey(publisher => publisher.Id);
				Property(publisher => publisher.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				Property(publisher => publisher.Code).IsRequired().HasMaxLength(1024).IsUnique();
			}
		}

		private class TagConfiguration : EntityTypeConfiguration<TagEntity>
		{
			public TagConfiguration()
			{
				ToTable("Tag");
				HasKey(tag => tag.Id);
				Property(tag => tag.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				Property(tag => tag.Code).IsRequired().IsUnicode(false).HasMaxLength(128).IsUnique();
			}
		}

		private class ApplicationOfferConfiguration : EntityTypeConfiguration<ApplicationOfferEntity>
		{
			public ApplicationOfferConfiguration()
			{
				ToTable("ApplicationOffer");
				HasKey(offer => offer.Id);
				Property(offer => offer.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				Property(offer => offer.MarketplaceOfferId).IsRequired();
				Property(offer => offer.MarketplaceMediaInstanceId).IsRequired();
				Property(offer => offer.PriceCurrencyCode).IsRequired().IsUnicode(false).HasMaxLength(3);
				Property(offer => offer.DisplayPrice).IsRequired().HasMaxLength(64);
				Property(offer => offer.Store).IsRequired();

				// Associations with marketplace client type
				HasMany(offer => offer.ClientTypes).
					WithMany().
					Map(mc => mc.
						MapLeftKey("ApplicationOfferId").
						MapRightKey("MarketplaceClientTypeId").
						ToTable("ApplicationOfferToMarketplaceClientType")
					);
			}
		}

		private class MarketplaceClientTypeConfiguration : EntityTypeConfiguration<MarketplaceClientTypeDictionaryItem>
		{
			public MarketplaceClientTypeConfiguration()
			{
				ToTable("MarketplaceClientType");
				Property(clientType => clientType.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
				Property(clientType => clientType.Name).IsRequired().IsUnicode(false).HasMaxLength(64).IsUnique();
			}
		}

		private class MarketplaceRegionConfiguration : EntityTypeConfiguration<MarketplaceRegionEntity>
		{
			public MarketplaceRegionConfiguration()
			{
				ToTable("MarketplaceRegion");
				Property(region => region.Code).IsRequired().IsUnicode(false).HasMaxLength(8).IsUnique();
				Property(region => region.Name).IsUnicode(false).HasMaxLength(64);
			}
		}

		private class ApplicationMediaInstanceConfiguration : EntityTypeConfiguration<ApplicationMediaInstanceEntity>
		{
			public ApplicationMediaInstanceConfiguration()
			{
				ToTable("ApplicationMediaInstance");
				HasKey(medialInstance => medialInstance.Id);
				Property(mediaInstance => mediaInstance.MarketplaceMediaInstanceId).IsRequired();
				Property(mediaInstance => mediaInstance.Version).IsRequired().IsUnicode(false).HasMaxLength(20);
				Property(mediaInstance => mediaInstance.Url).IsRequired().IsUnicode(false).HasMaxLength(4096);

				// Associations with marketplace client type
				HasMany(mediaInstance => mediaInstance.ClientTypes).
					WithMany().
					Map(mc => mc.
						ToTable("ApplicationMediaInstanceToMarketplaceClientType").
						MapLeftKey("ApplicationMediaInstanceId").
						MapRightKey("MarketplaceClientTypeId")
					);

				// Associations with supported languages
				HasMany(mediaInstance => mediaInstance.SupportedLanguages).
					WithMany().
					Map(mc => mc.
						ToTable("ApplicationMediaInstanceToSupportedLanguages").
						MapLeftKey("ApplicationMediaInstanceId").
						MapRightKey("LanguageId")
					);

				// Associations with device capabilities
				HasMany(mediaInstance => mediaInstance.Capabilities).
					WithRequired().
					HasForeignKey(capability => capability.ApplicationMediaInstanceId);

				// Associations with user rating statistics
				HasMany(mediaInstance => mediaInstance.UserRatings).
					WithRequired().
					HasForeignKey(userRatingStats => userRatingStats.MediaInstanceId);
			}
		}

		private class ApplicationMediaInstanceUserRatingStatisticsConfiguration : EntityTypeConfiguration<ApplicationMediaInstanceUserRatingStatisticsEntity>
		{
			public ApplicationMediaInstanceUserRatingStatisticsConfiguration()
			{
				ToTable("ApplicationMediaInstanceUserRatingStatistics");
				HasKey(userRatingStats => userRatingStats.Id);
			}
		}

		private class LanguageConfiguration : EntityTypeConfiguration<LanguageEntity>
		{
			public LanguageConfiguration()
			{
				ToTable("Language");
				HasKey(language => language.Id);
				Property(language => language.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				Property(language => language.Name).IsRequired().IsUnicode(true).HasMaxLength(64).IsUnique();
			}
		}

		private class DeviceCapabilityDisclosureConfiguration : EntityTypeConfiguration<DeviceCapabilityDisclosureEntity>
		{
			public DeviceCapabilityDisclosureConfiguration()
			{
				ToTable("DeviceCapabilityDisclosure");
				HasKey(capability => new {capability.DeviceCapabilityId, capability.ApplicationMediaInstanceId});

				// Association with device capability
				HasRequired(capability => capability.DeviceCapability).
					WithMany().
					HasForeignKey(capability => capability.DeviceCapabilityId);
			}
		}

		private class DeviceCapabilityConfiguration : EntityTypeConfiguration<DeviceCapabilityEntity>
		{
			public DeviceCapabilityConfiguration()
			{
				ToTable("DeviceCapability");
				HasKey(capability => capability.Id);
				Property(capability => capability.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				Property(capability => capability.Code).IsRequired().IsUnicode(false).HasMaxLength(64).IsUnique();
			}
		}

		private class ApplicationScreenshotConfiguration : EntityTypeConfiguration<ApplicationScreeshotEntity>
		{
			public ApplicationScreenshotConfiguration()
			{
				ToTable("ApplicationScreenshot");
				HasKey(screenshot => screenshot.Id);
				Property(screenshot => screenshot.Id);
			}
		}
	}
}
