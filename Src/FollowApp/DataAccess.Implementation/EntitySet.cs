﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace FollowApp.DataAccess
{
	internal class EntitySet<TEntity> : IEntitySet<TEntity> where TEntity : class
	{
		private readonly IDbSet<TEntity> _dbSet;

		public EntitySet(DbSet<TEntity> dbSet)
		{
			_dbSet = dbSet;
		}

		public IEnumerator<TEntity> GetEnumerator()
		{
			return _dbSet.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public Expression Expression
		{
			get { return _dbSet.Expression; }
		}

		public Type ElementType
		{
			get { return _dbSet.ElementType; }
		}

		public IQueryProvider Provider
		{
			get { return _dbSet.Provider; }
		}

		public TEntity Find(params object[] keyValues)
		{
			return _dbSet.Find(keyValues);
		}

		public TEntity Add(TEntity entity)
		{
			return _dbSet.Add(entity);
		}

		public TEntity Remove(TEntity entity)
		{
			return _dbSet.Remove(entity);
		}

		public TEntity Attach(TEntity entity)
		{
			return _dbSet.Attach(entity);
		}

		public TEntity Create()
		{
			return _dbSet.Create();
		}

		public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, TEntity
		{
			return _dbSet.Create<TDerivedEntity>();
		}
	}
}
