using System;

using NUnit.Framework;

namespace FollowApp.Common.Tests
{
	[TestFixture]
	public class UriQueryBuilderTests
	{
		[Test(Description = "Empty query is created")]
		public void empty_query_is_created()
		{
			var builder = new UriQueryBuilder();
			
			Assert.That(builder.GetQuery(), Is.Empty);
		}

		[Test(Description = "Query with single path parameter is created")]
		public void query_with_single_path_parameter_is_created()
		{
			var builder = new UriQueryBuilder();
			builder.AddPathParameter("pathParameter1");

			Assert.That(builder.GetQuery(), Is.EqualTo("pathParameter1"));
		}

		[Test(Description = "Query with multiple path parameters is created")]
		public void query_with_multiple_path_parameters_is_created()
		{
			var builder = new UriQueryBuilder();
			builder.AddPathParameter("pathParameter1");
			builder.AddPathParameter("pathParameter2");
			builder.AddPathParameter("pathParameter3");

			Assert.That(builder.GetQuery(), Is.EqualTo("pathParameter1/pathParameter2/pathParameter3"));
		}

		[Test(Description = "Query with single non empty value parameter is created")]
		public void query_with_single_non_empty_value_parameter_is_created()
		{
			var builder = new UriQueryBuilder();
			builder.AddParameter("param1", "someValue");

			Assert.That(builder.GetQuery(), Is.EqualTo("param1=someValue"));
		}

		[Test(Description = "Query with single empty value parameter is created")]
		public void query_with_single_empty_value_parameter_is_created()
		{
			var builder = new UriQueryBuilder();
			builder.AddParameter("param1", "");
			Assert.That(builder.GetQuery(), Is.EqualTo("param1="));

			builder = new UriQueryBuilder();
			builder.AddParameter("param1", null);
			Assert.That(builder.GetQuery(), Is.EqualTo("param1"));
		}

		[Test(Description = "Query with multiple parameters is created")]
		public void query_with_multiple_parameters_is_created()
		{
			var builder = new UriQueryBuilder();
			builder.AddParameter("param1", "1");
			builder.AddParameter("param2", null);
			builder.AddParameter("param3", "someValue");
			builder.AddParameter("param4", "");

			var result = builder.GetQuery();

			var expected = new[]
			{
				"param1=1",
				"param2",
				"param3=someValue",
				"param4="
			};
			Assert.That(result.Split('&'), Is.EquivalentTo(expected));
		}

		[Test(Description = "Query with path and ordinal parameters is created")]
		public void query_with_path_and_ordinal_parameters_is_created()
		{
			var builder = new UriQueryBuilder();
			builder.AddPathParameter("pathParameter1");
			builder.AddParameter("parameter1", "someValue");
			builder.AddPathParameter("pathParameter2");

			Assert.That(builder.GetQuery(), Is.EqualTo("pathParameter1/pathParameter2?parameter1=someValue"));
		}

		[Test(Description = "Uri object is created")]
		public void uri_object_is_created()
		{
			var builder = new UriQueryBuilder();
			builder.AddPathParameter("pathParameter1");
			builder.AddParameter("parameter1", "someValue");
			builder.AddPathParameter("pathParameter2");

			Assert.That(
				builder.GetQueryUri("http://test.com").AbsoluteUri,
				Is.EqualTo("http://test.com/pathParameter1/pathParameter2?parameter1=someValue")
				);
			Assert.That(
				builder.GetQueryUri("http://test.com/").AbsoluteUri,
				Is.EqualTo("http://test.com/pathParameter1/pathParameter2?parameter1=someValue")
				);
		}
	}
}
