using System;
using System.Collections.Generic;

using FollowApp.Collections;

using NUnit.Framework;

namespace FollowApp.Common.Tests
{
	public class CollectionsExtensionsTests
	{
		[TestFixture]
		public class TheIsEquivalentToMethod
		{
			[Test(Description = "Throws exception when the source collection is null")]
			public void throws_exception_when_the_source_collection_is_null()
			{
				Assert.That(
					() => NullIntsCollection.IsEquivalentTo(new[] {1}),
					Throws.Exception.InstanceOf<ArgumentNullException>().And.
						Property("ParamName").EqualTo("collection")
					);
			}

			[Test(Description = "Returns false when collection to compare is null")]
			public void returns_false_when_collection_to_compare_is_null()
			{
				Assert.That(new[] {1, 2}.IsEquivalentTo(null), Is.False);
			}

			[Test(Description = "Returns true when both source collection and collection to compare are empty")]
			public void returns_true_when_both_source_collection_and_collection_to_compare_are_empty()
			{
				Assert.That(new string[] {}.IsEquivalentTo(new List<string>()), Is.True);
			}

			[Test(Description = "Returns false when collections have different size")]
			public void returns_false_when_collections_have_different_size()
			{
				Assert.That(new string[] {}.IsEquivalentTo(new[] {"1"}), Is.False);
				Assert.That(new [] {1, 2, 3}.IsEquivalentTo(new[] {1, 2}), Is.False);
				Assert.That(new[] {null, null, "str1"}.IsEquivalentTo(new[] {null, null, null, "str1"}), Is.False);
				Assert.That(new string[] {}.IsEquivalentTo(new string[] {null}), Is.False);
			}

			[Test(Description = "Correctly compares collections when they don't have nulls")]
			public void correctly_compares_collections_when_they_don_t_have_nulls()
			{
				Assert.That(new[] { 1 }.IsEquivalentTo(new[] { 1 }), Is.True);
				Assert.That(new[] { 1 }.IsEquivalentTo(new[] { 2 }), Is.False);
				Assert.That(new[] { 1, 2, 3 }.IsEquivalentTo(new[] { 1, 2, 3 }), Is.True);
				Assert.That(new[] { 1, 2, 3 }.IsEquivalentTo(new[] { 1, 3, 2 }), Is.True);
				Assert.That(new[] { 1, 2, 3 }.IsEquivalentTo(new[] { 1, 2, 4 }), Is.False);
				Assert.That(new[] { 1, 2, 3 }.IsEquivalentTo(new[] { 6, 7, 8 }), Is.False);
			}

			[Test(Description = "Correctly compares collections containing nulls")]
			public void correctly_compares_collections_containing_nulls()
			{
				Assert.That(new object[] { null }.IsEquivalentTo(new object[] { null }), Is.True);
				Assert.That(new object[] { 1, null }.IsEquivalentTo(new object[] { null, null }), Is.False);
				Assert.That(new object[] { 1, null }.IsEquivalentTo(new object[] { 1, 2 }), Is.False);
				Assert.That(new object[] { null, null }.IsEquivalentTo(new object[] { null, null }), Is.True);
			}

			[Test(Description = "Uses default comparer for the elements")]
			public void uses_default_comparer_for_the_elements()
			{
				Assert.That(new[] {"str1"}.IsEquivalentTo(new[] {"str" + "1"}), Is.True);
				Assert.That(new[] { new { A = 1, B = 2 } }.IsEquivalentTo(new[] { new { A = 1, B = 2 } }), Is.True);
			}

			[Test(Description = "Correctly compares untyped collections")]
			public void correctly_compares_untyped_collections()
			{
				object someObject = new object();
				Assert.That(new[] { 1, "str1", someObject }.IsEquivalentTo(new[] { someObject, 1, "str1" }), Is.True);
			}

			[Test(Description = "Correctly compares duplicated elements")]
			public void correctly_compares_duplicated_elements()
			{
				Assert.That(new[] { 1, 2, 1, 2, 1 }.IsEquivalentTo(new[] { 1, 2, 1, 1, 2 }), Is.True);
				Assert.That(new[] { 1, 2, 1, 2, 1 }.IsEquivalentTo(new[] { 1, 2, 1, 2, 3 }), Is.False);
			}

			[Test(Description = "Correctrly compares complex type collections with equality comparer")]
			public void correctrly_compares_complex_type_collections_with_equality_comparer()
			{
				var object1 = new Complex(1, "One");
				var object1Prime1 = new Complex(1, "OnePrimeOne");
				var object2 = new Complex(2, "Two");
				var object2Prime2 = new Complex(2, "TwoPrimeTwo");

				var equalityComparer = new ComplexEqualityComparer();

				Assert.That(new[] { object1, object2 }.IsEquivalentTo(new[] { object2Prime2, object1Prime1 }, equalityComparer), Is.True);
				Assert.That(new[] { object1, object2, object1Prime1 }.IsEquivalentTo(new[] { object2Prime2, object1Prime1, object2 }, equalityComparer), Is.False);
			}
		}

		#region Helpers

		private class Complex
		{
			public int Id { get; private set; }
// ReSharper disable MemberCanBePrivate.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
			public string Name { get; private set; }
// ReSharper restore UnusedAutoPropertyAccessor.Local
// ReSharper restore MemberCanBePrivate.Local

			public Complex(int id, string name)
			{
				Id = id;
				Name = name;
			}
		}

		private class ComplexEqualityComparer : IEqualityComparer<Complex>
		{
			public bool Equals(Complex x, Complex y)
			{
				if (x == null && y == null) return true;
				if (x == null || y == null) return false;
				return x.Id == y.Id;
			}

			public int GetHashCode(Complex obj)
			{
				if (obj == null) return 0;
				return obj.Id.GetHashCode();
			}
		}

		private static IEnumerable<int> NullIntsCollection
		{
			get { return null; }
		}

		#endregion
	}
}
