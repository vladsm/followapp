﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FollowApp.UnitTesting.Utils.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vladimir Smirnov")]
[assembly: AssemblyProduct("FollowApp")]
[assembly: AssemblyCopyright("Copyright © Vladimir Smirnov (vladsm@gmail.com) 2011")]
[assembly: AssemblyTrademark("FollowApp")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("6370ea41-9a8d-47bf-b6a6-a8986892ec65")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
