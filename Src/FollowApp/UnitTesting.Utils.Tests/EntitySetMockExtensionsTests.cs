using System.Collections.Generic;
using System.Linq;

using FollowApp.UnitTesting.Utils.DataAccess;

using NUnit.Framework;

public sealed class EntitySetMockExtensionsTests
{
	[TestFixture]
	public sealed class TheAsMockedEntitySetMethod
	{
		[Test(Description = "Returns mock that supports LINQ")]
		public void returns_mock_that_supports_linq()
		{
			var countriesSetMock = GetCountries().AsMockedEntitySet();

			var countriesWithPopulationUnder100M =
				from country in countriesSetMock
				where country.Population < 100000000
				select country.Name;

			Assert.That(countriesWithPopulationUnder100M, Is.EquivalentTo(new[] {"UK", "Germany"}));
		}

		[Test(Description = "Mocks Find method")]
		public void mocks_find_method()
		{
			var countriesSetMock = GetCountries().AsMockedEntitySet(e => e.Capital);
			var ukCountry = countriesSetMock.Find("London");
			var notFoundCountry = countriesSetMock.Find("Nowhere");

			Assert.That(ukCountry, Is.Not.Null);
			Assert.That(ukCountry.Name, Is.EqualTo("UK"));
			Assert.That(notFoundCountry, Is.Null);

			countriesSetMock = GetCountries().AsMockedEntitySet(e => new object[] { e.Name, e.Capital });
			var germanyCountry = countriesSetMock.Find("Germany", "Berlin");
			var notGermanyCountry1 = countriesSetMock.Find("Germany", "Berling");
			var notGermanyCountry2 = countriesSetMock.Find("Germanya", "Berlin");

			Assert.That(germanyCountry, Is.Not.Null);
			Assert.That(germanyCountry.Name, Is.EqualTo("Germany"));
			Assert.That(notGermanyCountry1, Is.Null);
			Assert.That(notGermanyCountry2, Is.Null);
		}

		#region Testing data

		public class Country
		{
			public string Name { get; set; }
			public string Capital { get; set; }
			public int Population { get; set; }
		}

		private static IEnumerable<Country> GetCountries()
		{
			return new[]
			{
				new Country {Name = "USA", Capital = "Washington", Population = 800000000},
				new Country {Name = "UK", Capital = "London", Population = 70000000},
				new Country {Name = "Germany", Capital = "Berlin", Population = 90000000}
			};
		}

		#endregion
	}
}
