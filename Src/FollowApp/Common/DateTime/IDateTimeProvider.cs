﻿using System;

namespace FollowApp
{
	public interface IDateTimeProvider
	{
		DateTime UtcNow { get; }
	}
}
