using System;

namespace FollowApp
{
	public class SystemDateTimeProvider : IDateTimeProvider
	{
		public DateTime UtcNow
		{
			get { return DateTime.UtcNow; }
		}
	}
}
