using System;
using System.Collections.Generic;
using System.Linq;

namespace FollowApp
{
	public class UriQueryBuilder
	{
		private readonly List<string> _pathParameters = new List<string>();
		private readonly List<KeyValuePair<string, string>> _parameters = new List<KeyValuePair<string, string>>();
		
		public void AddPathParameter([NotNull] string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentException("Path parameter name can not be null or empty.", "name");
			}
			_pathParameters.Add(name);
		}

		public void AddParameter([NotNull] string name, [CanBeNull] string value)
		{
			if (name == null) throw new ArgumentNullException("name");
			_parameters.Add(new KeyValuePair<string, string>(name, value));
		}

		public string GetQuery()
		{
			var path = string.Join("/", _pathParameters.ToArray());
			var queryString = string.Join("&", _parameters.Select(GetParameterQueryStringPartPart).ToArray());
			if (string.IsNullOrEmpty(path)) return queryString;
			if (string.IsNullOrEmpty(queryString)) return path;
			return path + "?" + queryString;
		}

		public Uri GetQueryUri(string basePath)
		{
			if (string.IsNullOrEmpty(basePath))
			{
				throw new ArgumentException("Base path name can not be null or empty.", "basePath");
			}

			string uriPath = basePath[basePath.Length - 1] == '/' ? basePath + GetQuery() : basePath + "/" + GetQuery();
			return new Uri(uriPath);
		}


		private static string GetParameterQueryStringPartPart(KeyValuePair<string, string> parameter)
		{
			if (parameter.Value == null) return parameter.Key;
			if (parameter.Value == string.Empty) return parameter.Key + "=";
			return parameter.Key + "=" + parameter.Value;
		}
	}
}
