using System;
using System.Globalization;

namespace FollowApp
{
	public static class G
	{
		[NotNull]
		public static CultureInfo CommonCulture
		{
			get { return CultureInfo.InvariantCulture; }
		}

		[NotNull]
		[StringFormatMethod("message")]
		public static string Format([NotNull] string message, params object[] parameters)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return string.Format(CommonCulture, message, parameters);
		}

		[NotNull]
		public static string ToString(Guid guid, [NotNull] string format)
		{
			if (format == null)
				throw new ArgumentNullException("format");

			return guid.ToString(format);
		}

		[NotNull]
		public static string ToString(int count)
		{
			return count.ToString(CommonCulture);
		}

		public static bool ToBoolean([NotNull] string strBoolean)
		{
			if (strBoolean == null) throw new ArgumentNullException("strBoolean");
			return strBoolean.Equals("true", StringComparison.OrdinalIgnoreCase);
		}

		public static int ToInt32([NotNull] string strInt32)
		{
			if (strInt32 == null) throw new ArgumentNullException("strInt32");
			
			int result;
			if (int.TryParse(strInt32, NumberStyles.Integer, CommonCulture, out result))
			{
				return result;
			}
			throw new FormatException(Format("String value \"{0}\" can not be converted to Int32", strInt32));
		}

		public static int? ToNullableInt32([CanBeNull] string strInt32)
		{
			if (string.IsNullOrEmpty(strInt32)) return null;
			return ToInt32(strInt32);
		}

		public static decimal ToDecimal([NotNull] string strDecimal)
		{
			if (strDecimal == null) throw new ArgumentNullException("strDecimal");
			
			decimal result;
			if (decimal.TryParse(strDecimal, NumberStyles.Float, CommonCulture, out result))
			{
				return result;
			}
			throw new FormatException(Format("String value \"{0}\" can not be converted to Decimal", strDecimal));
		}

		public static decimal? ToNullableDecimal([CanBeNull] string strDecimal)
		{
			if (string.IsNullOrEmpty(strDecimal)) return null;
			return ToDecimal(strDecimal);
		}
	}
}
