using System;
using System.Collections.Generic;
using System.Linq;

namespace FollowApp.Collections
{
	public static class CollectionsExtensions
	{
		public static bool IsEquivalentTo<T>(
			[NotNull] this IEnumerable<T> collection,
			[CanBeNull] IEnumerable<T> collectionToCompare
			)
		{
			return IsEquivalentTo(collection, collectionToCompare, null);
		}

		public static bool IsEquivalentTo<T>(
			[NotNull] this IEnumerable<T> collection,
			[CanBeNull] IEnumerable<T> collectionToCompare,
			[CanBeNull] IEqualityComparer<T> equalityComparer
			)
		{
			if (collection == null) throw new ArgumentNullException("collection");
			if (ReferenceEquals(collection, collectionToCompare)) return true;
			if (collectionToCompare == null) return false;

			collection = collection.ToArray();
			collectionToCompare = collectionToCompare.ToArray();
			if (collection.Count() != collectionToCompare.Count()) return false;
			if (!collection.Any()) return true;

			var elementsCounts = new Dictionary<T, int>(collection.Count(), equalityComparer);
			int nullsCount = 0;
			foreach (var element in collection)
			{
				if (ReferenceEquals(element, null))
				{
					nullsCount++;
					continue;
				}
				
				int count;
				if (elementsCounts.TryGetValue(element, out count))
				{
					elementsCounts[element] = count + 1;
				}
				else
				{
					elementsCounts.Add(element, 1);
				}
			}

			foreach (var element in collectionToCompare)
			{
				if (ReferenceEquals(element, null))
				{
					if (nullsCount-- == 0) return false;
					continue;
				}

				int count;
				if (elementsCounts.TryGetValue(element, out count) && count > 0)
				{
					elementsCounts[element] = count - 1;
				}
				else
				{
					return false;
				}
			}

			return elementsCounts.All(p => p.Value == 0);
		}
	}
}
