﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FollowApp.Common.Net4")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vladimir Smirnov")]
[assembly: AssemblyProduct("FollowApp")]
[assembly: AssemblyCopyright("Copyright © Vladimir Smirnov (vladsm@gmail.com) 2011")]
[assembly: AssemblyTrademark("FollowApp")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("a2d47afe-cd28-4a60-abdf-0ace7a460ccd")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
