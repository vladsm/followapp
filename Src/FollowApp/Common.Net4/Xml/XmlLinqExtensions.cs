using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace FollowApp.Xml
{
	public static class XmlLinqExtensions
	{
		[CanBeNull]
		public static string AttributeValue(
			[NotNull] this XElement element,
			[NotNull] XName attributeName
			)
		{
			if (element == null) throw new ArgumentNullException("element");
			if (attributeName == null) throw new ArgumentNullException("attributeName");

			var attribute = element.Attribute(attributeName);
			return attribute == null ? null : attribute.Value;
		}

		[NotNull]
		public static XElement ExistingElement([NotNull] this XElement element, [NotNull] XName name)
		{
			if (element == null) throw new ArgumentNullException("element");
			if (name == null) throw new ArgumentNullException("name");
			var subElement = element.Element(name);
			if (subElement == null)
			{
				throw new InvalidOperationException(String.Format("Expected sub-element {0} doesn't exist.", name));
			}
			return subElement;
		}

		[NotNull]
		public static XElement SingleChildElement(
			[NotNull] this XElement element,
			[NotNull] XName name
			)
		{
			if (element == null) throw new ArgumentNullException("element");
			if (name == null) throw new ArgumentNullException("name");

			return element.Descendants(name).Single();
		}

		[NotNull]
		public static IEnumerable<XElement> SelectElements(
			[NotNull] this XNode node,
			[NotNull] string xpath,
			[CanBeNull] IXmlNamespaceResolver namespaceResolver = null
			)
		{
			var queryResult = (IEnumerable)node.XPathEvaluate(xpath, namespaceResolver);
			return queryResult.Cast<XElement>();
		}

		[NotNull]
		public static XElement SelectSingleElement(
			[NotNull] this XNode node,
			[NotNull] string xpath,
			[CanBeNull] IXmlNamespaceResolver namespaceResolver = null
			)
		{
			return node.SelectElements(xpath, namespaceResolver).Single();
		}

		public static int Int32Value([NotNull] this XElement element)
		{
			if (element == null) throw new ArgumentNullException("element");
			return G.ToInt32(element.Value);
		}

		public static int? NullableInt32Value([NotNull] this XElement element)
		{
			if (element == null) throw new ArgumentNullException("element");
			return G.ToNullableInt32(element.Value);
		}

		public static decimal DecimalValue([NotNull] this XElement element)
		{
			if (element == null) throw new ArgumentNullException("element");
			return G.ToDecimal(element.Value);
		}

		public static decimal? NullableDecimalValue([NotNull] this XElement element)
		{
			if (element == null) throw new ArgumentNullException("element");
			return G.ToNullableDecimal(element.Value);
		}
	}
}
