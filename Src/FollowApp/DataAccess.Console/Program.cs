﻿using System;
using System.Linq;

using FollowApp.DataAccess;
using FollowApp.DataAccess.Entities;
using FollowApp.Entities;

namespace DataAccess.Console
{
	class Program
	{
		static void Main()
		{
			IApplicationsEntitiesContext ctx = new EntitiesContextFactory().CreateApplicationsEntitiesContext("Default");
			ApplicationCategoryEntity parentCategory = new ApplicationCategoryEntity
			{
				Code = "travel",
				Title = "Путешествия"
			};
			var winMobile70Client = new MarketplaceClientTypeDictionaryItem {Id = (int) MarketplaceClientType.WinMobile70};
			ctx.MarketplaceClientTypes.Attach(winMobile70Client);

			var marketplaceRussia = (
				from region in ctx.MarketplaceRegions
				where region.Code == "ru-RU"
				select region
				).First();
			
			var applicationAttributes = new ApplicationAttributesEntity
			{
				Application = new ApplicationEntity
				{
					MarketplaceApplicationId = Guid.Parse("eedcc9b7-9f8e-4526-806d-ad0101c62d62"),
					MarketplaceRegion = marketplaceRussia,
					Publisher = new PublisherEntity
					{
						Code = "app-maker",
						Name = "App Maker"
					}
				},
				DateFrom = DateTime.Now,
				DateTo = null,
				Description = "Application description",
				Title = "Some application",
				SortTitle = "Some app",
				ReleaseDate = new DateTime(2011, 05, 29),
				ImageId = Guid.Parse("36d157a1-f759-4aff-984a-99622fdb6a5a"),
				Tags = new[] {new TagEntity {Code = "some-tag-1"} },
				Categories = new[]
				{
					parentCategory,
					new ApplicationCategoryEntity
					{
						Code = "travel.maps",
						Title = "Карты",
						ParentCategory = parentCategory
					}
				},
				Offers = new[]
				{
					new ApplicationOfferEntity
					{
						MarketplaceOfferId = Guid.Parse("323a6fe7-f9e9-46b5-aba0-1a1acbefdeff"),
						DisplayPrice = "$17.99",
						Price = 17.99M,
						PriceCurrencyCode = "USD",
						LicenseRight = (int)LicenseRight.Purchase,
						Store = "Zest",
						MarketplaceMediaInstanceId = Guid.Parse("7d38a385-0afc-4019-8ed5-494eb68fbebf"),
						ClientTypes = new[]
						{
							winMobile70Client
						}
					},
					new ApplicationOfferEntity
					{
						MarketplaceOfferId = Guid.Parse("323a6fe7-f9e9-46b5-aba0-1a1acbefdefc"),
						DisplayPrice = "0 руб.",
						Price = 0,
						PriceCurrencyCode = "RUR",
						LicenseRight = (int)LicenseRight.Trial,
						Store = "Zest",
						MarketplaceMediaInstanceId = Guid.Parse("7d38a385-0afc-4019-8ed5-494eb68fbebf"),
						ClientTypes = new[]
						{
							winMobile70Client
						}
					}
				}
			};
			ctx.ApplicationAttributes.Add(applicationAttributes);
			ctx.SaveChanges();
		}
	}
}
