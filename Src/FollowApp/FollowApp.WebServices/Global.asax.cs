﻿using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

using Autofac;
using Autofac.Integration.WebApi;

using FollowApp.CloudServices;

namespace FollowApp.WebServices
{
	public class WebApiApplication : HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);
			Configure(GlobalConfiguration.Configuration);

			InitializeCloudServices();
		}

		private void InitializeCloudServices()
		{
			new CloudServicesInitializer().EnsureInitialization();
		}

		private void Configure(HttpConfiguration configuration)
		{
			var containerBuilder = new ContainerBuilder();
			containerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());

			containerBuilder.Register<IFollowRequestServices>(c => new FollowRequestServices()).InstancePerApiRequest();

			var container = containerBuilder.Build();
			configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
		}

		private static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}

		private static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapHttpRoute(
				"DefaultApi",
				"api/{controller}/{id}",
				new { id = RouteParameter.Optional }
				);
			routes.MapRoute(
				"Default",
				"{controller}/{action}/{id}",
				new { controller = "Home", action = "Index", id = UrlParameter.Optional}
				);
		}
	}
}
