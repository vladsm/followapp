﻿using System;
using System.Web.Http;

using FollowApp.CloudServices;

namespace FollowApp.WebServices
{
	public class FollowRequestsController : ApiController
	{
		private readonly IFollowRequestServices _followRequestServices;

		public FollowRequestsController([NotNull] IFollowRequestServices followRequestServices)
		{
			if (followRequestServices == null) throw new ArgumentNullException("followRequestServices");

			_followRequestServices = followRequestServices;
		}

		public int Get()
		{
			var request = new ApplicationFollowRequest
			{
				UserId = "vladsm",
				ApplicationId = "TestApp_123",
				MarketplaceRegionCodes = new[] { "ru-RU", "en-US" }
			};

			try
			{
				_followRequestServices.Register(request);
			}
			catch (Exception exception)
			{
				throw;
			}
			return 145;
		}

		public void Post([NotNull] ApplicationFollowRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			throw new NotImplementedException();
		}
	}
}
