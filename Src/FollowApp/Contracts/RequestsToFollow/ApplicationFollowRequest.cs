using System.Collections.Generic;

namespace FollowApp
{
	public class ApplicationFollowRequest : FollowRequest
	{
		public string ApplicationId { get; set; }
		public IEnumerable<string> MarketplaceRegionCodes { get; set; }
	}
}
