namespace FollowApp.Entities
{
	public sealed class ApplicationCategory
	{
		public string Id { get; set; }
		public string Title { get; set; }
		public string ParentId { get; set; }
	}
}
