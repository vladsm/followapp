using System;
using System.Collections.Generic;

namespace FollowApp.Entities
{
	public sealed class Application
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string SortTitle { get; set; }
		public string Description { get; set; }
		public DateTime ReleaseDate { get; set; }
		public string Version { get; set; }
		public double AverageUserRating { get; set; }
		public int UserRatingCount { get; set; }
		public double AverageLastInstanceUserRating { get; set; }
		public int LastInstanceUserRatingCount { get; set; }
		public ImageReference Image { get; set; }
		public IEnumerable<ApplicationCategory> Categories { get; set; }
		public IEnumerable<Tag> Tags { get; set; }
		public IEnumerable<ApplicationOffer> Offers { get; set; }
		public PublisherReference Publisher { get; set; }
	}
}
