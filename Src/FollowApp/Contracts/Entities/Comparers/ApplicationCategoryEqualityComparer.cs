﻿using System.Collections.Generic;

namespace FollowApp.Entities
{
	public class ApplicationCategoryEqualityComparer : IEqualityComparer<ApplicationCategory>
	{
		public bool Equals(ApplicationCategory x, ApplicationCategory y)
		{
			if (x == null && y == null) return true;
			if (x == null || y == null) return false;
			return x.Id == y.Id && x.Title == y.Title;
		}

		public int GetHashCode(ApplicationCategory obj)
		{
			unchecked
			{
				int result = (obj.Id != null ? obj.Id.GetHashCode() : 0);
				result = (result * 397) ^ (obj.Title != null ? obj.Title.GetHashCode() : 0);
				return result;
			}
		}
	}
}
