namespace FollowApp.Entities
{
	public class PublisherReference
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}
}
