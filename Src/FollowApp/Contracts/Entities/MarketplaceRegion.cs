namespace FollowApp.Entities
{
	public class MarketplaceRegion
	{
		public string Code { get; private set; }

		public MarketplaceRegion(string code)
		{
			Code = code;
		}
	}
}
