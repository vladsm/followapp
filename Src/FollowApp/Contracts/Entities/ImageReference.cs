using System;

namespace FollowApp.Entities
{
	public class ImageReference
	{
		public Guid Id { get; set; }

		public ImageReference()
		{
		}

		public ImageReference(Guid id)
		{
			Id = id;
		}
	}
}
