using System;
using System.Collections.Generic;

namespace FollowApp.Entities
{
	public class ApplicationOffer
	{
		public Guid Id { get; set; }
		public Guid MediaInstanceId { get; set; }
		public IEnumerable<MarketplaceClientType> ClientTypes { get; set; }
		public PaymentTypes PaymentTypes { get; set; }
		public string Store { get; set; }
		public decimal Price { get; set; }
		public string DisplayPrice { get; set; }
		public string PriceCurrencyCode { get; set; }
		public LicenseRight LicenseRight { get; set; }
	}
}
