﻿using System;

namespace FollowApp.Entities
{
	[Flags]
	public enum PaymentTypes
	{
		None = 0x0,
		CreditCard = 0x1,
		MobileOperator = 0x2
	}
}
