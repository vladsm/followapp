using System;

namespace FollowApp.Entities
{
	public class Tag
	{
		public string Id { get; set; }

		public Tag([NotNull] string id)
		{
			if (id == null) throw new ArgumentNullException("id");
			Id = id;
		}
	}
}
