﻿using System;
using System.Threading;

using FollowApp.Entities;
using Con = System.Console;

namespace FollowApp.MarketplaceCrawler.Test.Console
{
	class Program
	{
		static void Main()
		{
			UpdateApplications();
			Con.ReadLine();
		}

		private static void UpdateApplications()
		{
			IMarketplaceCrawler crawler = new MarketplaceCrawler();
			int counter = 0;
			int addedNew = 0;
			int updated = 0;
			int statisticsUpdated = 0;
			int rankUpdated = 0;
			DateTime startDate = DateTime.Now;

			crawler.UpdateApplications(
				new MarketplaceRegion("en-US"),
				"Zest",
				false,
				(app, action, ex) => 
					HandleApplicationUpdate(
						app, action, ex, startDate, ++counter,
						ref addedNew, ref updated, ref statisticsUpdated, ref rankUpdated
						)
				);
		}

		private static bool HandleApplicationUpdate(
			Application application,
			UpdateApplicationAction action,
			Exception exception,
			DateTime startDate,
			int counter,
			ref int addedNew,
			ref int updated,
			ref int statisticsUpdated,
			ref int rankUpdated
			)
		{
			Con.WriteLine("Entry {0}", counter);
			Con.WriteLine("Application: {0}", application.Title);
			Con.WriteLine("Update action: {0}", action);
			if (exception != null)
			Con.WriteLine("Exception: {0}", exception);
			if (action == UpdateApplicationAction.AddedNew) ++addedNew;
			if ((action & UpdateApplicationAction.Updated) == UpdateApplicationAction.Updated) ++updated;
			if ((action & UpdateApplicationAction.UpdatedUserRatingStatistics) == UpdateApplicationAction.UpdatedUserRatingStatistics) ++statisticsUpdated;
			if ((action & UpdateApplicationAction.UpdatedDownloadRank) == UpdateApplicationAction.UpdatedDownloadRank) ++rankUpdated;
			Con.WriteLine("{0} since started", DateTime.Now - startDate);
			Con.WriteLine("Added: {0}, Updated: {1}, StatsUpdated: {2}, RankUpdated: {3}", addedNew, updated, statisticsUpdated, rankUpdated);
			Con.WriteLine("========================================");

			if (counter % 100 == 0 && counter > 0)
			{
				Thread.Sleep(1000);
			}

			return exception == null;
		}
	}
}
