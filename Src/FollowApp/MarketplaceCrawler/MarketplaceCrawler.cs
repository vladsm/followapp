﻿using System;
using System.Collections.Generic;
using System.Linq;

using FollowApp.Collections;
using FollowApp.DataAccess;
using FollowApp.DataAccess.Entities;
using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler
{
	public sealed class MarketplaceCrawler : IMarketplaceCrawler
	{
		private readonly IMarketplaceReader _marketplaceReader;
		private readonly Func<IApplicationsEntitiesContext> _entitiesContextCreator;
		private readonly IDateTimeProvider _datetimeProvider;

		#region Constructors

		public MarketplaceCrawler(
			[NotNull] IMarketplaceReader marketplaceReader,
			[NotNull] Func<IApplicationsEntitiesContext> entitiesContextCreator,
			[NotNull] IDateTimeProvider datetimeProvider
			)
		{
			if (marketplaceReader == null) throw new ArgumentNullException("marketplaceReader");
			if (entitiesContextCreator == null) throw new ArgumentNullException("entitiesContextCreator");
			if (datetimeProvider == null) throw new ArgumentNullException("datetimeProvider");

			_marketplaceReader = marketplaceReader;
			_entitiesContextCreator = entitiesContextCreator;
			_datetimeProvider = datetimeProvider;
		}

		public MarketplaceCrawler()
		{
			_marketplaceReader = new MarketplaceReader();
			// TODO: Refactor to replace it with the ServiceLocator call.
			_entitiesContextCreator = () => new EntitiesContextFactory().CreateApplicationsEntitiesContext();
			_datetimeProvider = new SystemDateTimeProvider();
		}

		#endregion

		public void UpdateApplications(
			MarketplaceRegion region,
			string store,
			bool withDownloadRanks,
			UpdateApplicationsCallback callback
			)
		{
			if (region == null) throw new ArgumentNullException("region");
			if (store == null) throw new ArgumentNullException("store");

			callback = callback ?? ((application, action, exception) => true);

			var query = new SearchApplicationsQuery(region, MarketplaceClientType.WinMobile71)
			{
				Store = store,
				SearchString = null,
				ChunkSize = SearchApplicationsChunkSize.Hundred,
				SortOrder = withDownloadRanks ? 
					SearchApplicationsSortOrder.TopDownloadRankedGoesFirst :
					SearchApplicationsSortOrder.LastReleasedGoesFirst
			};
			var now = _datetimeProvider.UtcNow;
			var searchApplicationsResult = _marketplaceReader.SearchApplications(query);

			MarketplaceRegionEntity marketplaceRegionEntity;
			using (var context = _entitiesContextCreator())
			{
				marketplaceRegionEntity = GetMarketplaceRegionEntity(context, region.Code);
			}

			int orderIndex = 1;

			while (searchApplicationsResult != null)
			{
				var applications = searchApplicationsResult.Applications.ToArray();

				foreach (var application in applications)
				{
					Exception updatingException = null;
					UpdateApplicationAction action = UpdateApplicationAction.Nothing;

					try
					{
						using (var context = _entitiesContextCreator())
						{
							action = TryUpdateApplication(
								context,
								application,
								marketplaceRegionEntity,
								now,
								withDownloadRanks ? orderIndex++ : (int?) null
								);
						}
					}
					catch (Exception exception)
					{
						updatingException = exception;
					}

					bool continueUpdate = callback(application, action, updatingException);
					if (!continueUpdate) return;
				}

				searchApplicationsResult = searchApplicationsResult.HasMore ? searchApplicationsResult.Next() : null;
			}
		}

		private UpdateApplicationAction TryUpdateApplication(
			IApplicationsEntitiesContext entitiesContext,
			Application application,
			MarketplaceRegionEntity marketplaceRegionEntity,
			DateTime loadDate,
			int? orderIndex
			)
		{
			UpdateApplicationAction action = UpdateApplicationAction.Nothing;

			entitiesContext.MarketplaceRegions.Attach(marketplaceRegionEntity);

			ApplicationEntity applicationEntity = entitiesContext.Applications.FirstOrDefault(
				a => a.MarketplaceApplicationId == application.Id && a.MarketplaceRegion.Code == marketplaceRegionEntity.Code
				);
			bool applicationChanged = true;
			if (applicationEntity == null)
			{
				applicationEntity = new ApplicationEntity
				{
					MarketplaceApplicationId = application.Id,
					MarketplaceRegion = marketplaceRegionEntity,
					Publisher = AddPublisher(entitiesContext, application.Publisher),
					DownloadRanks = orderIndex.HasValue ? 
						new List<ApplicationDownloadRankEntity>
						{
							new ApplicationDownloadRankEntity
							{
								Rank = orderIndex.Value,
								DateFrom = loadDate,
								DateTo = null
							}
						} :
						new List<ApplicationDownloadRankEntity>()
				};
				entitiesContext.Applications.Add(applicationEntity);
				action = UpdateApplicationAction.AddedNew;
			}
			else
			{
				if (orderIndex.HasValue && AddDownloadRank(applicationEntity, orderIndex.Value, loadDate))
				{
					action |= UpdateApplicationAction.UpdatedDownloadRank;
				}
				
				ApplicationAttributesEntity previousVersion =
					entitiesContext.ApplicationAttributes.First(a =>
						a.ApplicationId == applicationEntity.Id && a.DateTo == null
						);
				applicationChanged = IsApplicationChanged(application, previousVersion);
				if (applicationChanged)
				{
					previousVersion.DateTo = loadDate;
					action |= UpdateApplicationAction.Updated;
				}
				else
				{
					if (TryUpdateUserRatings(previousVersion, application, loadDate))
					{
						action |= UpdateApplicationAction.UpdatedUserRatingStatistics;
					}
				}
			}

			if (applicationChanged)
			{
				var applicationAttributesEntity = new ApplicationAttributesEntity
				{
					Application = applicationEntity,
					DateFrom = loadDate,
					DateTo = null,
					Title = application.Title,
					SortTitle = application.SortTitle,
					ShortDescription = application.Description,
					Version = application.Version,
					ReleaseDate = application.ReleaseDate,
					SearchImageId = application.Image.Id,
					Categories = new List<ApplicationCategoryEntity>(),
					Offers = new List<ApplicationOfferEntity>(),
					UserRatings = new List<ApplicationUserRatingStatisticsEntity>()
				};
				AddCategories(entitiesContext, applicationAttributesEntity, application.Categories, marketplaceRegionEntity);
				AddOffers(applicationAttributesEntity, application.Offers);
				AddUserRatings(applicationAttributesEntity, application, loadDate);

				entitiesContext.ApplicationAttributes.Add(applicationAttributesEntity);
			}

			if (action != UpdateApplicationAction.Nothing)
			{
				entitiesContext.SaveChanges();
			}

			return action;
		}

		private bool AddDownloadRank(ApplicationEntity applicationEntity, int rank, DateTime loadDate)
		{
			var previousRankRecord = applicationEntity.DownloadRanks.FirstOrDefault(r => r.DateTo == null);
			if (previousRankRecord != null && previousRankRecord.Rank == rank) return false;

			if (previousRankRecord != null)
			{
				previousRankRecord.DateTo = loadDate;
			}
			applicationEntity.DownloadRanks.Add(
				new ApplicationDownloadRankEntity
				{
					ApplicationId = applicationEntity.Id,
					DateFrom = loadDate,
					DateTo = null,
					Rank = rank
				}
				);
			return true;
		}

		private bool TryUpdateUserRatings(
			ApplicationAttributesEntity applicationAttributesEntity,
			Application application,
			DateTime now
			)
		{
			var previousVersion = applicationAttributesEntity.UserRatings.Single(r => r.DateTo == null);

			const double epsilon = 0.0000001;
			if (
				previousVersion.Count == application.UserRatingCount &&
				Math.Abs(previousVersion.Average - application.AverageUserRating) < epsilon &&
				previousVersion.LastInstanceCount == application.LastInstanceUserRatingCount &&
				Math.Abs(previousVersion.LastInstanceAverage - application.AverageLastInstanceUserRating) < epsilon
				)
			{
				return false;
			}
			
			previousVersion.DateTo = now;
			applicationAttributesEntity.UserRatings.Add(
				new ApplicationUserRatingStatisticsEntity
				{
					ApplicationAttributesId = applicationAttributesEntity.Id,
					DateFrom = now,
					DateTo = null,
					Count = application.UserRatingCount,
					Average = application.AverageUserRating,
					LastInstanceCount = application.LastInstanceUserRatingCount,
					LastInstanceAverage = application.AverageLastInstanceUserRating
				}
				);

			return true;
		}

		private void AddUserRatings(
			ApplicationAttributesEntity applicationAttributesEntity,
			Application application,
			DateTime addDate
			)
		{
			applicationAttributesEntity.UserRatings.Add(
				new ApplicationUserRatingStatisticsEntity
				{
					DateFrom = addDate,
					DateTo = null,
					Count = application.UserRatingCount,
					Average = application.AverageUserRating,
					LastInstanceCount = application.LastInstanceUserRatingCount,
					LastInstanceAverage = application.AverageLastInstanceUserRating
				}
				);
		}

		private void AddOffers(
			ApplicationAttributesEntity applicationAttributesEntity,
			IEnumerable<ApplicationOffer> offers
			)
		{
			foreach (var offer in offers)
			{
				var offerEntity = new ApplicationOfferEntity
				{
					MarketplaceOfferId = offer.Id,
					MarketplaceMediaInstanceId = offer.MediaInstanceId,
					PaymentTypes = (int)offer.PaymentTypes,
					Price = offer.Price,
					PriceCurrencyCode = offer.PriceCurrencyCode,
					DisplayPrice = offer.DisplayPrice,
					LicenseRight = (int)offer.LicenseRight,
					Store = offer.Store
				};
				applicationAttributesEntity.Offers.Add(offerEntity);
			}
		}

		private void AddCategories(
			IApplicationsEntitiesContext entitiesContext,
			ApplicationAttributesEntity applicationAttributesEntity,
			IEnumerable<ApplicationCategory> categories,
			MarketplaceRegionEntity marketplaceRegionEntity
			)
		{
			var categoriesDictionary = categories.ToDictionary(c => c.Id);
			var addedCategories = new Dictionary<string, ApplicationCategoryEntity>();
			foreach (var category in categoriesDictionary.Values)
			{
				ApplicationCategoryEntity categoryEntity = entitiesContext.Categories.SingleOrDefault(c =>
					c.Code == category.Id && c.MarketplaceRegion.Code == marketplaceRegionEntity.Code
					);

				if (categoryEntity != null)
				{
					applicationAttributesEntity.Categories.Add(categoryEntity);
					continue;
				}

				if (!addedCategories.TryGetValue(category.Id, out categoryEntity))
				{
					categoryEntity = new ApplicationCategoryEntity { Code = category.Id, Title = category.Title, MarketplaceRegion = marketplaceRegionEntity};
					addedCategories.Add(category.Id, categoryEntity);
				}

				if (category.ParentId != null)
				{
					ApplicationCategoryEntity parentCategoryEntity =
						entitiesContext.Categories.SingleOrDefault(c => c.Code == category.ParentId && c.MarketplaceRegion.Code == marketplaceRegionEntity.Code);

					if (parentCategoryEntity == null)
					{
						if (!addedCategories.TryGetValue(category.ParentId, out parentCategoryEntity))
						{
							ApplicationCategory parentCategory;
							if (categoriesDictionary.TryGetValue(category.ParentId, out parentCategory))
							{
								parentCategoryEntity = new ApplicationCategoryEntity
								{
									Code = parentCategory.Id,
									Title = parentCategory.Title,
									MarketplaceRegion = marketplaceRegionEntity
									//MarketplaceRegion = new MarketplaceRegionEntity {Id = marketplaceRegionEntity.Id}
								};
								addedCategories.Add(category.ParentId, parentCategoryEntity);
								categoryEntity.ParentCategory = parentCategoryEntity;
							}
						}
					}
					if (parentCategoryEntity != null)
					{
						categoryEntity.ParentCategory = parentCategoryEntity;
					}
				}
				applicationAttributesEntity.Categories.Add(categoryEntity);
			}
		}

		private class ApplicationOfferEqualityComparer : IEqualityComparer<ApplicationOffer>
		{
			public bool Equals(ApplicationOffer x, ApplicationOffer y)
			{
				if (x == null && y == null) return true;
				if (x == null || y == null) return false;
				return 
					x.PaymentTypes == y.PaymentTypes &&
					x.Price == y.Price &&
					x.DisplayPrice == y.DisplayPrice &&
					x.PriceCurrencyCode == y.PriceCurrencyCode &&
					x.LicenseRight == y.LicenseRight;
			}

			public int GetHashCode(ApplicationOffer obj)
			{
				int result = obj.PaymentTypes.GetHashCode();
				result = (result * 397) ^ obj.Price.GetHashCode();
				result = (result * 397) ^ (obj.DisplayPrice != null ? obj.DisplayPrice.GetHashCode() : 0);
				result = (result * 397) ^ (obj.PriceCurrencyCode != null ? obj.PriceCurrencyCode.GetHashCode() : 0);
				result = (result * 397) ^ obj.LicenseRight.GetHashCode();
				return result;
			}
		}

		private bool IsApplicationChanged(
			Application loadedApplication,
			ApplicationAttributesEntity storedApplicationAttributes
			)
		{
			// checking base attributes
			if (
				loadedApplication.Title != storedApplicationAttributes.Title ||
				loadedApplication.SortTitle != storedApplicationAttributes.SortTitle ||
				loadedApplication.Description != storedApplicationAttributes.ShortDescription ||
				loadedApplication.Version != storedApplicationAttributes.Version ||
				loadedApplication.ReleaseDate != storedApplicationAttributes.ReleaseDate ||
				loadedApplication.Image.Id != storedApplicationAttributes.SearchImageId
				)
			{
				return true;
			}

			// checking categories
			var categoryEqualityComparer = new ApplicationCategoryEqualityComparer();
			var storedCategories = storedApplicationAttributes.Categories.Select(c => 
				new ApplicationCategory { Id = c.Code, Title = c.Title }
				);
			if (!loadedApplication.Categories.IsEquivalentTo(storedCategories, categoryEqualityComparer))
			{
				return true;
			}

			// checking offers
			var offerEqualityComparer = new ApplicationOfferEqualityComparer();
			var storedOffers = storedApplicationAttributes.Offers.Select(o =>
				new ApplicationOffer
				{
					PaymentTypes = (PaymentTypes)o.PaymentTypes,
					Price = o.Price,
					DisplayPrice = o.DisplayPrice,
					PriceCurrencyCode = o.PriceCurrencyCode,
					LicenseRight = (LicenseRight)o.LicenseRight
				}
				);
			if (!loadedApplication.Offers.IsEquivalentTo(storedOffers, offerEqualityComparer))
			{
				return true;
			}

			return false;
		}

		private MarketplaceRegionEntity GetMarketplaceRegionEntity(
			IApplicationsEntitiesContext entitiesContext,
			string code
			)
		{
			var marketplaceRegionEntity = entitiesContext.MarketplaceRegions.FirstOrDefault(r => r.Code == code);
			if (marketplaceRegionEntity == null)
			{
				marketplaceRegionEntity = entitiesContext.MarketplaceRegions.Add(new MarketplaceRegionEntity {Code = code});
				entitiesContext.SaveChanges();
			}
			return marketplaceRegionEntity;
		}

		private PublisherEntity AddPublisher(IApplicationsEntitiesContext entitiesContext, PublisherReference publisher)
		{
			if (publisher == null) return null;

			PublisherEntity publisherEntity = 
				entitiesContext.Publishers.FirstOrDefault(p => p.Code == publisher.Id) ??
				entitiesContext.Publishers.Add(new PublisherEntity {Code = publisher.Id, Name = publisher.Name});
			
			return publisherEntity;
		}
	}
}
