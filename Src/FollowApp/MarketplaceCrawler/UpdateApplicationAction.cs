using System;

namespace FollowApp.MarketplaceCrawler
{
	[Flags]
	public enum UpdateApplicationAction
	{
		Nothing = 0,
		AddedNew = 0x1,
		Updated = 0x10,
		UpdatedUserRatingStatistics = 0x100,
		UpdatedDownloadRank = 0x1000
	}
}
