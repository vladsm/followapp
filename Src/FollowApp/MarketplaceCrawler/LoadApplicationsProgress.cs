namespace FollowApp.MarketplaceCrawler
{
	/// <summary>
	/// Provides the information about application loading progress.
	/// </summary>
	public sealed class LoadApplicationsProgress
	{
		/// <summary>
		/// Gets the flag showing that there are no more applications expected to be loaded.
		/// </summary>
		public bool Finished { get; private set; }
		
		/// <summary>
		/// Gets the number of the applications loaded since the process was started by the time.
		/// </summary>
		public int TotalLoadedCount { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="LoadApplicationsProgress"/>.
		/// </summary>
		/// <param name="finished">
		/// The flag showing that there are no more applications expected to be loaded.
		/// </param>
		/// <param name="totalLoadedCount">
		/// The number of the applications loaded since the process was started by the time.
		/// </param>
		public LoadApplicationsProgress(bool finished, int totalLoadedCount)
		{
			Finished = finished;
			TotalLoadedCount = totalLoadedCount;
		}
	}
}
