using System;
using System.ServiceModel.Syndication;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	internal class SyndicationSimpleElementExtensionReader<T> : ISyndicationElementExtensionReader<T>
	{
		public T Read(SyndicationElementExtension elementExtension)
		{
			return elementExtension.GetObject<T>();
		}
	}

	internal sealed class SyndicationStringElementExtensionReader : SyndicationSimpleElementExtensionReader<string>
	{
	}

	internal sealed class SyndicationInt32ElementExtensionReader : SyndicationSimpleElementExtensionReader<int>
	{
	}

	internal sealed class SyndicationDecimalElementExtensionReader : SyndicationSimpleElementExtensionReader<decimal>
	{
	}

	internal sealed class SyndicationDoubleElementExtensionReader : SyndicationSimpleElementExtensionReader<double>
	{
	}

	internal sealed class SyndicationDateTimeElementExtensionReader : SyndicationSimpleElementExtensionReader<DateTime>
	{
	}
}
