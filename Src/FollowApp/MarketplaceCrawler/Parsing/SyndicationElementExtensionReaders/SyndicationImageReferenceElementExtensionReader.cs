using System;
using System.Xml.Linq;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	internal sealed class SyndicationImageReferenceElementExtensionReader
		: SyndicationCustomElementExtensionReader<ImageReference>
	{
		protected override ImageReference Read([NotNull] XElement rootExtensionElement)
		{
			if (rootExtensionElement == null) throw new ArgumentNullException("rootExtensionElement");
			XNamespace ns = rootExtensionElement.Name.Namespace;
			EnsureChildElements(rootExtensionElement, ns + "id");

			var idElement = rootExtensionElement.Element(ns + "id");
			if (idElement == null) throw new InvalidOperationException("Sub element <id /> is expected.");

			return new ImageReference(ReadUuid(idElement.Value));
		}
	}
}
