using System;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml.Linq;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	public abstract class SyndicationCustomElementExtensionReader<TResult>
		: ISyndicationElementExtensionReader<TResult>
	{
		public TResult Read(SyndicationElementExtension elementExtension)
		{
			var categoriesElement = XNode.ReadFrom(elementExtension.GetReader()) as XElement;
			if (categoriesElement == null)
			{
				throw new MarketplaceSyndicationParsingException("Extension element is incorrectly formatted.");
			}
			return Read(categoriesElement);
		}

		protected abstract TResult Read(XElement rootExtensionElement);

		protected static void EnsureChildElements([NotNull] XElement element, [NotNull] XName name)
		{
			if (element == null) throw new ArgumentNullException("element");
			if (name == null) throw new ArgumentNullException("name");

			if (!element.Elements(name).Any())
			{
				throw new MarketplaceSyndicationParsingException(
					string.Format("Child element <{0} /> can not be found.", name)
					);
			}
		}

		protected static void EnsureChildElements([NotNull] XElement element, [NotNull] params XName[] names)
		{
			if (element == null) throw new ArgumentNullException("element");
			if (names == null) throw new ArgumentNullException("names");

			if (names.Length == 1)
			{
				EnsureChildElements(element, names[0]);
			}
			else
			{
				if (!names.All(name => element.Elements(name).Any()))
				{
					throw new MarketplaceSyndicationParsingException(
						string.Format(
							"Some child elements {0} can not be found.",
							string.Join(", ", names.Select(name => string.Format("<{0} />", name)))
							)
						);
				}
			}
		}

		protected static Guid ReadUuid([NotNull] string uuid)
		{
			return UuidReader.ReadMarketplaceId(uuid);
		}
	}
}
