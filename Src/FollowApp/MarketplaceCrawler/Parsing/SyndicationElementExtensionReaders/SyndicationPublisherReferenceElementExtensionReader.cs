using System;
using System.Xml.Linq;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	public class SyndicationPublisherReferenceElementExtensionReader
		: SyndicationCustomElementExtensionReader<PublisherReference>
	{
		protected override PublisherReference Read([NotNull] XElement rootExtensionElement)
		{
			if (rootExtensionElement == null) throw new ArgumentNullException("rootExtensionElement");

			XNamespace ns = rootExtensionElement.Name.Namespace;
			EnsureChildElements(rootExtensionElement, ns + "id", ns + "name");

			return new PublisherReference
			{
				Id = rootExtensionElement.Element(ns + "id").Value,
				Name = rootExtensionElement.Element(ns + "name").Value
			};
		}
	}
}
