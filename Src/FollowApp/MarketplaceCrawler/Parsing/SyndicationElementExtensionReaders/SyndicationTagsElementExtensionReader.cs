using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	public class SyndicationTagsElementExtensionReader
		: SyndicationCustomElementExtensionReader<IEnumerable<Tag>>
	{
		protected override IEnumerable<Tag> Read(XElement rootExtensionElement)
		{
			XNamespace ns = rootExtensionElement.Name.Namespace;
			return rootExtensionElement.Elements(ns + "tag").Select(e => new Tag(e.Value)).ToList();
		}
	}
}
