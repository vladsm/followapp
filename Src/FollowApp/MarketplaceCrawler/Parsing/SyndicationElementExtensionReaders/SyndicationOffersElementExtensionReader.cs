using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using FollowApp.Entities;
using FollowApp.Xml;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	internal sealed class SyndicationOffersElementExtensionReader
		: SyndicationCustomElementExtensionReader<IEnumerable<ApplicationOffer>>
	{
		protected override IEnumerable<ApplicationOffer> Read(XElement rootExtensionElement)
		{
			XNamespace ns = rootExtensionElement.Name.Namespace;
			return rootExtensionElement.Elements(ns + "offer").Select(ConvertToApplicationOffer).ToList();
		}

		private static ApplicationOffer ConvertToApplicationOffer(XElement offerElement)
		{
			XNamespace ns = offerElement.Name.Namespace;
			EnsureChildElements(offerElement, 
				ns + "offerId", ns + "mediaInstanceId", ns + "clientTypes", ns + "paymentTypes",
				ns + "store", ns + "price", ns + "displayPrice", ns + "price", ns + "priceCurrencyCode",
				ns + "priceCurrencyCode", ns + "licenseRight");

			return new ApplicationOffer
			{
				Id = ReadUuid(offerElement.ExistingElement(ns + "offerId").Value),
				MediaInstanceId = ReadUuid(offerElement.ExistingElement(ns + "mediaInstanceId").Value),
				ClientTypes = ConvertToClientTypes(offerElement.Element(ns + "clientTypes")),
				PaymentTypes = ConvertToPaymentTypes(offerElement.Element(ns + "paymentTypes")),
				Store = offerElement.ExistingElement(ns + "store").Value,
				Price = offerElement.ExistingElement(ns + "price").DecimalValue(),
				DisplayPrice = offerElement.ExistingElement(ns + "displayPrice").Value,
				PriceCurrencyCode = offerElement.ExistingElement(ns + "priceCurrencyCode").Value,
				LicenseRight = ConvertToLicenseRights(offerElement.ExistingElement(ns + "licenseRight").Value)
			};
		}

		private static PaymentTypes ConvertToPaymentTypes(XElement paymentTypesElement)
		{
			PaymentTypes result = PaymentTypes.None;
			foreach (var paymentTypeElement in paymentTypesElement.Elements(paymentTypesElement.Name.Namespace + "paymentType"))
			{
				switch (paymentTypeElement.Value)
				{
					case "Credit Card":
						result |= PaymentTypes.CreditCard;
						break;
					case "Mobile Operator":
						result |= PaymentTypes.MobileOperator;
						break;
					default:
						throw new MarketplaceSyndicationParsingException(
							G.Format("Can not convert string \"{0}\" to PaymentTypes enum value.", paymentTypeElement.Value)
							);
				}
			}
			return result;
		}

		private static IEnumerable<MarketplaceClientType> ConvertToClientTypes(XElement clientTypesElement)
		{
			return clientTypesElement.
				Elements(clientTypesElement.Name.Namespace + "clientType").
				Select(e => ConvertToClientType(e.Value)).
				ToList();
		}

		private static MarketplaceClientType ConvertToClientType(string strClientType)
		{
			switch (strClientType)
			{
				case "WinMobile 7.0":
					return MarketplaceClientType.WinMobile70;
				case "WinMobile 7.1":
					return MarketplaceClientType.WinMobile71;
			}
			throw new MarketplaceSyndicationParsingException(
				G.Format("Can not convert string \"{0}\" to MarketplaceClientType enum value.", strClientType)
				);
		}

		private static LicenseRight ConvertToLicenseRights(string strLicenseRight)
		{
			switch (strLicenseRight)
			{
				case "Purchase":
					return LicenseRight.Purchase;
				case "Trial":
					return LicenseRight.Trial;
			}
			throw new MarketplaceSyndicationParsingException(
				G.Format("Can not convert string \"{0}\" to LicenseRight enum value.", strLicenseRight)
				);
		}
	}
}
