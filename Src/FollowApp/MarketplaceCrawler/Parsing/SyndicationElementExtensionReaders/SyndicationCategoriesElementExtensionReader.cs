using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using FollowApp.Entities;
using FollowApp.Xml;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	internal sealed class SyndicationCategoriesElementExtensionReader
		: SyndicationCustomElementExtensionReader<IEnumerable<ApplicationCategory>>
	{
		protected override IEnumerable<ApplicationCategory> Read(XElement rootExtensionElement)
		{
			XNamespace ns = rootExtensionElement.Name.Namespace;
			return rootExtensionElement.Elements(ns + "category").Select(ConvertToApplicationCategory).ToList();
		}

		private static ApplicationCategory ConvertToApplicationCategory(XElement categoryElement)
		{
			XNamespace ns = categoryElement.Name.Namespace;
			EnsureChildElements(categoryElement, ns + "id", ns + "title", ns + "isRoot");

			string parentId;
			if (categoryElement.ExistingElement(ns + "isRoot").Value.Equals("true", StringComparison.OrdinalIgnoreCase))
			{
				parentId = null;
			}
			else
			{
				EnsureChildElements(categoryElement, ns + "parentId");
				parentId = categoryElement.ExistingElement(ns + "parentId").Value;
			}
			return new ApplicationCategory
			{
				Id = categoryElement.ExistingElement(ns + "id").Value,
				Title = categoryElement.ExistingElement(ns + "title").Value,
				ParentId = parentId
			};
		}
	}
}
