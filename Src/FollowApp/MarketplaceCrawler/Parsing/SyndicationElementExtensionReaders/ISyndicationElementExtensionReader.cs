using System.ServiceModel.Syndication;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	internal interface ISyndicationElementExtensionReader<out TResult>
	{
		TResult Read(SyndicationElementExtension elementExtension);
	}
}
