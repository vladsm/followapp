﻿using System;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	internal static class UuidReader
	{
		public static Guid ReadMarketplaceId([NotNull] string uuid)
		{
			if (uuid == null) throw new ArgumentNullException("uuid");
			if (!uuid.StartsWith("urn:uuid:")) throw new ArgumentException("Uuid has no 'urn:uuid:' prefix.", "uuid");

			return Guid.Parse(uuid.Substring(9));
		}
	}
}
