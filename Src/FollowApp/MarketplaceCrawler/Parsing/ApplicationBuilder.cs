using System;
using System.Collections.Generic;
using System.ServiceModel.Syndication;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	internal sealed class ApplicationBuilder
	{
		private static Dictionary<string, IAttributeInitializer> _attributeInitializersDictionary;
		private Application _result = new Application();
		private readonly List<SyndicationElementExtension> _extensions = new List<SyndicationElementExtension>();

		static ApplicationBuilder()
		{
			InitializeAttributeInitializers();
		}

		public void Reset()
		{
			_result = new Application();
			_extensions.Clear();
		}

		public void SetId(Guid id)
		{
			_result.Id = id;
		}

		public void SetTitle([NotNull] string title)
		{
			if (title == null) throw new ArgumentNullException("title");
			_result.Title = title;
		}

		public void AddExtension([NotNull] SyndicationElementExtension elementExtension)
		{
			if (elementExtension == null) throw new ArgumentNullException("elementExtension");
			_extensions.Add(elementExtension);
		}

		public void AddExtensions([NotNull] IEnumerable<SyndicationElementExtension> elementExtensions)
		{
			if (elementExtensions == null) throw new ArgumentNullException("elementExtensions");
			_extensions.AddRange(elementExtensions);
		}

		public Application  Build()
		{
			_result.Categories = new ApplicationCategory[] {};
			_result.Tags = new Tag[] {};
			_result.Offers = new ApplicationOffer[] {};

			foreach (var extension in _extensions)
			{
				IAttributeInitializer attributeInitializer;
				string extensionName = QualifiedElementExtensionName(extension.OuterNamespace, extension.OuterName);
				if (_attributeInitializersDictionary.TryGetValue(extensionName, out attributeInitializer))
				{
					attributeInitializer.Initialize(_result, extension);
				}
			}
			return _result;
		}

		#region AttributeInitializer

		private interface IAttributeInitializer
		{
			void Initialize(
				Application application,
				SyndicationElementExtension elementExtension
				);
		}

		private sealed class AttributeInitializer<TAttribute> : IAttributeInitializer
		{
			private readonly Action<Application, TAttribute> _attributeSetter;
			private readonly ISyndicationElementExtensionReader<TAttribute> _extensionReader;

			public AttributeInitializer(
				[NotNull] Action<Application, TAttribute> attributeSetter,
				[NotNull] ISyndicationElementExtensionReader<TAttribute> extensionReader
				)
			{
				if (attributeSetter == null) throw new ArgumentNullException("attributeSetter");
				if (extensionReader == null) throw new ArgumentNullException("extensionReader");
				_attributeSetter = attributeSetter;
				_extensionReader = extensionReader;
			}

			public void Initialize(
				[NotNull] Application application,
				[NotNull] SyndicationElementExtension elementExtension
				)
			{
				if (application == null) throw new ArgumentNullException("application");
				if (elementExtension == null) throw new ArgumentNullException("elementExtension");
				_attributeSetter(application, _extensionReader.Read(elementExtension));
			}
		}

		private static void SetupAttributeInitializer<TAttribute>(
			[NotNull] string elementExtensionName,
			[NotNull] Action<Application, TAttribute> attributeSetter,
			ISyndicationElementExtensionReader<TAttribute> extensionReader
			)
		{
			if (elementExtensionName == null) throw new ArgumentNullException("elementExtensionName");
			if (attributeSetter == null) throw new ArgumentNullException("attributeSetter");

			_attributeInitializersDictionary.Add(
				QualifiedElementExtensionName(Constants.AppCatalogSchemaNamespace, elementExtensionName),
				new AttributeInitializer<TAttribute>(attributeSetter, extensionReader)
				);
		}

		private static void InitializeAttributeInitializers()
		{
			var stringReader = new SyndicationStringElementExtensionReader();
			var intReader = new SyndicationInt32ElementExtensionReader();
			var doubleReader = new SyndicationDoubleElementExtensionReader();
			var dateTimeReader = new SyndicationDateTimeElementExtensionReader();

			_attributeInitializersDictionary = new Dictionary<string, IAttributeInitializer>();
			SetupAttributeInitializer("sortTitle", (o, v) => o.SortTitle = v, stringReader);
			SetupAttributeInitializer("releaseDate", (o, v) => o.ReleaseDate = v, dateTimeReader);
			SetupAttributeInitializer("shortDescription", (o, v) => o.Description = v, stringReader);
			SetupAttributeInitializer("version", (o, v) => o.Version = v, stringReader);
			SetupAttributeInitializer("averageUserRating", (o, v) => o.AverageUserRating = v, doubleReader);
			SetupAttributeInitializer("userRatingCount", (o, v) => o.UserRatingCount = v, intReader);
			SetupAttributeInitializer("averageLastInstanceUserRating", (o, v) => o.AverageLastInstanceUserRating = v, doubleReader);
			SetupAttributeInitializer("lastInstanceUserRatingCount", (o, v) => o.LastInstanceUserRatingCount = v, intReader);
			SetupAttributeInitializer("image", (o, v) => o.Image = v, new SyndicationImageReferenceElementExtensionReader());
			SetupAttributeInitializer("categories", (o, v) => o.Categories = v, new SyndicationCategoriesElementExtensionReader());
			SetupAttributeInitializer("tags", (o, v) => o.Tags = v, new SyndicationTagsElementExtensionReader());
			SetupAttributeInitializer("offers", (o, v) => o.Offers = v, new SyndicationOffersElementExtensionReader());
			SetupAttributeInitializer("publisher", (o, v) => o.Publisher = v, new SyndicationPublisherReferenceElementExtensionReader());
		}

		#endregion

		private static string QualifiedElementExtensionName(string ns, string name)
		{
			return ns + "/" + name;
		}
	}
}
