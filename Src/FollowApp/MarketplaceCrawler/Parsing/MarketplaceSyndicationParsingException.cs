﻿using System;
using System.Runtime.Serialization;

namespace FollowApp.MarketplaceCrawler.Parsing
{
	public class MarketplaceSyndicationParsingException : Exception
	{
		public MarketplaceSyndicationParsingException()
		{
		}

		public MarketplaceSyndicationParsingException(string message)
			: base(message)
		{
		}

		public MarketplaceSyndicationParsingException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		protected MarketplaceSyndicationParsingException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}