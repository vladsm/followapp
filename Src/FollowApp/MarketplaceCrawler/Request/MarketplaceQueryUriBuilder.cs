using System;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler
{
	internal class MarketplaceQueryUriBuilder
	{
		private const string MarketplaceBaseUrl = "http://catalog.zune.net";

		public Uri GetUri(SearchApplicationsQuery query)
		{
			var builder = new UriQueryBuilder();
			AddCatalogVersion(builder);
			AddRegionQueryParameter(builder, query);
			builder.AddPathParameter("apps");
			builder.AddParameter("clientType", ConvertClientTypeToUriComponentValue(query.ClientType));
			if (query.Store != null)
			{
				builder.AddParameter("store", query.Store);
			}
			if (query.SortOrder != SearchApplicationsSortOrder.None)
			{
				builder.AddParameter("orderby", ConvertSearchApplicationsSortOrderToUriComponentValue(query.SortOrder));
			}
			if (!string.IsNullOrEmpty(query.SearchString))
			{
				builder.AddParameter("q", query.SearchString);
			}
			if (query.ChunkSize != SearchApplicationsChunkSize.Default)
			{
				builder.AddParameter("chunksize", ConvertSearchApplicationsChunkSizeToUriComponentValue(query.ChunkSize));
			}
			return builder.GetQueryUri(MarketplaceBaseUrl);
		}

		#region Helpers

		internal Uri GetAbsoluteUri(Uri relativeUri)
		{
			return new Uri(new Uri(MarketplaceBaseUrl), relativeUri);
		}

		private static void AddCatalogVersion(UriQueryBuilder builder)
		{
			builder.AddPathParameter("v3.2");
		}
		
		private static void AddRegionQueryParameter(UriQueryBuilder builder, MarketplaceQueryBase query)
		{
			builder.AddPathParameter(query.Region.Code);
		}

		private static string ConvertClientTypeToUriComponentValue(MarketplaceClientType clientType)
		{
			switch (clientType)
			{
				case MarketplaceClientType.WinMobile70:
					return "WinMobile 7.0";
				case MarketplaceClientType.WinMobile71:
					return "WinMobile 7.1";
			}
			throw new ArgumentException("Client type is unknown", "clientType");
		}

		private static string ConvertSearchApplicationsSortOrderToUriComponentValue(SearchApplicationsSortOrder sortOrder)
		{
			switch (sortOrder)
			{
				case SearchApplicationsSortOrder.None:
					return "";
				case SearchApplicationsSortOrder.LastReleasedGoesFirst:
					return "releaseDate";
				case SearchApplicationsSortOrder.TopDownloadRankedGoesFirst:
					return "downloadRank";
			}
			throw new ArgumentException("Search applications result sort order value is unknown", "sortOrder");
		}

		private static string ConvertSearchApplicationsChunkSizeToUriComponentValue(SearchApplicationsChunkSize chunkSize)
		{
			switch (chunkSize)
			{
				case SearchApplicationsChunkSize.Default:
					return "";
				case SearchApplicationsChunkSize.Ten:
					return "10";
				case SearchApplicationsChunkSize.Fifty:
					return "50";
				case SearchApplicationsChunkSize.Hundred:
					return "100";
			}
			throw new ArgumentException("Chunk size value is unknown", "chunkSize");
		}

		#endregion
	}
}
