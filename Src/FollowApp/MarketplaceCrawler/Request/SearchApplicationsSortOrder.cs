﻿namespace FollowApp.MarketplaceCrawler
{
	public enum SearchApplicationsSortOrder
	{
		None,
		LastReleasedGoesFirst,
		TopDownloadRankedGoesFirst
	}
}
