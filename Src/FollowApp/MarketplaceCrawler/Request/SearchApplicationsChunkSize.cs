﻿namespace FollowApp.MarketplaceCrawler
{
	public enum SearchApplicationsChunkSize
	{
		Default,
		Ten,
		Fifty,
		Hundred
	}
}
