using System;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler
{
	public class MarketplaceQueryBase
	{
		private MarketplaceRegion _region;

		public MarketplaceRegion Region
		{
			get { return _region; }
			set
			{
				if (value == null) throw new ArgumentNullException("value");
				_region = value;
			}
		}

		public MarketplaceQueryBase([NotNull] MarketplaceRegion region)
		{
			if (region == null) throw new ArgumentNullException("region");
			_region = region;
		}
	}
}
