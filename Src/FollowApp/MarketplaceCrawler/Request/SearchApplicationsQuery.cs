﻿using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler
{
	public class SearchApplicationsQuery : MarketplaceQueryBase
	{
		public MarketplaceClientType ClientType { get; set; }
		public string Store { get; set; }
		public SearchApplicationsSortOrder SortOrder { get; set; }
		public string SearchString { get; set; }
		public SearchApplicationsChunkSize ChunkSize { get; set; }

		public SearchApplicationsQuery(
			[NotNull] MarketplaceRegion region,
			MarketplaceClientType clientType
			)
			: base(region)
		{
			ClientType = clientType;
			SortOrder = SearchApplicationsSortOrder.None;
			ChunkSize = SearchApplicationsChunkSize.Default;
		}
	}
}
