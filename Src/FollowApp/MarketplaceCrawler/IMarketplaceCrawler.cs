using System;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler
{
	/// <summary>
	/// Define the methods for the marketplace crawling.
	/// </summary>
	public interface IMarketplaceCrawler
	{
		/// <summary>
		/// Adds or updates applications in the persistent storage by loading them from the marketplace.
		/// The following information about application is loading: basic application attributes, user rating
		/// statistics, categories, offers, supported client types. The supported languages, screenshot references,
		/// media instances are not loading by this method.
		/// </summary>
		/// <remarks>
		/// If the application loaded from the marketplace is a new one (doesn't exist in a persistent storage) it is
		/// simply added to it. Otherwise there are three cases:
		/// 1) Some attributes of the application except user rating statistics are changed. In this case the new
		/// version of ApplicationAttributes entity will be added into the storage and the previous one will be closed.
		/// 2) User rating statistics attributes only are changed. In this case the new version of the user rating
		/// statistics entity will be added to the existing application attributes entity version.
		/// 3) No changes detected. In this case nothing is changing in the storage.
		/// In addition to the cases 1 and 2 the download rank of the application can be updated.
		/// </remarks>
		/// <param name="region">The marketplace region (country).</param>
		/// <param name="store">The marketplace store.</param>
		/// <param name="updateDownloadRanks">
		/// The flag that is specifying whether the applications download ranks will be updated.
		/// </param>
		/// <param name="callback">
		/// The callback that will be called during the updating process. See <see cref="UpdateApplicationsCallback"/>
		/// </param>
		void UpdateApplications(
			[NotNull] MarketplaceRegion region,
			[NotNull] string store,
			bool updateDownloadRanks,
			[CanBeNull] UpdateApplicationsCallback callback
			);
	}

	/// <summary>
	/// Represents the callback method that is calling out from the applications update process (
	/// <see cref="IMarketplaceCrawler.UpdateApplications"/>). The method is called synchronously after
	/// the next loaded application is processed (added to the storage, updated or skipped).
	/// </summary>
	/// <param name="application">The application that have been processed.</param>
	/// <param name="action">
	/// The action that has been performed with the application in the storage.
	/// The following actions can be performed over the application in the storage:
	/// 1) Nothing;
	/// 2) AddedNew;
	/// 3) Updated;
	/// 4) Updated | UpdatedDownloadRank
	/// 5) UpdatedUserRatingStatistics;
	/// 6) UpdatedUserRatingStatistics | UpdatedDowloadRank;
	/// 7) UpdatedDownloadRank.
	/// </param>
	/// <param name="exception">The exception occured while processing application.</param>
	/// <returns>
	/// If the callback returns false, the updating process will be stopped. Otherwise it will be continued.
	/// </returns>
	public delegate bool UpdateApplicationsCallback(
		Application application,
		UpdateApplicationAction action,
		Exception exception
		);
}
