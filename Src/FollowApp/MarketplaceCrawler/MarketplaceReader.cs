using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml;

using FollowApp.Entities;
using FollowApp.MarketplaceCrawler.Parsing;

namespace FollowApp.MarketplaceCrawler
{
	public class MarketplaceReader : IMarketplaceReader
	{
		private readonly MarketplaceQueryUriBuilder _queryUriBuilder;
		private readonly ContentLoader _contentLoader;

		public SearchApplicationsResult SearchApplications(SearchApplicationsQuery query)
		{
			if (query == null) throw new ArgumentNullException("query");

			var uri = _queryUriBuilder.GetUri(query);
			return LoadApplications(uri);
		}


		[NotNull]
		internal SearchApplicationsResult LoadApplications([NotNull] Uri uri)
		{
			using (XmlReader xmlReader = _contentLoader.LoadXmlContent(uri))
			{
				var feed = SyndicationFeed.Load(xmlReader);
				if (feed == null)
				{
					throw new MarketplaceSyndicationParsingException("Loaded string has incorrect format.");
				}

				var builder = new ApplicationBuilder();
				var resultItems = new List<Application>();
				foreach (var feedItem in feed.Items)
				{
					builder.SetId(UuidReader.ReadMarketplaceId(feedItem.Id));
					builder.SetTitle(feedItem.Title.Text);
					builder.AddExtensions(feedItem.ElementExtensions);
					resultItems.Add(builder.Build());
					builder.Reset();
				}
				SyndicationLink nextLink = feed.Links.
					FirstOrDefault(link => link.RelationshipType.Equals("next", StringComparison.OrdinalIgnoreCase));
				return new SearchApplicationsResult(
					resultItems,
					nextLink == null ?
						(Func<SearchApplicationsResult>)null :
						() => LoadApplications(_queryUriBuilder.GetAbsoluteUri(nextLink.Uri))
					);
			}
		}

		#region Constructors

		public MarketplaceReader()
		{
			_queryUriBuilder = new MarketplaceQueryUriBuilder();
			_contentLoader = new ContentLoader();
		}

		internal MarketplaceReader(
			[NotNull] MarketplaceQueryUriBuilder queryUriBuilder,
			[NotNull] ContentLoader contentLoader
			)
		{
			if (queryUriBuilder == null) throw new ArgumentNullException("queryUriBuilder");
			if (contentLoader == null) throw new ArgumentNullException("contentLoader");

			_queryUriBuilder = queryUriBuilder;
			_contentLoader = contentLoader;
		}

		#endregion
	}
}
