using System;
using System.Xml;

namespace FollowApp.MarketplaceCrawler
{
	internal class ContentLoader
	{
		public virtual XmlReader LoadXmlContent(Uri uri)
		{
			return XmlReader.Create(uri.AbsoluteUri);
		}
	}
}
