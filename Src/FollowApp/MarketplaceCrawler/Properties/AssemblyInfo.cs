﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FollowApp.MarketplaceCrawler")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vladimir Smirnov")]
[assembly: AssemblyProduct("FollowApp")]
[assembly: AssemblyCopyright("Copyright © Vladimir Smirnov (vladsm@gmail.com) 2011")]
[assembly: AssemblyTrademark("FollowApp")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("f01df1de-df5d-40cc-afe6-5f19b227a0ab")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("FollowApp.MarketplaceCrawler.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100457c7e834fd6340ba8792f9ea71ba288d972220fb1ed8c6f645179f3cd20e4ce80892d0b9fcf6e4b9ea00783f10169df1f35895cd53ec160145c48d39c91f000b5f528f9afaae697eeb005efa28dad5193a26dadcf918d8987c6884d094b4d021a244501665685aef2559d5d894a97de0b03d1f215e934eba12ccec8f8c1d8ea")]
