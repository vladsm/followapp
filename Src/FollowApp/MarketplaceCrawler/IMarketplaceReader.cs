namespace FollowApp.MarketplaceCrawler
{
	public interface IMarketplaceReader
	{
		[NotNull]
		SearchApplicationsResult SearchApplications([NotNull] SearchApplicationsQuery query);
	}
}
