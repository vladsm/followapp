﻿using System;
using System.Collections.Generic;

using FollowApp.Entities;

namespace FollowApp.MarketplaceCrawler
{
	public class SearchApplicationsResult
	{
		private readonly Func<SearchApplicationsResult> _nextChunkGetter;
		private readonly List<Application> _applications;

		[NotNull]
		public IEnumerable<Application> Applications
		{
			get { return _applications; }
		}

		[NotNull]
		public SearchApplicationsResult Next()
		{
			if (_nextChunkGetter == null) throw new InvalidOperationException("No more applications available");

			return _nextChunkGetter();
		}

		public bool HasMore
		{
			get { return _nextChunkGetter != null; }
		}

		internal SearchApplicationsResult(
			[NotNull] IEnumerable<Application> applications,
			[CanBeNull] Func<SearchApplicationsResult> nextChunkGetter
			)
		{
			if (applications == null) throw new ArgumentNullException("applications");
			
			_applications = new List<Application>(applications);
			_nextChunkGetter = nextChunkGetter;
		}
	}
}
