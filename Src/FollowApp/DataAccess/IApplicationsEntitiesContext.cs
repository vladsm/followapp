﻿using FollowApp.DataAccess.Entities;

namespace FollowApp.DataAccess
{
	public interface IApplicationsEntitiesContext : IEntitiesContext
	{
		IEntitySet<ApplicationEntity> Applications { get; }
		IEntitySet<ApplicationAttributesEntity> ApplicationAttributes { get; }
		IEntitySet<MarketplaceRegionEntity> MarketplaceRegions { get; }
		IEntitySet<PublisherEntity> Publishers { get; }
		IEntitySet<ApplicationCategoryEntity> Categories { get; }
		IEntitySet<MarketplaceClientTypeDictionaryItem> MarketplaceClientTypes { get; }
		IEntitySet<DeviceCapabilityEntity> Capabilities { get; }
		IEntitySet<LanguageEntity> Languages { get; }
	}
}
