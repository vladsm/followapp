﻿using System.Linq;

namespace FollowApp.DataAccess
{
	public interface IEntitySet<TEntity> : IQueryable<TEntity> where TEntity : class
	{
		TEntity Find(params object[] keyValues);
		TEntity Add(TEntity entity);
		TEntity Remove(TEntity entity);
		TEntity Attach(TEntity entity);
		TEntity Create();
		TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, TEntity;
	}
}
