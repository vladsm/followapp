namespace FollowApp.DataAccess.Entities
{
	public class MarketplaceClientTypeDictionaryItem
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
