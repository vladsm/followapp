using System;
using System.Collections.Generic;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationEntity
	{
		public int Id { get; set; }
		public Guid MarketplaceApplicationId { get; set; }

		public virtual MarketplaceRegionEntity MarketplaceRegion { get; set; }
		public virtual PublisherEntity Publisher { get; set; }

		public virtual ICollection<ApplicationDownloadRankEntity> DownloadRanks { get; set; }
	}
}
