using System.Collections.Generic;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationCategoryEntity
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Title { get; set; }
		public int? ParentId { get; set; }

		public virtual MarketplaceRegionEntity MarketplaceRegion { get; set; }
		public virtual ApplicationCategoryEntity ParentCategory { get; set; }
		public virtual ICollection<ApplicationCategoryEntity> ChildCategories { get; set; }
	}
}
