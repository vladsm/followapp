namespace FollowApp.DataAccess.Entities
{
	public class TagEntity
	{
		public int Id { get; set; }
		public string Code { get; set; }
	}
}
