namespace FollowApp.DataAccess.Entities
{
	public class DeviceCapabilityDisclosureEntity
	{
		public int ApplicationMediaInstanceId { get; set; }
		public int DeviceCapabilityId { get; set; }
		public byte DisclosureType { get; set; }

		public virtual DeviceCapabilityEntity DeviceCapability { get; set; }
	}
}
