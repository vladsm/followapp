namespace FollowApp.DataAccess.Entities
{
	public class PublisherEntity
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
	}
}
