namespace FollowApp.DataAccess.Entities
{
	public class DeviceCapabilityEntity
	{
		public int Id { get; set; }
		public string Code { get; set; }
	}
}
