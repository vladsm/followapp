using System;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationMediaInstanceUserRatingStatisticsEntity
	{
		public int Id { get; set; }
		public int MediaInstanceId { get; set; }
		public DateTime DateFrom { get; set; }
		public DateTime? DateTo { get; set; }
		public double Average { get; set; }
		public int Count { get; set; }
	}
}