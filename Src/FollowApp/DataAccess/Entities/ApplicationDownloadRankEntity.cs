using System;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationDownloadRankEntity
	{
		public int Id { get; set; }
		public int ApplicationId { get; set; }
		public DateTime DateFrom { get; set; }
		public DateTime? DateTo { get; set; }
		public int Rank { get; set; }
	}
}
