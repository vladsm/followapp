using System;
using System.Collections.Generic;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationMediaInstanceEntity
	{
		public int Id { get; set; }
		public Guid MarketplaceMediaInstanceId { get; set; }
		public string Title { get; set; }
		public string Version { get; set; }
		public string Url { get; set; }
		public int PackageSize { get; set; }
		public int InstallSize { get; set; }

		public virtual ICollection<MarketplaceClientTypeDictionaryItem> ClientTypes { get; set; }
		public virtual ICollection<LanguageEntity> SupportedLanguages { get; set; }
		public virtual ICollection<DeviceCapabilityDisclosureEntity> Capabilities { get; set; }
		public virtual ICollection<ApplicationMediaInstanceUserRatingStatisticsEntity> UserRatings { get; set; }
	}
}
