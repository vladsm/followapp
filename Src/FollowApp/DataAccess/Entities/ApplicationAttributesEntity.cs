using System;
using System.Collections.Generic;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationAttributesEntity
	{
		public int Id { get; set; }
		public int ApplicationId { get; set; }
		public DateTime DateFrom { get; set; }
		public DateTime? DateTo { get; set; }

		public string Title { get; set; }
		public string SortTitle { get; set; }
		public string Description { get; set; }
		public string ShortDescription { get; set; }
		public DateTime ReleaseDate { get; set; }
		public string Version { get; set; }
		public Guid? ImageId { get; set; }
		public Guid? SearchImageId { get; set; }
		public Guid? BackgroundImageId { get; set; }
		public string VisibilityStatus { get; set; }

		public virtual ApplicationEntity Application { get; set; }
		public virtual ICollection<ApplicationUserRatingStatisticsEntity> UserRatings { get; set; }
		public virtual ICollection<ApplicationCategoryEntity> Categories { get; set; }
		public virtual ICollection<TagEntity> Tags { get; set; }
		public virtual ICollection<ApplicationOfferEntity> Offers { get; set; }
		public virtual ICollection<ApplicationMediaInstanceEntity> MediaInstances { get; set; }
		public virtual ICollection<ApplicationScreeshotEntity> Screenshots { get; set; }
	}
}
