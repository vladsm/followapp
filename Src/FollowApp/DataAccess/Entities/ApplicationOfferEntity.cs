using System;
using System.Collections.Generic;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationOfferEntity
	{
		public int Id { get; set; }
		public Guid MarketplaceOfferId { get; set; }
		public Guid MarketplaceMediaInstanceId { get; set; }
		public int PaymentTypes { get; set; }
		public string Store { get; set; }
		public decimal Price { get; set; }
		public string DisplayPrice { get; set; }
		public string PriceCurrencyCode { get; set; }
		public int LicenseRight { get; set; }
		
		public virtual ICollection<MarketplaceClientTypeDictionaryItem> ClientTypes { get; set; }
	}
}
