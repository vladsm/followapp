﻿using System;

namespace FollowApp.DataAccess.Entities
{
	public class ApplicationUserRatingStatisticsEntity
	{
		public int Id { get; set; }
		public int ApplicationAttributesId { get; set; }
		public DateTime DateFrom { get; set; }
		public DateTime? DateTo { get; set; }
		public double Average { get; set; }
		public int Count { get; set; }
		public double LastInstanceAverage { get; set; }
		public int LastInstanceCount { get; set; }
	}
}
