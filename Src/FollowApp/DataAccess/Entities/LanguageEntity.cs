namespace FollowApp.DataAccess.Entities
{
	public class LanguageEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
