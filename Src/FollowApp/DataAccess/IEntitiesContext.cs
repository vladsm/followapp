using System;

namespace FollowApp.DataAccess
{
	public interface IEntitiesContext : IDisposable
	{
		int SaveChanges();
	}
}
