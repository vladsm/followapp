﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FollowApp.DataAccess")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vladimir Smirnov")]
[assembly: AssemblyProduct("FollowApp")]
[assembly: AssemblyCopyright("Copyright © Vladimir Smirnov (vladsm@gmail.com) 2011")]
[assembly: AssemblyTrademark("FollowApp")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("85d943b6-e666-49f5-88bc-812fbcb7b7a2")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
