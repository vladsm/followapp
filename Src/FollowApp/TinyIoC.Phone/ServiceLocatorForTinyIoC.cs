﻿using System;
using System.Collections.Generic;

using FollowApp;

using Microsoft.Practices.ServiceLocation;

namespace TinyIoC
{
	public class ServiceLocatorForTinyIoC : ServiceLocatorImplBase
	{
		private readonly TinyIoCContainer _container;

		public ServiceLocatorForTinyIoC() : this(TinyIoCContainer.Current)
		{
		}

		public ServiceLocatorForTinyIoC([NotNull] TinyIoCContainer container)
		{
			if (container == null) throw new ArgumentNullException("container");
			
			_container = container;
		}

		protected override object DoGetInstance(Type serviceType, string key)
		{
			try
			{
				return key == null ? _container.Resolve(serviceType) : _container.Resolve(serviceType, key);
			}
			catch (Exception exception)
			{
				throw new ActivationException(exception.Message, exception);
			}
		}

		protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
		{
			try
			{
				return _container.ResolveAll(serviceType, true);
			}
			catch (Exception exception)
			{
				throw new ActivationException(exception.Message, exception);
			}
		}
	}
}
