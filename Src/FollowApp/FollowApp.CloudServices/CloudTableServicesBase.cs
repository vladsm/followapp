using System;

using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;

namespace FollowApp.CloudServices
{
	public class CloudTableServicesBase
	{
		private readonly Lazy<TableServiceContext> _tableServiceContext;
		private readonly Lazy<CloudStorageAccount> _storageAccount;

		public CloudTableServicesBase()
		{
			_storageAccount = new Lazy<CloudStorageAccount>(CreateStorageAccount);
			_tableServiceContext = new Lazy<TableServiceContext>(
				() => StorageAccount.CreateCloudTableClient().GetDataServiceContext()
				);
		}

		protected CloudStorageAccount StorageAccount
		{
			get { return _storageAccount.Value; }
		}

		protected TableServiceContext TableServiceContext
		{
			get { return _tableServiceContext.Value; }
		}

		private static CloudStorageAccount CreateStorageAccount()
		{
			return CloudStorageAccount.Parse(
				RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString")
				);
		}
	}
}