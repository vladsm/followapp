using System;

namespace FollowApp.CloudServices.Entitites
{
	internal class ApplicationFollowRecord : FollowRecord
	{
		public string ApplicationId { get; set; }
		public string MarketplaceRegionCode { get; set; }

		public ApplicationFollowRecord(
			[NotNull] string userId,
			[NotNull] string applicationId,
			[NotNull] string marketplaceRegionCode
			)
		{
			if (userId == null) throw new ArgumentNullException("userId");
			if (applicationId == null) throw new ArgumentNullException("applicationId");
			if (marketplaceRegionCode == null) throw new ArgumentNullException("marketplaceRegionCode");

			UserId = userId;
			ApplicationId = applicationId;
			MarketplaceRegionCode = marketplaceRegionCode;
		}

		public override void FillKeys()
		{
			PartitionKey = GetPartitionKey(ApplicationId, MarketplaceRegionCode);
			RowKey = GetRowKey(UserId);
		}

		public static string GetPartitionKey(string applicationId, string marketplaceRegionCode)
		{
			return G.Format(
				"APPID({0})_REGION({1})",
				applicationId.ToUpperInvariant(),
				marketplaceRegionCode.ToUpperInvariant()
				);
		}

		public static string GetRowKey(string userId)
		{
			return G.Format("USERID({0})", userId.ToUpperInvariant());
		}
	}
}
