using Microsoft.WindowsAzure.StorageClient;

namespace FollowApp.CloudServices.Entitites
{
	internal abstract class FollowRecord : TableServiceEntity
	{
		public string UserId { get; set; }

		public abstract void FillKeys();
	}
}
