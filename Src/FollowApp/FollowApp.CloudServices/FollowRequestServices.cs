﻿using FollowApp.CloudServices.Entitites;

namespace FollowApp.CloudServices
{
	public class FollowRequestServices : CloudTableServicesBase, IFollowRequestServices
	{
		public void Register(ApplicationFollowRequest request)
		{
			foreach (var marketplaceRegionCode in request.MarketplaceRegionCodes)
			{
				var record = new ApplicationFollowRecord(request.UserId, request.ApplicationId, marketplaceRegionCode);
				record.FillKeys();
				TableServiceContext.AddObject(CloudTableNames.FollowRecords, record);
			}
			var response = TableServiceContext.SaveChangesWithRetries();
		}
	}
}
