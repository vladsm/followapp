﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;

namespace FollowApp.CloudServices
{
	public class CloudServicesInitializer
	{
		public void EnsureInitialization()
		{
			var storageAccount = CloudStorageAccount.Parse(
				RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString")
				);
			var tableClient = storageAccount.CreateCloudTableClient();

			tableClient.CreateTableIfNotExist(CloudTableNames.FollowRecords);
		}
	}
}
