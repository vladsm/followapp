﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using FollowApp.DataAccess;
using FollowApp.DataAccess.Entities;

using Moq;

namespace FollowApp.UnitTesting.Utils.DataAccess
{
	public class ApplicationsEntitiesContextMockBuilder
	{
		private IEntitySet<ApplicationEntity> _applications;
		private IEntitySet<MarketplaceRegionEntity> _marketplaceRegions;
		private IEntitySet<PublisherEntity> _publishers;
		private IEntitySet<ApplicationAttributesEntity> _applicationAttributes;
		private IEntitySet<ApplicationCategoryEntity> _categories;
		private IEntitySet<MarketplaceClientTypeDictionaryItem> _marketplaceClientTypes;
		private IEntitySet<LanguageEntity> _languages;
		private IEntitySet<DeviceCapabilityEntity> _capabilities;

		public Mock<IApplicationsEntitiesContext> Mock
		{
			get
			{
				var mock = new Mock<IApplicationsEntitiesContext>();

				SetupEntitySet(mock, c => c.Applications, _applications);
				SetupEntitySet(mock, c => c.MarketplaceRegions, _marketplaceRegions);
				SetupEntitySet(mock, c => c.Publishers, _publishers);
				SetupEntitySet(mock, c => c.ApplicationAttributes, _applicationAttributes);
				SetupEntitySet(mock, c => c.Categories, _categories);
				SetupEntitySet(mock, c => c.MarketplaceClientTypes, _marketplaceClientTypes);
				SetupEntitySet(mock, c => c.Languages, _languages);
				SetupEntitySet(mock, c => c.Capabilities, _capabilities);

				SetupEntitySetAdd(mock, c => c.Applications);
				SetupEntitySetAdd(mock, c => c.MarketplaceRegions);
				SetupEntitySetAdd(mock, c => c.Publishers);
				SetupEntitySetAdd(mock, c => c.ApplicationAttributes);
				SetupEntitySetAdd(mock, c => c.Categories);
				SetupEntitySetAdd(mock, c => c.MarketplaceClientTypes);
				SetupEntitySetAdd(mock, c => c.Languages);
				SetupEntitySetAdd(mock, c => c.Capabilities);
				
				return mock;
			}
		}

		public IApplicationsEntitiesContext Object
		{
			get { return Mock.Object; }
		}

		public ApplicationsEntitiesContextMockBuilder WithApplications(
			[NotNull] IEntitySet<ApplicationEntity> applications
			)
		{
			if (applications == null) throw new ArgumentNullException("applications");
			_applications = applications;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithApplications(
			[NotNull] Mock<IEntitySet<ApplicationEntity>> applications
			)
		{
			if (applications == null) throw new ArgumentNullException("applications");
			_applications = applications.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithApplications(
			[NotNull] IEnumerable<ApplicationEntity> applications
			)
		{
			if (applications == null) throw new ArgumentNullException("applications");
			_applications = applications.AsMockedEntitySet();
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithMarketplaceRegions(
			[NotNull] IEntitySet<MarketplaceRegionEntity> marketplaceRegions
			)
		{
			if (marketplaceRegions == null) throw new ArgumentNullException("marketplaceRegions");
			_marketplaceRegions = marketplaceRegions;
			return this;
		}
		
		public ApplicationsEntitiesContextMockBuilder WithMarketplaceRegions(
			[NotNull] Mock<IEntitySet<MarketplaceRegionEntity>> marketplaceRegions
			)
		{
			if (marketplaceRegions == null) throw new ArgumentNullException("marketplaceRegions");
			_marketplaceRegions = marketplaceRegions.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithMarketplaceRegions(
			[NotNull] IEnumerable<MarketplaceRegionEntity> marketplaceRegions
			)
		{
			if (marketplaceRegions == null) throw new ArgumentNullException("marketplaceRegions");
			_marketplaceRegions = marketplaceRegions.AsMockedEntitySet();
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithPublishers(
			[NotNull] IEntitySet<PublisherEntity> publishers
			)
		{
			if (publishers == null) throw new ArgumentNullException("publishers");
			_publishers = publishers;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithPublishers(
			[NotNull] Mock<IEntitySet<PublisherEntity>> publishers
			)
		{
			if (publishers == null) throw new ArgumentNullException("publishers");
			_publishers = publishers.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithPublishers(
			[NotNull] IEnumerable<PublisherEntity> publishers
			)
		{
			if (publishers == null) throw new ArgumentNullException("publishers");
			_publishers = publishers.AsMockedEntitySet();
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithApplicationAttributes(
			[NotNull] IEntitySet<ApplicationAttributesEntity> applicationAttributes
			)
		{
			if (applicationAttributes == null) throw new ArgumentNullException("applicationAttributes");
			_applicationAttributes = applicationAttributes;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithApplicationAttributes(
			[NotNull] Mock<IEntitySet<ApplicationAttributesEntity>> applicationAttributes
			)
		{
			if (applicationAttributes == null) throw new ArgumentNullException("applicationAttributes");
			_applicationAttributes = applicationAttributes.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithApplicationAttributes(
			[NotNull] IEnumerable<ApplicationAttributesEntity> applicationAttributes
			)
		{
			if (applicationAttributes == null) throw new ArgumentNullException("applicationAttributes");
			_applicationAttributes = applicationAttributes.AsMockedEntitySet();
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithCategories(
			[NotNull] IEntitySet<ApplicationCategoryEntity> categories
			)
		{
			if (categories == null) throw new ArgumentNullException("categories");
			_categories = categories;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithCategories(
			[NotNull] Mock<IEntitySet<ApplicationCategoryEntity>> categories
			)
		{
			if (categories == null) throw new ArgumentNullException("categories");
			_categories = categories.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithCategories(
			[NotNull] IEnumerable<ApplicationCategoryEntity> categories
			)
		{
			if (categories == null) throw new ArgumentNullException("categories");
			_categories = categories.AsMockedEntitySet();
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithMarketplaceClientTypes(
			[NotNull] IEntitySet<MarketplaceClientTypeDictionaryItem> marketplaceClientTypes
			)
		{
			if (marketplaceClientTypes == null) throw new ArgumentNullException("marketplaceClientTypes");
			_marketplaceClientTypes = marketplaceClientTypes;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithMarketplaceClientTypes(
			[NotNull] Mock<IEntitySet<MarketplaceClientTypeDictionaryItem>> marketplaceClientTypes
			)
		{
			if (marketplaceClientTypes == null) throw new ArgumentNullException("marketplaceClientTypes");
			_marketplaceClientTypes = marketplaceClientTypes.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithMarketplaceClientTypes(
			[NotNull] IEnumerable<MarketplaceClientTypeDictionaryItem> marketplaceClientTypes
			)
		{
			if (marketplaceClientTypes == null) throw new ArgumentNullException("marketplaceClientTypes");
			_marketplaceClientTypes = marketplaceClientTypes.AsMockedEntitySet();
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithLanguages(
			[NotNull] IEntitySet<LanguageEntity> languages
			)
		{
			if (languages == null) throw new ArgumentNullException("languages");
			_languages = languages;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithLanguages(
			[NotNull] Mock<IEntitySet<LanguageEntity>> languages
			)
		{
			if (languages == null) throw new ArgumentNullException("languages");
			_languages = languages.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithLanguages(
			[NotNull] IEnumerable<LanguageEntity> languages
			)
		{
			if (languages == null) throw new ArgumentNullException("languages");
			_languages = languages.AsMockedEntitySet();
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithCapabilities(
			[NotNull] IEntitySet<DeviceCapabilityEntity> capabilities
			)
		{
			if (capabilities == null) throw new ArgumentNullException("capabilities");
			_capabilities = capabilities;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithCapabilities(
			[NotNull] Mock<IEntitySet<DeviceCapabilityEntity>> capabilities
			)
		{
			if (capabilities == null) throw new ArgumentNullException("capabilities");
			_capabilities = capabilities.Object;
			return this;
		}

		public ApplicationsEntitiesContextMockBuilder WithCapabilities(
			[NotNull] IEnumerable<DeviceCapabilityEntity> capabilities
			)
		{
			if (capabilities == null) throw new ArgumentNullException("capabilities");
			_capabilities = capabilities.AsMockedEntitySet();
			return this;
		}


		private static Mock<T> GetMock<T>(T obj) where T : class
		{
			try
			{
				return Moq.Mock.Get(obj);
			}
			catch (ArgumentException)
			{
				return null;
			}
		}

		private void SetupEntitySet<TEntity>(
			Mock<IApplicationsEntitiesContext> mock,
			Expression<Func<IApplicationsEntitiesContext, IEntitySet<TEntity>>> entitySetGetter,
			IEntitySet<TEntity> configuredEntitySet
			) where TEntity : class
		{
			mock.SetupGet(entitySetGetter).
				Returns(configuredEntitySet ?? EntitySetMock.EmptySet<TEntity>());
		}

		private static void SetupEntitySetAdd<TEntity>(
			Mock<IApplicationsEntitiesContext> mock,
			Func<IApplicationsEntitiesContext, IEntitySet<TEntity>> entitySetGetter
			) where TEntity : class
		{
			var entitiesSetMock = GetMock(entitySetGetter(mock.Object));
			if (entitiesSetMock != null)
			{
				entitiesSetMock.
					Setup(s => s.Add(It.IsAny<TEntity>())).
					Returns<TEntity>(e => e);
			}
		}
	}
}
