using FollowApp.DataAccess;

namespace FollowApp.UnitTesting.Utils.DataAccess
{
	public static class EntitySetMock
	{
		public static IEntitySet<TEntity> EmptySet<TEntity>() where TEntity : class
		{
			return new TEntity[] {}.AsMockedEntitySet();
		}
	}
}
