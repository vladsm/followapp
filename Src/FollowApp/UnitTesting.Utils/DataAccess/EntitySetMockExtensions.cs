using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using FollowApp.DataAccess;

using Moq;

namespace FollowApp.UnitTesting.Utils.DataAccess
{
	public static class EntitySetMockExtensions
	{
		public static IEntitySet<TEntity> AsMockedEntitySet<TEntity>(this IEnumerable<TEntity> collection)
			where TEntity : class
		{
			return CreateQueryableEntitySetMock(collection).Object;
		}

		public static IEntitySet<TEntity> AsMockedEntitySet<TEntity>([NotNull] this IEnumerable<TEntity> collection,
			Func<TEntity, object[]> keyGetter
			)
			where TEntity : class
		{
			if (collection == null) throw new ArgumentNullException("collection");

			collection = collection.ToList();
			var entitySetMock = CreateQueryableEntitySetMock(collection);

			entitySetMock.
				Setup(x => x.Find(It.IsAny<object[]>())).
				Returns<object[]>(key => collection.FirstOrDefault(e => key.SequenceEqual(keyGetter(e))));
			
			return entitySetMock.Object;
		}

		public static IEntitySet<TEntity> AsMockedEntitySet<TEntity>(
			this IEnumerable<TEntity> collection,
			Func<TEntity, object> keyGetter
			)
			where TEntity : class
		{
			return collection.AsMockedEntitySet(e => new[] {keyGetter(e)});
		}

		private static Mock<IEntitySet<TEntity>> CreateQueryableEntitySetMock<TEntity>(
			[NotNull] IEnumerable<TEntity> collection
			)
			where TEntity : class
		{
			if (collection == null) throw new ArgumentNullException("collection");

			IQueryable<TEntity> queryable = collection.AsQueryable();
			var entitySetMock = new Mock<IEntitySet<TEntity>>();
			entitySetMock.Setup(x => x.GetEnumerator()).Returns(queryable.GetEnumerator());
			entitySetMock.Setup(x => x.Expression).Returns(queryable.Expression);
			entitySetMock.Setup(x => x.ElementType).Returns(queryable.ElementType);
			entitySetMock.Setup(x => x.Provider).Returns(queryable.Provider);
			entitySetMock.As<IEnumerable>().Setup(x => x.GetEnumerator()).Returns(queryable.GetEnumerator);
			return entitySetMock;
		}
	}
}
